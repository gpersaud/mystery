# New Mytoori Launch Plan

## Intro

The new mytoori is a web application where users can see a list of bilingual books, purchase any of these and read them online.
This document covers a few topics that need to be documented of how the solution will work and how to go from the current development stages to the production application.

## Solution (front-end and back-end)

The solution consists of two apps. The back-end application running on NodeJS connects to the mongo database to store data and provide the API endpoints.
The front-end application is built on ReactJS and connects to the back-end through the end-points it provides.

## Deployment

### Testing environment

So far testing has been taking place on the following environments. https://mytoori.netlify.com hosts the front-end application. Heroku hosts the back-end on mytoori-service.heroku.com

Code is pushed to these environments in the following way:

_Front-end_: Simply pushing from the development branch deploys the latest code making it available on mytoori.netlify.com.

_Back-end_: Here the code would need to be pushed from the master branch to be deployed. A shortcut is to push directly from development to master.

### Acceptance environment

One of the first things is to get an ACC environment up and running. The idea is to update this regularly to mimic the PRD environment prior to really going to PRD.
The Front-end acc environment is https://mytoori-acc.netlify.com. It automatically deploys when updates are pushed to the acc branch.

## Code management

Both code repositories are stored in bitbucket projects. Environment variables are not in the code repository. These need to be configured directly on the hosting platforms.

## Database management

All the databases are on https://mlab.com. The production data is currently stored in a database called meteor. These are all free instances for now.

### Database copy

Sometimes a new copy of the PRD database is required. Regular non-automatic backups are made of the PRD database and stored in
`/Users/Giwan/Documents/Workflow/Mytoori/db_bck/backup_data`

#### Create new copy of database

Backup the latest data again if necessary
Copy all the `*.BSON` and `*.JSON` files to a folder called **dump**
While in the parent of the dump folder, run mongorestore as described in [mongorestore](#mongorestore)

#### mongorestore

The following reads from the **dump** folder to restore the target database.

```
    mongorestore --noIndexRestore -h ds219100.mlab.com --port 19100 -d meteor_acc -u meteor
```

#### Make import directly from database backup

This will read directly from the backup folder and import that into a database. The `--drop` flag also drops all collections before re-importing them.

```
    mongorestore --noIndexRestore --drop -h ds219100.mlab.com --port 19100 -d meteor_acc -u meteor --dir backup_data/dump_13_05_2018
```

### Data migration

The new application requires migration to create new data structures. These have a more long term value. There are scripts in place to ensure that the data can be migrated without errors.

The scripts are located outside of the project in a separate folder.
`/Users/Giwan/Documents/Workflow/Mytoori/db_bck/migration`

What does the migration really do?
It creates arrays for certain content. For example instead of using title1 and title2 as properties, these should be stored in an array of objects.

```
    doc.titles = [{
        lang: "en",
        value: "title in english"
    }, {
        lang: "nl",
        value: "title in dutch"
    }]
```

Flow:

1.  Create a copy of the PRD database (meteor)
2.  Import the data as described in [mongorestore](#mongorestore)
3.  From the terminal execute the migration scripts

Migration scripts order

1.  documents.js
2.  topics.js (will rename storyCollection based on the current database name)
3.  documentContentRestructure.js

To execute a migration script login to the mongo database in terminal. Check first which directory you are in. From there you can load scripts in the database with the following.

```
    load("./migration/documents.js")
```

This will read the script file and execute the JS functions. In documents.js for example, the contents of the documents collection are read and stored in a collection called **items**.

**IMPORTANT**

The results of the changes are only visible in robo T3 after refreshing the database. The `print` command can be used to see the results of the actions in the terminal. Considering the infrequency of this action it's best to leave them in place for confirmation thta everything is going well.

_Tip:
In the mongo shell use pwd(); to see what directory you're in and ls(); to list the files in that directory._

\_Note:
The items table is not actually used during the migration. This was just created for testing and can be removed.
Instead the data is stored in the documents collection. That is also the table that is updated with the proper titles array.

# Tips

## Find all titles with the text "sample"

`db.getCollection('documents').find({"title1": { $regex: /.*Sample.*/ig }})`

### Find all titles with text "sample" that have a price larger than 0

`db.getCollection('documents').find({"title1": { $regex: /.*Sample.*/ig }, price: {$gt: 0}})`

### Find a collection based on the size of the chapters

`db.getCollection('documents').find({"title1": { $regex: /pride/ig }, status: "published", "chapters": {$size: 61}})`
