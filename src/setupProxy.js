const { createProxyMiddleware } = require("http-proxy-middleware");

const serverOptions = {
    target: "https://mytoori-service.herokuapp.com",
    changeOrigin: true,
};

const options = {
    target: "http://localhost:3000",
    pathRewrite: {
        v3: "v2",
    },
    changeOrigin: true,
};

module.exports = (app) => {
    app.use("/api/v2", createProxyMiddleware(serverOptions));
    app.use("/api/v3", createProxyMiddleware(options));
};
