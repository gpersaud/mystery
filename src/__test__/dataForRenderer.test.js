import { fetchBooksDataServer } from "../../server/middleware/dataForRenderer";
// import * as bookListActions from "../actions/booksListActions";
jest.mock("../utils/helpers");
jest.mock("../actions/booksListActions");

describe("fetchBookDataServer", () => {
    const { networkFetchBooks } = require("../actions/booksListActions");
    it("should fetch book data on the server", async () => {
        networkFetchBooks.mockImplementation(() => ({
            ok: true,
            books: { test: "object" },
        }));

        const results = await fetchBooksDataServer("/books/free");
        expect(networkFetchBooks).toHaveBeenCalledWith("free");
        expect(networkFetchBooks).toHaveBeenCalledTimes(1);
        expect(results).toEqual({ test: "object" });
    });

    // it("should throw an error", async () => {
    //     networkFetchBooks.mockImplementation(() => ({}));
    //     const { logger } = require("../utils/helpers");

    //     await fetchBooksDataServer();

    //     expect(networkFetchBooks).toHaveBeenCalledTimes(1);
    //     expect(networkFetchBooks).toHaveBeenCalledWith("free");

    //     expect(logger.error).toHaveBeenCalled();
    // });

    it("should default to featured", async () => {
        networkFetchBooks.mockImplementation(() => ({
            ok: true,
            books: { test: "object" },
        }));

        const data = await fetchBooksDataServer("/books/");
        expect(networkFetchBooks).toHaveBeenCalledTimes(1);
        expect(networkFetchBooks).toHaveBeenCalledWith("featured");
    });

    it("should return an empty array", async () => {
        expect(await fetchBooksDataServer("/")).toEqual([]);
    });
});
