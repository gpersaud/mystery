import React from "react";
import PropTypes from "prop-types";
import CSSTransition from "react-transition-group/CSSTransition";

class ReaderText extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            inverted: props.inverted
        };
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        if (nextProps.inverted !== this.state.inverted)
            this.setState({
                inverted: nextProps.inverted
            });
    }

    switchContent = () => {
        const { bookmark } = this.props;
        this.setState(currentState => ({
            inverted: !currentState.inverted
        }));
        bookmark && bookmark(this.props.id);
    };
    render() {
        const { texts, id } = this.props;
        const toggle = this.state.inverted;
        return (
            <CSSTransition
                in={toggle}
                appear={toggle}
                timeout={300}
                classNames="fade"
            >
                <p id={id} onClick={this.switchContent} dir="auto">
                    {texts[Number(toggle)]}
                </p>
            </CSSTransition>
        );
    }
}

ReaderText.propTypes = {
    texts: PropTypes.array,
    id: PropTypes.string,
    bookmark: PropTypes.func,
    inverted: PropTypes.bool
};

export default ReaderText;
