import React from "react";
import PropTypes from "prop-types";
import CSSTransition from "react-transition-group/CSSTransition";

class ReaderTitle extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            inverted: props.inverted
        };
        this.switchState = this.switchState.bind(this);
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        if (nextProps.inverted !== this.state.isInverted)
            this.setState({
                inverted: nextProps.inverted
            });
    }

    switchState() {
        this.setState(currentState => ({
            inverted: !currentState.inverted
        }));
        const { bookmark } = this.props;
        bookmark && bookmark(this.props.chapterId);
    }

    render() {
        const { titles = [], chapterId } = this.props;
        const toggle = this.state.inverted;
        return (
            <header id={chapterId} onClick={this.switchState}>
                <CSSTransition
                    in={toggle}
                    appear={toggle}
                    timeout={500}
                    classNames="fade"
                >
                    <HeaderStyle type={this.props.type}>
                        {titles[Number(toggle)]}
                    </HeaderStyle>
                </CSSTransition>
            </header>
        );
    }
}
ReaderTitle.propTypes = {
    titles: PropTypes.array,
    type: PropTypes.number,
    chapterId: PropTypes.string,
    bookmark: PropTypes.func,
    inverted: PropTypes.bool
};
export default ReaderTitle;

const HeaderStyle = ({ type, children }) =>
    type === 1 ? (
        <h1 dir="auto">{children}</h1>
    ) : (
        <h2 dir="auto">{children}</h2>
    );

HeaderStyle.propTypes = {
    type: PropTypes.number,
    children: PropTypes.string
};
