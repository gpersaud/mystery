import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
    fetchBookContent,
    bookmarkSync,
    getBookmark,
} from "../../actions/bookActions";
import ReaderTitle from "./ReaderTitle";
import ReaderChapters from "./ReaderChapters";
import Loading from "../Loading/Loading";
import { Link } from "react-router-dom";
import "../../stylesheets/Reader.scss";

const _BOOKMARKS = "BOOKMARKS";
class Reader extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            inverted: !!props.match.params.inverted,
        };
    }
    componentDidMount() {
        const { fetchBookContent, match, book, getBookmark } = this.props;
        book._id !== match.params.id && fetchBookContent(match.params.id);
        getBookmark(match.params.id);
    }

    componentDidUpdate() {
        if (
            this.props.book &&
            this.props.book._id &&
            this.props.book._id === this.props.match.params.id
        ) {
            const { bookmarks } = this.props;
            const p = document.getElementById(bookmarks[this.props.book._id]);
            p && p.scrollIntoView({ behaviour: "smooth", block: "center" });
        }
    }

    bookmark(paragraphId) {
        let savedBookmarks =
            JSON.parse(window.localStorage.getItem(_BOOKMARKS)) || {};
        savedBookmarks[this.props.book._id] = paragraphId;
        window.localStorage.setItem(_BOOKMARKS, JSON.stringify(savedBookmarks));
        // store bookmark on server
        bookmarkSync(this.props.book._id, paragraphId);
    }

    formatLink() {
        const inverted = this.props.match.params.inverted ? "" : "i";
        return {
            pathname: `/read/${this.props.match.params.id}/${inverted}`,
        };
    }

    render() {
        const { book, match, isLoading } = this.props;
        if (isLoading) return <Loading />;
        return book && book._id && book._id === match.params.id ? (
            <div className="mt-reader__text-container">
                <ReaderTitle
                    titles={[book.title1, book.title2]}
                    type={1}
                    inverted={!!this.props.match.params.inverted}
                    bookmark={this.bookmark.bind(this)}
                />
                <ReaderChapters
                    chapters={book.chapters}
                    inverted={!!this.props.match.params.inverted}
                    bookmark={this.bookmark.bind(this)}
                />
            </div>
        ) : (
            <div className="mt-reader__container">
                <h1>Oops! Your library does not seem to have this book.</h1>
                <p>
                    Please select a book from your{" "}
                    <Link to="/library">Library</Link>.
                    <br /> <br />
                    <a href="mailto:info@duolir.com">
                        Let us know if there&aposs anyway we can help.
                    </a>
                </p>
            </div>
        );
    }
}

Reader.defaultProps = {
    book: {},
};

Reader.propTypes = {
    book: PropTypes.object,
    bookmarks: PropTypes.object,
    match: PropTypes.object,
    fetchBookContent: PropTypes.func,
    getBookmark: PropTypes.func,
    isLoading: PropTypes.bool,
};

const mapStateToProps = (state) => ({
    book: state.readerReducer.book,
    bookmarks: state.readerReducer.bookmarks,
    isLoading: state.readerReducer.isLoading,
});

const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            fetchBookContent,
            bookmarkSync,
            getBookmark,
        },
        dispatch
    );

export default connect(mapStateToProps, mapDispatchToProps)(Reader);
