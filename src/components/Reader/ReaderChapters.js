import React from "react";
import PropTypes from "prop-types";
import ReaderTitle from "./ReaderTitle";
import ReaderParagraphs from "./ReaderParagraphs";

const ReaderChapters = ({ chapters = [], bookmark, inverted }) =>
    chapters.map(chapter => (
        <div key={`chapter-${chapter.id}${chapter._id}`}>
            <ReaderTitle
                titles={[chapter.title1, chapter.title2]}
                type={2}
                bookmark={bookmark}
                chapterId={chapter.id}
                inverted={inverted}
            />
            <ReaderParagraphs
                paragraphs={chapter.paragraphs}
                bookmark={bookmark}
                inverted={inverted}
            />
        </div>
    ));

ReaderChapters.propTypes = {
    chapters: PropTypes.array
};

export default ReaderChapters;
