import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { fetchBook } from "../../actions/bookActions";
import ReaderTitle from "./ReaderTitle";
import ReaderChapters from "./ReaderChapters";
import Loading from "../Loading/Loading";
import "../../stylesheets/Reader.scss";
import { scrollToTop } from "../../utils/helpers";
import { Link } from "react-router-dom";
import LanguageSwitch from "../Header/LanguageSwitch";

const FreeReader = (props) => {
    const considerFetchingBook = () => {
        const { match } = props;
        props.fetchBook(match.params.bookId, 100);

        scrollToTop(".mt-layout__content-inner");
    };

    useEffect(considerFetchingBook, []);

    const { book } = props;

    if (!book || props.isLoading) {
        return <Loading />;
    }

    const titles = [book.title1, book.title2];
    const languages = book.languages || [book.language1, book.language2];

    if (props.match.params.inverted) {
        titles.reverse();
        languages.reverse();
    }

    return (
        <article className="mt-free-reader">
            <div className="mt-free-reader__header-container">
                <header className="mt-free-reader__header">
                    <div className="mt-free-reader__header-content">
                        {titles.map((t) => t && <h6 key={t}>{t}</h6>)}
                    </div>
                    <LanguageSwitch location={props.location}>
                        <div className="mt-free-reader__header-langs">
                            {languages.map((l) => l && <div key={l}>{l}</div>)}
                        </div>
                    </LanguageSwitch>
                    <Link to={`/book/${props.match.params.bookId}`}>
                        <span className="material-icons">close</span>
                    </Link>
                </header>
            </div>
            <main className="mt-free-reader__text-container">
                <ReaderTitle
                    titles={[book.title1, book.title2]}
                    type={1}
                    inverted={!!props.match.params.inverted}
                />
                <ReaderChapters
                    chapters={book.chapters}
                    inverted={!!props.match.params.inverted}
                />
            </main>
        </article>
    );
};

FreeReader.propTypes = {
    book: PropTypes.object,
    isLoading: PropTypes.bool,
    match: PropTypes.object,
    location: PropTypes.object,
    fetchBook: PropTypes.func,
};

const mapStateToProps = (state) => ({
    book: state.booksReducer.selectedBook,
    isLoading: state.readerReducer.isLoading,
});

const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            fetchBook,
        },
        dispatch
    );

export default connect(mapStateToProps, mapDispatchToProps)(FreeReader);
