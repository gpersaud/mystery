import React from "react";
import ReaderText from "./ReaderText";

const ReaderParagraphs = ({ paragraphs = [], bookmark, inverted }) =>
    paragraphs.map((p) => (
        <ReaderText
            key={`paragraph-${p.id || p._id}`}
            id={p.id || p._id}
            texts={[p.text1, p.text2]}
            bookmark={bookmark}
            inverted={inverted}
        />
    ));

export default ReaderParagraphs;
