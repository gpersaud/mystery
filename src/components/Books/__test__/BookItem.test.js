import React from "react";
import { shallow } from "enzyme";
import BookItem from "../BookItem";
import props from "../__mocks__/book.mock";
import { MemoryRouter, Link } from "react-router-dom";

props.fetchBookCover = jest.fn();

describe("Book", () => {
    it("should render the book item component", () => {
        const wrapper = shallow(
            <MemoryRouter initialEntries={["/test"]} keyLength={1} key={"x"}>
                <BookItem {...props} />
            </MemoryRouter>
        );
        expect(wrapper.find(<Link to={{}} />)).toBeDefined();
    });
});
