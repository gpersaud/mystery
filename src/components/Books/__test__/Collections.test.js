import React from "react";
import { mount } from "enzyme";
import Collections from "../Collections";
import { MemoryRouter, NavLink } from "react-router-dom";

let fetchBooks = undefined,
    fetchCollections = undefined;

describe("Collections", () => {
    let mountedCollections;
    let props;

    const mountCollections = props => {
        if (!mountedCollections) {
            mountedCollections = mount(
                <MemoryRouter>
                    <Collections {...props} />
                </MemoryRouter>
            );
        }
        return mountedCollections;
    };
    beforeEach(() => {
        fetchBooks = jest.fn();
        fetchCollections = jest.fn();
        mountedCollections = undefined;

        props = {
            collections: [
                {
                    _id: "d48qdrDaMGQW6aA9E",
                    id: "25338999275",
                    title1: "Featured",
                    title2: "",
                    cover_image: "",
                    cover_image_type: "",
                    status: "public",
                    password: "",
                    target: "featured",
                    titles: [
                        {
                            lang: "featured",
                            value: "featured"
                        }
                    ]
                },
                {
                    _id: "zX5FnLFWQyZGLfMQd",
                    id: "25312785686",
                    title1: "Free",
                    title2: "",
                    cover_image: "",
                    cover_image_type: "",
                    status: "public",
                    password: "",
                    target: "free",
                    titles: [
                        {
                            lang: "free",
                            value: "free"
                        }
                    ]
                }
            ],
            fetchBooks,
            fetchCollections,
            location: {
                pathname: "/book/com.mytoori.something"
            },
            match: {
                params: {
                    collection: "featured"
                }
            }
        };
    });

    it("always renders a div", () => {
        const divs = mountCollections({ ...props, fetchCollections });
        expect(divs.length).toBeGreaterThan(0);
    });
    // describe("the rendered div", () => {
    //   it("contains everything else that gets rendered", () => {
    //     const divs = mountCollections({ fetchCollections }).find("div");
    //     const wrappingDiv = divs.first();
    //     expect(wrappingDiv.children()).toEqual(
    //       mountCollections({ fetchCollections }).children()
    //     );
    //   });
    // });

    it("should render the collections", () => {
        const collections = mountCollections(props);
        expect(collections.length).toBe(1);
    });

    it("should render 2 NavLink items", () => {
        const links = mountCollections(props).find(NavLink);
        expect(links.length).toBeGreaterThan(0);
        expect(links.length).toBe(2);
    });

    it("should trigger the fetch collections function on mount ", () => {
        mountCollections(props);
        expect(fetchCollections).toHaveBeenCalledTimes(1);
    });

    it("should fetchBooks when NavLink is clicked", () => {
        const collections = mountCollections(props);

        collections
            .find(NavLink)
            .last()
            .simulate("click");
        expect(fetchBooks).toHaveBeenCalledTimes(1);
        expect(fetchBooks.mock.calls[0][0]).toBe("free");
        collections
            .find(NavLink)
            .first()
            .simulate("click");
        expect(fetchBooks.mock.calls[1][0]).toBe("featured");
    });
});
