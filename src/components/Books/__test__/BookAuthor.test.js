import React from "react";
import { render } from "enzyme";
import BookAuthor from "../../Author/BookAuthor";

describe("Check book author", () => {
    it("should render the book author", () => {
        const props = {
            book: {
                authorFirstname: "Jane",
                authorLastname: "Doe",
                author: "Jane Doe"
            }
        };

        const author = render(<BookAuthor {...props} />);
        expect(author).toMatchSnapshot();
    });
});
