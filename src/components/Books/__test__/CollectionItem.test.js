import React from "react";
import { mount } from "enzyme";
import CollectionItem from "../CollectionItem";
import { MemoryRouter } from "react-router-dom";

/**
 * Check that the item is rendered
 * It should have a property for the active class
 * The title (label) shown should match the props that was passed in
 */

describe("CollectionItem", () => {
    let props;
    let mountedCollectionItem;

    const mountCollectionItem = props =>
        mountedCollectionItem ||
        (mountedCollectionItem = mount(
            <MemoryRouter>
                <CollectionItem {...props} />
            </MemoryRouter>
        ));

    beforeEach(() => {
        mountedCollectionItem = undefined;
        props = {
            fetchBooks: jest.fn(),
            item: {
                title1: "titlevalue",
                target: "featured",
                titles: [
                    {
                        lang: "featured",
                        value: "featured"
                    }
                ]
            }
        };
    });

    it("should check that the div has been rendered", () => {
        const component = mountCollectionItem(props);
        expect(component.length).toBeGreaterThan(0);
    });

    it("Should have a property for the active class", () => {
        const component = mountCollectionItem(props);
        expect(component.find("a").length).toBe(1);
    });

    /**
     * The collection item is wrapped in the MemoryRouter component
     * Therefore we first need to go one level deep to get to the
     * CollectionItem component
     */
    it("should receive a target and title property", () => {
        const collectionItem = mountCollectionItem(props).find(CollectionItem);
        expect(collectionItem.props().item.target).toBe("featured");
        expect(collectionItem.props().item.title1).toEqual("titlevalue");
    });

    it("Should trigger the fetchBooks function", () => {
        const collectionItem = mountCollectionItem(props).find(CollectionItem);
        expect(props.fetchBooks).toHaveBeenCalledTimes(0);
        collectionItem.simulate("click");
        expect(props.fetchBooks).toHaveBeenCalledTimes(1);
    });
    it("should an argument that matches the props item target value", () => {
        const collectionItem = mountCollectionItem(props).find(CollectionItem);
        collectionItem.simulate("click");
        expect(props.fetchBooks).toHaveBeenCalledTimes(1);
        expect(props.fetchBooks.mock.calls[0][0]).toBe(props.item.target);
        collectionItem.simulate("dblclick");
        expect(props.fetchBooks).toHaveBeenCalledTimes(1);
        collectionItem.simulate("click");
        expect(props.fetchBooks).toHaveBeenCalledTimes(2);
    });
});
