import React from "react";
import BookCover from "../BookCover";
import { shallow, mount } from "enzyme";
import { scaledUrl } from "../BookCover";

describe("BookCover", () => {
    const book = {
        coverImages: [
            {
                _id: "5d0e45c513056484315f930b",
                value:
                    "https://res.cloudinary.com/mytoori/image/upload/v1560608643/mytoori/covers/28955985393_f9c09135e9_o.png.png",
            },
        ],
    };

    it("should match snapshot", () => {
        const fetchBookCover = jest.fn();
        const wrapper = shallow(
            <BookCover
                book={book}
                fetchBookCover={fetchBookCover}
                large={true}
                covers={[]}
            />
        );

        expect(wrapper).toMatchSnapshot();
        expect(fetchBookCover).not.toHaveBeenCalled();
    });

    it("should call fetchBookCover", () => {
        const fetchBookCover = jest.fn();

        jest.spyOn(React, "useEffect").mockImplementation((f) => f());

        mount(
            <BookCover
                book={{ ...book, coverImages: [] }}
                fetchBookCover={fetchBookCover}
                large={true}
                covers={[]}
            />
        );

        expect(fetchBookCover).toHaveBeenCalled();
    });
});

describe("scaled function", () => {
    const url =
        "https://res.cloudinary.com/mytoori/image/upload/v1560608643/mytoori/covers/28117181962_1ffa870fcb_o.png.png";
    it("should return a url string with the scaled default values", () => {
        const result = scaledUrl(url);
        expect(result).toEqual(
            "https://res.cloudinary.com/mytoori/image/upload/c_scale,w_125/v1560608643/mytoori/covers/28117181962_1ffa870fcb_o.png.png"
        );
    });

    it("should return a url string with the scaled provided values", () => {
        const result = scaledUrl(url, "w_100");
        expect(result).toEqual(
            "https://res.cloudinary.com/mytoori/image/upload/c_scale,w_100/v1560608643/mytoori/covers/28117181962_1ffa870fcb_o.png.png"
        );
    });

    it("should return the string given", () => {
        const result = scaledUrl("test");
        expect(result).toEqual("test");
    });
});
