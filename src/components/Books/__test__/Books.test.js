import React from "react";
import { render } from "enzyme";
import Books from "../Books";
import { MemoryRouter } from "react-router-dom";
import configureMockStore from "redux-mock-store";
import { Provider } from "react-redux";
import thunk from "redux-thunk";
// import mockBook from "../__mocks__/book.mock";

const mockStore = configureMockStore([thunk]);
// const mockFetchBooks = jest.fn();
// const mockFetchBookCover = jest.fn();

/**
 * This test imports the books component with the connect component
 * wrapped around it. Therefore a store component must be provided.
 * This is mocked using "redux-mock-store"
 */
// describe("Books", () => {
//   it("should render a list of books", () => {
// const store = mockStore({
//   booksReducer: {
//     books: []
//   },
//   filterReducer: {
//     languages: []
//   }
// });
//     const renderedBooks = shallow(
//       <Books store={store} fetchBooks={() => {}} />
//     );
//     expect(renderedBooks.prop.fetchBooks).toEqual(() => {});
//     expect(renderedBooks.prop("books")).toEqual([]);
//   });
// });

/**
 * This test omits the use of the redux connect component
 * which makes it easy to simply test the react component
 */
xdescribe("Tests books component without connect", () => {
    it("should test books component without connect", () => {
        const ContextStore = React.createContext();
        const store = mockStore({
            booksReducer: {
                books: []
            },
            filterReducer: {
                languages: []
            },
            authorReducer: {
                authors: []
            }
        });
        const props = {
            match: {
                params: {
                    collection: "featured"
                }
            }
        };
        const renderedBooks = render(
            <MemoryRouter>
                <Provider context={ContextStore}>
                    <Books store={ContextStore} {...props} />
                </Provider>
            </MemoryRouter>
        );
        const headerText = renderedBooks.find("h1").text();
        expect(headerText).not.toEqual("adfalsdjlj");
        expect(headerText).toEqual("Bilingual Stories");
        expect(renderedBooks).toMatchSnapshot();
    });
});

xdescribe("test that the header is not rendered", () => {
    it("should not render the header intro", () => {
        const store = mockStore({
            booksReducer: {
                books: []
            },
            filterReducer: {
                languages: []
            },
            authorReducer: {
                authors: []
            }
        });
        const props = {
            match: {
                params: {
                    collection: "en"
                }
            }
        };
        const renderedBooks = render(
            <MemoryRouter>
                <Books store={store} {...props} />
            </MemoryRouter>
        );
        const headerText = renderedBooks.find("h1").text();
        expect(headerText).toEqual("");
        expect(renderedBooks).toMatchSnapshot();
    });
});
