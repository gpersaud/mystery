import React, { useEffect } from "react";
import PropTypes from "prop-types";

export const scaledUrl = (url, width = "w_125") =>
    url.replace(/upload/i, `upload/c_scale,${width}`);

const formatCoverStyle = (coverUrl = "") => ({
    backgroundImage: `url(${coverUrl})`,
    width: "125px",
    height: "195px",
    backgroundSize: "contain",
});

const getFlickrCover = (covers, book) => {
    const cover = getCover(covers, book);
    return cover && cover.coverUrl ? cover.coverUrl : "";
};

const BookCover = ({
    covers = [],
    book = {},
    large = false,
    fetchBookCover,
}) => {
    useEffect(() => {
        const { coverImages = [] } = book;
        if (coverImages && coverImages.length > 0) return; // no need to fetch from flickr
        const coverId = getBookCoverId(book);
        const bookCover = covers.find((cover) => cover.coverId === coverId);
        !bookCover && fetchBookCover(coverId, book._id);
    }, [fetchBookCover, book, covers]);

    const { coverImages = [] } = book;
    const [cloudinaryImage = {}] = coverImages;

    const coverUrl =
        (cloudinaryImage && cloudinaryImage.value) ||
        getFlickrCover(covers, book);

    const className = large
        ? "mt-book__cover-large"
        : "mt-book__cover-container";

    return (
        <div>
            <div className="mt-book__cover">
                {!coverUrl ? null : (
                    <div
                        style={formatCoverStyle(scaledUrl(coverUrl))}
                        className={className}
                    />
                )}
            </div>
        </div>
    );
};

BookCover.propTypes = {
    book: PropTypes.shape({
        coverImages: PropTypes.array,
    }),
    fetchBookCover: PropTypes.func,
    large: PropTypes.bool,
    covers: PropTypes.array,
};

export default BookCover;

/**
 * get the id of the book covers in the book
 * The return object will have a url which is used to show the book cover
 * @param {Array} covers The list of covers that have been downloaded
 * @param {Object} book The book object with it's associated cover ids
 */
const getCover = (covers = [], book) =>
    (covers &&
        covers.length > 0 &&
        covers.find((cover) => cover.coverId === getBookCoverId(book))) ||
    {};

/**
 * Takes a book object and extracts it's cover id
 * @param {Object} book The book object from which to extract the cover id
 */
const getBookCoverId = (book) =>
    book &&
    book.covers &&
    book.covers.length > 0 &&
    book.covers[0] &&
    book.covers[0].value;
