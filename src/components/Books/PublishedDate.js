import React from "react";

import DateFNS from "date-fns";

const PublishedDate = () => {
    return (
        <div className="mt-book__info-published">
            <div className="mt-book__info-published-title"> published </div>{" "}
            {DateFNS.format(new Date(), "Do of MMMM YYYY")}{" "}
        </div>
    );
};

export default PublishedDate;
