import React from "react";
import PropTypes from "prop-types";
import md5 from "md5";

const UserIcon = ({ email = "" }) => {
    if (!email) return <div className="mt-book__info-user" />;
    const hash = md5(email);
    return (
        <div>
            <img
                src={`https://www.gravatar.com/avatar/${hash}?s=50`}
                className="mt-book__info-user"
                alt="user profile "
            />
        </div>
    );
};
UserIcon.propTypes = {
    email: PropTypes.string
};

export default UserIcon;
