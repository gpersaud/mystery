import React from "react";
import PropTypes from "prop-types";
import BookItem from "./BookItem";
import Loading from "../Loading/Loading";
import { Helmet } from "react-helmet";
import { getLanguageName } from "../Label/LanguageName";
import Alert from "react-s-alert";

const _BOOK_AMOUNT = 9;
const _BOOK_INCREMENT = 6;
let timer;

class BooksList extends React.Component {
    componentDidMount() {
        const { fetchBooks, match } = this.props;
        fetchBooks &&
            fetchBooks(
                match.params.collection || "featured",
                match.params.limit || _BOOK_AMOUNT
            );

        // TODO: check if this is the best way to do this
        window.document
            .querySelector(".mt-layout__content")
            .addEventListener("scroll", this.handleScroll);
    }

    componentWillUnmount() {
        window.document
            .querySelector(".mt-layout__content")
            .removeEventListener("scroll", this.handleScroll);
    }

    handleScroll = (e) => {
        const {
            fetchBooks,
            match,
            books = [],
            isLoading,
            allBooksLoaded,
        } = this.props;
        const { scrollHeight, scrollTop, clientHeight } = e.target;
        const bottom =
            Math.round(scrollHeight) - Math.round(scrollTop) <=
            Math.round(clientHeight) + 50;
        if (!timer && !isLoading && bottom && !allBooksLoaded) {
            timer = setTimeout(() => {
                fetchBooks(
                    match.params.collection || "featured",
                    books.length + _BOOK_INCREMENT
                );
                clearTimeout(timer);
                timer = null;
            }, 1000);
            Alert.info("Loading...");
        }
    };

    // TODO: the check here seems weird but is necessary
    // to fix the renderer bug for SSR
    // Try to optimize later
    render() {
        const {
            isLoading,
            books = [],
            match: { params: { collection } = {} } = {},
        } = this.props;
        return isLoading && books.length < 1 ? (
            <Loading />
        ) : (
            <div className="mt-books__grid">
                <Helmet>
                    <title>
                        {`${getLanguageName(collection)} Mytoori books`}
                    </title>
                </Helmet>
                {books &&
                    books.length > 0 &&
                    typeof books.map === "function" &&
                    books.map((book, i) => (
                        <BookItem
                            {...this.props}
                            key={book._id + i}
                            book={book}
                        />
                    ))}
            </div>
        );
    }
}

BooksList.propTypes = {
    fetchBooks: PropTypes.func,
    isLoading: PropTypes.bool,
    match: PropTypes.shape({
        isExact: PropTypes.bool,
        params: PropTypes.shape({
            collection: PropTypes.string,
            limit: PropTypes.number
        }),
        path: PropTypes.string,
        url: PropTypes.string,
    }),
    books: PropTypes.arrayOf(
        PropTypes.oneOfType([PropTypes.object, PropTypes.instanceOf(BookItem)])
    ),
    allBooksLoaded: PropTypes.bool,
};

export default BooksList;
