import React from "react";
import PropTypes from "prop-types";
import { NavLink } from "react-router-dom";

class CollectionItem extends React.Component {
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }
    handleClick() {
        this.props.fetchBooks(this.props.item.target);
    }
    render() {
        const { item } = this.props;
        return (
            <NavLink
                to={`/books/${item.target}`}
                className="mt-collections__link"
                activeClassName="mt-collections__link--active"
                onClick={this.handleClick}
            >
                <button>{item.titles[0].value}</button>
            </NavLink>
        );
    }
}

export default CollectionItem;

CollectionItem.propTypes = {
    fetchBooks: PropTypes.func,
    item: PropTypes.shape({
        title1: PropTypes.string.isRequired,
        target: PropTypes.string.isRequired,
        titles: PropTypes.array,
    }),
};
