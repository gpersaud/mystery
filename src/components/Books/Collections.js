import React, { useEffect } from "react";
import PropTypes from "prop-types";
import CollectionItem from "./CollectionItem";
import { sortCollections } from "../../utils/helpers";
import Loading from "../Loading/Loading";

const getFilterName = (id, collection = []) => {
    const currentCollection = collection.find((c) => c.target === id);
    return currentCollection && currentCollection.title1
        ? currentCollection.title1
        : "";
};

const Collections = ({ filter, fetchCollections, collections, ...props }) => {
    useEffect(() => {
        fetchCollections();
    }, [filter, fetchCollections]);

    if (!(collections && collections.length > 0 && Array.isArray(collections)))
        return <Loading />;

    const sortedCollections = sortCollections(collections);

    return (
        <div className="mt-collections">
            <label>Collection</label>
            <span className="mt-collections__filter">
                {getFilterName(props.match.params.collection, collections)}
            </span>
            <div className="mt-collections__container">
                {sortedCollections.map((item, idx) => (
                    <CollectionItem
                        key={item._id + idx}
                        {...props}
                        item={item}
                    />
                ))}
            </div>
        </div>
    );
};

Collections.propTypes = {
    fetchCollections: PropTypes.func.isRequired,
    collections: PropTypes.array,
    isFixed: PropTypes.bool,
    filter: PropTypes.string,
    match: PropTypes.object,
};

export default Collections;
