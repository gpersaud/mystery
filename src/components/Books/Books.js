import React from "react";
import PropTypes from "prop-types";
import BooksList from "./BooksList";
import Collections from "./Collections";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { fetchBooks, selectBook } from "../../actions/booksListActions";
import { fetchBookCover } from "../../actions/bookActions";
import { fetchCollections } from "../../actions/collectionActions";
import { fetchUser } from "../../actions/authorActions";
import Intro from "../Intro/Intro";

const Books = props => (
    <div className="mt-books__container">
        <Collections {...props} />
        <Intro {...props} />
        <BooksList {...props} />
    </div>
);

Books.propTypes = {
    isFixed: PropTypes.bool
};

const mapStateToProps = state => ({
    books: state.booksReducer.books,
    covers: state.booksReducer.covers,
    isLoading: state.booksReducer.isLoading,
    collections: state.booksReducer.collections,
    authors: state.authorReducer.authors,
    allBooksLoaded: state.booksReducer.allBooksLoaded
});

const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            fetchBooks,
            fetchCollections,
            fetchBookCover,
            selectBook,
            fetchUser
        },
        dispatch
    );
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Books);
