import React from "react";
import PropTypes from "prop-types";

const BookItemInfo = ({ book }) => {
    if (
        book &&
        book.authors &&
        book.authors.length > 0 &&
        (book.authors[0].firstName || book.authors[0].lastName)
    ) {
        return (
            <div className="mt-book__info-container">
                <div className="mt-book__info-author">
                    <div className="mt-book__info-author-title">Author</div>
                    {book.authors.map((author, idx) => (
                        <div key={author.lastName + idx}>
                            {author.firstName} {author.lastName}
                        </div>
                    ))}
                </div>
            </div>
        );
    }
    return null;
};

BookItemInfo.propTypes = {
    authors: PropTypes.array,
    book: PropTypes.object,
    fetchUser: PropTypes.func
};

export default BookItemInfo;
