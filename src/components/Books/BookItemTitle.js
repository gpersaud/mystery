import React from "react";
import PropTypes from "prop-types";

const BookItemTitle = ({ children, number }) => (
    <div className={`mt-book__instance-${number}`}>
        <div className={`mt-book__text-title-${number}`} dir="auto">
            {children}
        </div>
    </div>
);

BookItemTitle.defaultProps = {
    number: 1
};

BookItemTitle.propTypes = {
    children: PropTypes.string.isRequired,
    number: PropTypes.number
};

export default BookItemTitle;
