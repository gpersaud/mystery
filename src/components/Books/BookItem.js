import React, { Component } from "react";
import PropTypes from "prop-types";
import LanguageLabel from "../Label/Language";
import BookItemTitle from "./BookItemTitle";
import BookItemInfo from "./BookItemInfo";
import BookCover from "./BookCover";
import { Link } from "react-router-dom";
import "../../stylesheets/BookItem.scss";

class BookItem extends Component {
    showBook = () => this.props.selectBook(this.props.book);

    bookPrice = book => (book.price ? `€ ${book.price}` : "Free");

    render() {
        const { book = [] } = this.props;
        const bookTitles = [...book.titles].reverse();
        return (
            <Link
                className="mt-book__container"
                to={{
                    pathname: `/book/${book._id}`,
                    state: { sender: "bookItem" }
                }}
                onClick={this.showBook}
            >
                <div className="mt-book">
                    <BookCover
                        book={book}
                        fetchBookCover={this.props.fetchBookCover}
                        large={false}
                        covers={this.props.covers}
                    />
                    <LanguageLabel
                        languages={[book.language1, book.language2]}
                    />
                    <div className="mt-book__text-container">
                        {bookTitles.map((title, idx) => (
                            <BookItemTitle
                                key={`bookItem-bookItemTitle-${title.value}-${title.lang}-${idx}`}
                                number={idx}
                            >
                                {title.value}
                            </BookItemTitle>
                        ))}
                    </div>
                    <BookItemInfo {...this.props} />
                    <div className="mt-book__action-container">
                        <span>{this.bookPrice(book)}</span>
                        <span>more info</span>
                    </div>
                </div>
            </Link>
        );
    }
}

BookItem.propTypes = {
    book: PropTypes.object,
    fetchBookCover: PropTypes.func,
    history: PropTypes.object,
    selectBook: PropTypes.func,
    covers: PropTypes.array
};

export default BookItem;
