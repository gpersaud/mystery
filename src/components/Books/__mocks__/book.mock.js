export default {
    book: {
        _id: "asldfjlasjd",
        cover: "1234",
        cover2: "5678",
        title1: "title1",
        title2: "title2",
        titles: [
            {
                lang: "en",
                value: "title1"
            },
            { lang: "nl", value: "title2" }
        ],
        language1: "en",
        language2: "nl",
        description1: "decription1",
        description2: "description2",
        authorFirstname: "authorfirstname",
        authorLastname: "authorlastname",
        authors: [],
        isbn: ["com.mytoori.something"],
        notes1: "notes1",
        notes2: "notes2",
        status: "published",
        userId: "asdlfja",
        coverImages: []
    }
};
