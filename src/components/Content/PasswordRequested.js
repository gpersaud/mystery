import React from "react";
import PropTypes from "prop-types";
import "../../stylesheets/ForgotPassword.scss";

const PasswordRequested = ({ email, resetPasswordAgain }) => (
    <div className="mt-password-requested__container">
        <div className="mt-password-requested">
            <h1>Your password reset has been requested</h1>
            <p>
                We sent a password reset link to <strong>{email}</strong>. Check
                your email inbox to continue. Please note that it may take
                several minutes for the email to arrive.
            </p>
            <button onClick={resetPasswordAgain}>Try again</button>
        </div>
    </div>
);

PasswordRequested.propTypes = {
    email: PropTypes.string,
    resetPasswordAgain: PropTypes.func
};

export default PasswordRequested;
