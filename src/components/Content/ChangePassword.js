import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { validateHash, changePassword } from "../../actions/userActions";
import Alert from "react-s-alert";
import Loading from "../Loading/Loading";
import { validatePassword } from "../../utils/helpers";
import { Redirect } from "react-router-dom";

/**
 * Check that the user has a valid hash in their route and then
 * allow them to reset the password.
 * The hash is also sent with the request to update the password
 * @param {Object} param0 The parameters passed into the route
 */
class ChangePassword extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            password: null,
            repeatPassword: null
        };
        this.handlePasswordUpdate = this.handlePasswordUpdate.bind(this);
        this.handleOnChange = this.handleOnChange.bind(this);
    }

    handleOnChange(e) {
        const { name, value } = e.currentTarget;
        this.setState(current => {
            current[name] = value;
            return { ...current };
        });
    }

    handlePasswordUpdate(e) {
        e.preventDefault();
        //check that the password match
        const { password, repeatPassword } = this.state;
        try {
            validatePassword({ password, repeatPassword });
            this.props.changePassword(password, this.props.match.params.hash);
        } catch (e) {
            Alert.error(e.message);
        }
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        if (nextProps.isPasswordUpdated !== undefined) {
            Alert.info(
                `password ${
                    nextProps.isPasswordUpdated === true
                        ? "updated"
                        : "update failed"
                }`
            );
        }
    }

    render() {
        if (this.props.isChangingPassword) return <Loading />;

        if (this.props.isPasswordUpdated) return <Redirect to="/login" />;

        return (
            <div className="mt-password-reset__container">
                <div className="mt-password-reset">
                    <form
                        name="password-reset-form"
                        className="mt-password-reset__form"
                        onSubmit={this.handlePasswordUpdate}
                    >
                        <label>
                            <span>New Password</span>
                            <input
                                type="password"
                                name="password"
                                placeholder="Type new password"
                                onChange={this.handleOnChange}
                            />
                        </label>

                        <label htmlFor="password2">
                            <span>Repeat password</span>
                            <input
                                type="password"
                                id="password1"
                                name="repeatPassword"
                                placeholder="Enter new password again"
                                onChange={this.handleOnChange}
                            />
                        </label>
                        <div className="mt-password-reset__action-container">
                            <button
                                className="mt-shared__button"
                                data-type="primary"
                            >
                                Update Password
                            </button>
                        </div>
                    </form>
                    <div className="mt-password-reset__action-secondary">
                        <a
                            className="mt-shared__button"
                            data-type="secondary"
                            href="/login"
                        >
                            login
                        </a>
                    </div>
                    <HelpMessage
                        isPasswordUpdated={this.props.isPasswordUpdated}
                    />
                </div>
            </div>
        );
    }
}

ChangePassword.propTypes = {
    validateHash: PropTypes.func,
    match: PropTypes.object,
    isResetHashValid: PropTypes.bool,
    isValidatingHash: PropTypes.bool,
    changePassword: PropTypes.func,
    isPasswordUpdated: PropTypes.bool,
    isChangingPassword: PropTypes.bool
};

const mapStateToProps = state => ({
    isResetHashValid: state.userReducer.isResetHashValid,
    isValidatingHash: state.userReducer.isValidatingHash,
    isChangingPassword: state.userReducer.isChangingPassword,
    isPasswordUpdated: state.userReducer.isPasswordUpdated
});

const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            validateHash,
            changePassword
        },
        dispatch
    );

export default connect(mapStateToProps, mapDispatchToProps)(ChangePassword);

const HelpMessage = ({ isPasswordUpdated }) => {
    if (isPasswordUpdated === undefined || isPasswordUpdated === true)
        return null;
    return (
        <div className="mt-password-reset__message">
            Please try again or contact us at{" "}
            <a href="mailto:info@mytoori.com">info@mytoori.com</a> for
            assistance.
        </div>
    );
};

HelpMessage.propTypes = {
    isPasswordUpdated: PropTypes.bool
};
