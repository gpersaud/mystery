import React from "react";
import PropTypes from "prop-types";
import { Route, Switch } from "react-router-dom";
import PrivateRoute from "../PrivateRoute";
import loadable from "@loadable/component";
import ssrRoutes from "../../routes";

import "react-s-alert/dist/s-alert-default.css";
import "react-s-alert/dist/s-alert-css-effects/stackslide.css";

// Lazy load components
const Library = loadable(() => import("../Library/Library"));
const Reader = loadable(() => import("../../components/Reader/Reader"));
const Terms = loadable(() => import("../Policy/Terms"));
const AboutContainer = loadable(() => import("../Info/AboutContainer"));
const Contact = loadable(() => import("../Info/Contact"));
const Checkout = loadable(() => import("../Checkout/Checkout"));
const Videos = loadable(() => import("../Info/Videos"));
const Faq = loadable(() => import("../Info/Faq"));
const Signup = loadable(() => import("../../components/User/Signup"));
const NotFound = loadable(() => import("../../components/NotFound/NotFound"));
const Receipt = loadable(() => import("../Finance/Receipt"));
const AuthFailed = loadable(() => import("./AuthFailed"));
const ForgotPassword = loadable(() => import("./ForgotPassword"));
const ChangePassword = loadable(() => import("./ChangePassword"));

const Content = (props) => (
    <main className="mt-layout__content">
        <div className="mt-layout__content-inner">
            <Switch>
                <PrivateRoute path="/library/:id?" component={Library} />
                <PrivateRoute path="/read/:id/:inverted?" component={Reader} />
                {/* localProps allows the signup page to have access to the 
                params. The singup can show the email passed to it. */}
                <Route
                    path="/signup/:email?"
                    render={(localProps) => (
                        <Signup {...props} {...localProps} />
                    )}
                />
                <Route path="/auth/failed" component={AuthFailed} />
                <Route path="/receipt/:id" component={Receipt} />
                <Route path="/password" component={ForgotPassword} />
                <Route
                    path="/changepassword/:hash"
                    component={ChangePassword}
                />
                <Route path="/terms" component={Terms} />
                <Route path="/about" component={AboutContainer} />
                <Route path="/learning" component={Videos} />
                <Route path="/contact" component={Contact} />
                <Route path="/checkout/:bookId" component={Checkout} />
                <Route path="/faq" component={Faq} />

                {ssrRoutes.map((r) => (
                    <Route key={r.path} path={r.path} component={r.component} />
                ))}
                <Route path="*" component={NotFound} />
            </Switch>
        </div>
    </main>
);

Content.propTypes = {
    auth: PropTypes.object,
};

export default Content;
