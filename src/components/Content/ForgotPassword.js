import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { resetPassword, resetPasswordAgain } from "../../actions/userActions";
import PasswordRequested from "./PasswordRequested";
import Loading from "../Loading/Loading";
import PrimaryButton from "../../components/Form/PrimaryButton";
import "../../stylesheets/ForgotPassword.scss";

class ForgotPassword extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            credentials: {},
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleChange(e) {
        e.preventDefault();
        const { credentials } = this.state;
        credentials[e.currentTarget.name] = e.currentTarget.value;
        this.setState(() => ({ credentials }));
    }
    handleSubmit(e) {
        e.preventDefault();
        const email = e.currentTarget.querySelector("input[name='email']")
            .value;
        this.props.resetPassword({ email });
    }
    render() {
        if (this.props.isRequestingPassword) return <Loading />;
        if (this.props.hasRequestedPassword)
            return (
                <PasswordRequested
                    email={this.props.email}
                    resetPasswordAgain={this.props.resetPasswordAgain}
                />
            );
        return (
            <div className="mt-password-reset__container">
                <div className="mt-password-reset">
                    <h1>Password Reset</h1>
                    <p>
                        You will receive a link on the email address entered
                        below. <br />
                        Click the link to reset your password.
                    </p>
                    <form onSubmit={this.handleSubmit}>
                        <label>
                            <span>Email address</span>
                            <input
                                type="email"
                                name="email"
                                label="email address"
                                autoComplete="email"
                                placeholder="Enter your email address here"
                                onChange={this.handleChange}
                            />
                        </label>

                        <PrimaryButton>
                            Email me a password reset link
                        </PrimaryButton>
                    </form>
                </div>
            </div>
        );
    }
}

ForgotPassword.propTypes = {
    resetPassword: PropTypes.func,
    hasRequestedPassword: PropTypes.bool,
    isRequestingPassword: PropTypes.bool,
    email: PropTypes.string,
    resetPasswordAgain: PropTypes.func,
};

const mapStateToProps = (state) => ({
    isRequestingPassword: state.userReducer.isRequestingPassword,
    hasRequestedPassword: state.userReducer.hasRequestedPassword,
    email: state.userReducer.email,
});

const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            resetPassword,
            resetPasswordAgain,
        },
        dispatch
    );
export default connect(mapStateToProps, mapDispatchToProps)(ForgotPassword);
