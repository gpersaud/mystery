import React from "react";
import PropTypes from "prop-types";

const AuthFailed = (props) => {
    return(
        <div>
            <h1>Authentication failed</h1>
            <p>{props.err}</p>
        </div>
    );
}; 

AuthFailed.propTypes = {
    err: PropTypes.object
};

export default AuthFailed;
