import React, { useState } from "react";
import PropTypes from "prop-types";
import BookPageDescription from "./BookPageDescription";
import BookAuthor from "../Author/BookAuthor";
import CSSTransition from "react-transition-group/CSSTransition";

const BookPageInfo = ({ book }) => {
    const [toggle, setToggle] = useState(false);
    const handleClick = () => setToggle(!toggle);

    const languages = [book.language1, book.language2];
    const description = [book.description1, book.description2];
    const notes = [book.notes1, book.notes2];

    return (
        <div className="mt-book-page__info" onClick={handleClick}>
            <CSSTransition
                in={toggle}
                appear={toggle}
                timeout={300}
                classNames="fade-big"
            >
                <BookPageDescription
                    lang={languages[Number(toggle)]}
                    alternateLang={languages[Number(!toggle)]}
                    description={description[Number(toggle)]}
                    note={notes[Number(toggle)]}
                />
            </CSSTransition>
            <BookAuthor book={book} />
        </div>
    );
};

BookPageInfo.propTypes = {
    book: PropTypes.object
};

export default BookPageInfo;
