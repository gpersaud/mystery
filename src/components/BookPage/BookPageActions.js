import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import Loading from "../Loading/Loading";
import BuyButton from "../Button/BuyButton";
import Price from "./Price";
import { loginRedirect } from "../../Auth/Auth";
import { doesTheUserOwnTheBook } from "../../utils/helpers";
import ShowReadLink from "./ShowReadLink";
import { Link } from "react-router-dom";

const BookPageActions = (props) => {
    const [ownsIt, setOwnership] = useState(undefined);

    const considerFetchingLibrary = () => {
        // if the book is not found fetch that one book from the server
        const { didFetchLibrary, isAuthenticated, fetchLibrary } = props;
        !didFetchLibrary && isAuthenticated && fetchLibrary();
    };

    const establishOwnership = () => {
        // if the book is not found fetch that one book from the server
        const { book, isAuthenticated, libraryBooks } = props;

        /**
         * Initially "ownsIt" is undefined
         * After the check the ownsIt is either true or false
         */

        // if the user is logged in check if they already own the book
        const result = doesTheUserOwnTheBook(
            isAuthenticated,
            libraryBooks,
            book
        );
        if (result !== undefined) {
            setOwnership(result);
        }

        document.querySelector(".mt-layout__content-inner").scrollTo(0, 0);
    };

    useEffect(considerFetchingLibrary, []);

    useEffect(establishOwnership, [props.libraryBooks.length, props.book._id]);

    /**
     * What action should be taken depends on the book
     * Is it a free book, then immediately add it to the user's library
     * if not take the user to the purchasing process
     */
    const handlePurchase = () => {
        window.gtag("event", "purchase_initiated", {
            event_category: "checkout",
        });
        const { book, match, history, getFreeBook, isAuthenticated } = props;
        isAuthenticated
            ? handleCheckout(book, history, getFreeBook)
            : history.push(loginRedirect(match.url));
    };

    /* if the user is logged in check if they have purchased this book if yes, show read, otherwise buy
     * If the user is not logged in show the login button
     */

    return !props.book || Object.keys(props.book).length < 1 ? (
        <Loading />
    ) : (
        <div>
            <Price price={props.book.price} ownsIt={ownsIt} />

            {ownsIt ? (
                <ShowReadLink {...props} />
            ) : (
                <>
                    <BuyButton
                        isAuthenticated={props.isAuthenticated}
                        price={props.book.price}
                        handlePurchase={handlePurchase}
                    />
                    <br />
                    <Link
                        to={`/book/read/${props.match.params.bookId}`}
                        alt="Open with free reader"
                        className="mt-book-page__header-action-free"
                    >
                        Read free
                    </Link>
                </>
            )}
        </div>
    );
};

BookPageActions.propTypes = {
    book: PropTypes.object,
    match: PropTypes.object,
    fetchLibrary: PropTypes.func,
    purchaseBook: PropTypes.func,
    isAuthenticated: PropTypes.bool,
    libraryBooks: PropTypes.array,
    didFetchLibrary: PropTypes.bool,
    getFreeBook: PropTypes.func,
    history: PropTypes.object,
};

export default BookPageActions;

export const handleCheckout = (book, history, getFreeBook) =>
    book.price > 0
        ? history.push(`/checkout/${book._id}`)
        : getFreeBook(book._id);
