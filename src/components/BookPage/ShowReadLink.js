import React from "react";
import { getLibraryBook } from "../../utils/helpers";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";

const ShowReadLink = (props) => {
    const libEntry = getLibraryBook(
        props.isAuthenticated,
        props.libraryBooks,
        props.book
    );
    const pathname = `/read/${libEntry.item._id}`;
    const pathObject = { pathname };
    return (
        <Link className="mt-cta-button" to={pathObject}>
            read
        </Link>
    );
};

ShowReadLink.propTypes = {
    isAuthenticated: PropTypes.bool,
    libraryBooks: PropTypes.array,
    book: PropTypes.object,
};

export default ShowReadLink;
