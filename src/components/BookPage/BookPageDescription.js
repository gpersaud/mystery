import React from "react";
import PropTypes from "prop-types";
import LanguageName from "../Label/LanguageName";

const BookPageDescription = ({ lang, description, note, alternateLang }) => (
    <div className="mt-book-page__info-item">
        <div className="mt-book-page__info-header">
            <LanguageName code={lang} />
            <span className="mt-book-page__info-header-intro">Description</span>
            &nbsp;&nbsp;
            <sup>
                click for <LanguageName code={alternateLang} />
            </sup>
        </div>
        <div className="mt-book-page__info-item-text" dir="auto">
            {/* <div className="mt-book-page__pulse-container">
                <span className="pulse" />
            </div> */}
            {description}
        </div>
        <div className="mt-book-page__info-header">
            <LanguageName code={lang} />
            <span className="mt-book-page__info-header-intro">Notes</span>
        </div>
        <div className="mt-book-page__info-item-note" dir="auto">
            {note}
        </div>
    </div>
);

BookPageDescription.propTypes = {
    lang: PropTypes.string,
    alternateLang: PropTypes.string,
    description: PropTypes.string,
    note: PropTypes.string,
};

export default BookPageDescription;
