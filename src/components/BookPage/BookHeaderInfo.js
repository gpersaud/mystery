import React from "react";
import PropTypes from "prop-types";

const BookHeaderInfo = ({ titles = [] }) =>
    titles.length > 0 ? (
        <div className="mt-book-page__header-info-container">
            <div className="mt-book-page__header-info">
                <h1 dir="auto">{titles[0]}</h1>
                <h2 dir="auto">{titles[1]}</h2>
            </div>
        </div>
    ) : null;

BookHeaderInfo.propTypes = {
    titles: PropTypes.array
};

export default BookHeaderInfo;
