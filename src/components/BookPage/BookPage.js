import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
    fetchBook,
    purchaseBook,
    getFreeBook,
} from "../../actions/bookActions";
import { fetchLibrary } from "../../actions/libraryActions";
import Loading from "../Loading/Loading";
import Language from "../Label/Language";
import BookPageInfo from "./BookPageInfo";
import BookHeaderInfo from "./BookHeaderInfo";
import BookCover from "../Books/BookCover";
import { fetchBookCover, fetchBookStats } from "../../actions/bookActions";
import { Helmet } from "react-helmet";
import BookStats from "./BookStats";
import BookPagePreview from "./BookPagePreview";
import BookPageActions from "./BookPageActions";
import { getBookTitles, getLanguages } from "../../utils/helpers";
import "../../stylesheets/Book.scss";

export const BookPage = ({ book, ...props }) => {
    const considerFetchingBook = () => {
        const { match } = props;

        (!book || book.id !== match.params.bookId) &&
            props.fetchBook(match.params.bookId, 2);

        /**
         * Keep track of the number of users visiting a given book page
         */
        window.gtag("event", "view_item");
    };

    useEffect(considerFetchingBook, [props.match.params.bookId]);

    if (!book || Object.keys(book).length < 1) return <Loading />;

    const titles = getBookTitles(book) || ["title1", "title2"];
    const languages = getLanguages(book) || ["language1", "language2"];

    return (
        <div className="mt-book-page__container">
            <Helmet>
                <title>{book.title1}</title>
            </Helmet>
            <Language languages={languages} />
            <div className="mt-book-page__header-container">
                <BookHeaderInfo
                    titles={titles}
                    bookId={props.match.params.bookId}
                />

                <div className="mt-book-page__header-action">
                    <BookCover
                        book={book}
                        fetchBookCover={props.fetchBookCover}
                        large={true}
                        covers={props.covers}
                    />
                    <BookPageActions {...props} book={book} />
                </div>
            </div>
            <BookStats {...props} _id={book._id} />
            <BookPageInfo book={book} />
            <BookPagePreview book={book} />
        </div>
    );
};

BookPage.propTypes = {
    history: PropTypes.object,
    books: PropTypes.array.isRequired,
    book: PropTypes.object,
    match: PropTypes.object,
    fetchBook: PropTypes.func,
    fetchLibrary: PropTypes.func,
    purchaseBook: PropTypes.func,
    isAuthenticated: PropTypes.bool,
    libraryBooks: PropTypes.array,
    didFetchLibrary: PropTypes.bool,
    getFreeBook: PropTypes.func,
    fetchBookStats: PropTypes.func,
    fetchBookCover: PropTypes.func,
    covers: PropTypes.array,
};

const mapStateToProps = (state) => ({
    books: state.booksReducer.books,
    book: state.booksReducer.selectedBook,
    covers: state.booksReducer.covers,
    message: state.messageReducer.message,
    isAuthenticated: state.userReducer.isLoggedIn,
    libraryBooks: state.libraryReducer.books,
    didFetchLibrary: state.libraryReducer.didFetchLibrary,
    selectedBookStats: state.booksReducer.selectedBookStats,
    isLoadingStats: state.booksReducer.isLoadingStats,
});

const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            fetchBook,
            purchaseBook,
            fetchLibrary,
            getFreeBook,
            fetchBookCover,
            fetchBookStats,
        },
        dispatch
    );

export default connect(mapStateToProps, mapDispatchToProps)(BookPage);
