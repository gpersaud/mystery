import React from "react";
import PropTypes from "prop-types";

class BookStats extends React.Component {
    componentDidMount() {
        const { _id, fetchBookStats, selectedBookStats } = this.props;
        if (!(selectedBookStats && selectedBookStats.length > 0) && _id) {
            fetchBookStats(_id);
        }
    }

    componentDidUpdate() {
        const {
            _id,
            fetchBookStats,
            isLoadingStats,
            selectedBookStats,
        } = this.props;
        if (
            !isLoadingStats &&
            !(selectedBookStats && selectedBookStats.length > 0) &&
            _id
        ) {
            fetchBookStats(_id);
        }
    }
    render() {
        const { selectedBookStats = [], isLoadingStats } = this.props;
        if (isLoadingStats) return <div>loading...</div>;
        if (!(selectedBookStats && selectedBookStats.length > 0)) return null;
        selectedBookStats.sort((a, b) => (a.label > b.label ? 1 : -1));
        return (
            <div className="mt-book-page__stats">
                {selectedBookStats.map(({ label, value }, idx) => (
                    <StatItem label={label} key={idx + label + value}>
                        {value}
                    </StatItem>
                ))}
            </div>
        );
    }
}

BookStats.propTypes = {
    _id: PropTypes.string,
    selectedBookStats: PropTypes.array.isRequired,
    fetchBookStats: PropTypes.func,
    isLoadingStats: PropTypes.bool,
};

export default BookStats;

const StatItem = ({ label, children }) => (
    <div className="mt-book-page__stats-item">
        <div className="mt-book-page__stats-item-value">{children}</div>
        <label className="mt-book-page__stats-item-label">{label}</label>
    </div>
);

StatItem.propTypes = {
    label: PropTypes.string,
    children: PropTypes.any,
};
