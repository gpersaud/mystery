export default {
    history: {
        length: 5,
        action: "POP",
        location: '{hash: "", key: "ob8st0", pathname: "/book/5e2450a6…}',
        createHref: "createHref",
        push: "push",
        replace: "replace",
        go: "go",
        goBack: "goBack",
        goForward: "goForward",
        block: "block",
        listen: "listen"
    },
    location: {
        pathname: "/book/5e2450a61d062900156c3c31",
        search: "",
        hash: "",
        state: '{sender: "bookItem"}',
        key: "ob8st0"
    },
    match: {
        path: "/book/:bookId",
        url: "/book/5e2450a61d062900156c3c31",
        isExact: true,
        params: '{bookId: "5e2450a61d062900156c3c31"}'
    },
    books: ['{_id: "5e2450a61d062900156c3c31", __v: 5, authors: …}'],
    book: {
        _id: "5e2450a61d062900156c3c31",
        languages: ["nl", "en"],
        isbn: '["com.mytoori.book.Nederlandsegezegden", "com.mytoo…]',
        titles: "[{…}, {…}]",
        userId: "5PP4hKr7vKNWMrCNa",
        descriptions: "[{…}, {…}]",
        notes: "[{…}, {…}]",
        id: "5e2450a61d062900156c3c38",
        covers: "[]",
        coverImages: "[{…}]",
        authors: "[]",
        translators: "[]",
        chapters: "[{…}]",
        __v: 5,
        status: "published"
    },
    covers: [],
    message: {
        level: "error",
        message: "this is a test from the reducer"
    },
    isAuthenticated: true,
    libraryBooks: [
        '{item: {…}, isArchived: false, mark: "undefined", p…}',
        '{item: {…}, isArchived: false, mark: "undefined", p…}',
        '{item: {…}, isArchived: false, mark: "undefined", p…}',
        '{item: {…}, isArchived: false, mark: "undefined", p…}',
        '{item: {…}, isArchived: false, mark: "undefined", p…}',
        '{item: {…}, isArchived: false, purchaseDate: "2019-…}',
        '{item: {…}, isArchived: false, purchaseDate: "2019-…}',
        '{item: {…}, isArchived: false, purchaseDate: "2019-…}',
        '{item: {…}, isArchived: false, purchaseDate: "2019-…}',
        '{item: {…}, isArchived: false, purchaseDate: "2019-…}',
        '{item: {…}, isArchived: false, purchaseDate: "2019-…}',
        '{item: {…}, isArchived: false, purchaseDate: "2019-…}',
        '{item: {…}, isArchived: false, purchaseDate: "2020-…}',
        '{item: {…}, isArchived: false, purchaseDate: "2020-…}',
        '{item: {…}, isArchived: false, purchaseDate: "2020-…}'
    ],
    selectedBookStats: [
        '{label: "chapters", value: 1}',
        '{label: "pages", value: 1}',
        '{label: "words", value: 22}'
    ],
    isLoadingStats: false,
    fetchBook: "",
    purchaseBook: "",
    fetchLibrary: "",
    getFreeBook: "",
    fetchBookCover: "",
    fetchBookStats: ""
};
