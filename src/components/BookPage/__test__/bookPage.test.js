import React from "react";
import { BookPage } from "../BookPage";
import bookPageProps from "../__mock___/bookPage.mock";
import { shallow } from "enzyme";

describe("Book Page", () => {
    const fetchBook = jest.fn();
    const fetchLibrary = jest.fn();
    const purchaseBook = jest.fn();
    const getFreeBook = jest.fn();
    const fetchBookStats = jest.fn();
    const fetchBookCover = jest.fn();

    const props = {
        ...bookPageProps,
        fetchBook,
        fetchLibrary,
        purchaseBook,
        getFreeBook,
        fetchBookCover,
        fetchBookStats,
        book: {
            coverImages: []
        }
    };

    it("should match the snapshot", () => {
        const wrapper = shallow(<BookPage {...props} />);
        expect(wrapper).toMatchSnapshot();
    });
});
