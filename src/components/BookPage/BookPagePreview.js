import React from "react";
import ReaderText from "../Reader/ReaderText";
import PropTypes from "prop-types";

const BookPagePreview = ({ book: { chapters = [] } = {} }) => {
    return (
        <div className="mt-preview__container">
            <header>
                Preview
                <p>
                    These are the two books you will receive with your purchase.{" "}
                    <strong>Click the text to try it out.</strong>
                    <br />
                    Bookmarks will not be saved and you will receive a much
                    better reading experience with your purchase.
                </p>
            </header>
            <div className="mt-preview__text-container">
                <div className="mt-preview__text">
                    {chapters &&
                        chapters.map((chapter) =>
                            chapter.paragraphs.map((p) => (
                                <ReaderText
                                    key={`paragraph-${p.id || p._id}`}
                                    id={p.id || p._id}
                                    texts={[p.text1, p.text2]}
                                    inverted={false}
                                />
                            ))
                        )}
                </div>
                <div className="mt-preview__text">
                    {chapters &&
                        chapters.map((chapter) =>
                            chapter.paragraphs.map((p) => (
                                <ReaderText
                                    key={`paragraph-${p.id || p._id}`}
                                    id={p.id || p._id}
                                    texts={[p.text1, p.text2]}
                                    inverted={true}
                                />
                            ))
                        )}
                </div>
            </div>
        </div>
    );
};

BookPagePreview.propTypes = {
    book: PropTypes.object,
};

export default BookPagePreview;
