import React from "react";
import PropTypes from "prop-types";

/**
 * Price component either shows the price or the free Label
 * If the user already owns the book, the user can immediately read the book
 * @param {number} price  The price of the books
 * @param {boolean} ownsIt Boolean indicating if the user already owns the book
 */
const Price = ({ price, ownsIt }) => (
    <div className="mt-book-page__header-action-price">
        {price > 0 ? (
            <span className={ownsIt ? "owns-it" : ""}>
                &#8364;&nbsp;{price}
            </span>
        ) : (
            <span>Free</span>
        )}
    </div>
);
export default Price;

Price.propTypes = {
    price: PropTypes.number,
    ownsIt: PropTypes.bool
};
