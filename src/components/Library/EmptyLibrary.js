import React from "react";
import { Link } from "react-router-dom";

const EmptyLibrary = () => (
    <div className="mt-library__empty-container">
        <h1>Empty Library</h1>
        <p>
            Visit the{"  "}
            <Link to="/books/featured">bookshop</Link>
            {"  "}
            to purchase or try out some of our{" "}
            <Link to="/books/free">free</Link> books
        </p>
    </div>
);

export default EmptyLibrary;
