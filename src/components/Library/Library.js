import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { fetchLibrary } from "../../actions/libraryActions";
import LibraryEntry from "./LibraryItem";
import EmptyLibrary from "./EmptyLibrary";
import Loading from "../Loading/Loading";
import { Helmet } from "react-helmet";
import "../../stylesheets/Library.scss";

class Library extends React.Component {
    componentDidMount() {
        const { books, fetchLibrary } = this.props;
        !(books && books.length > 0) && fetchLibrary();
        // scroll to the previously selected book
        const closeBook = document.getElementById(this.props.match.params.id);
        if (closeBook) closeBook.scrollIntoView();
    }
    render() {
        if (this.props.isLoading) return <Loading />;
        const { books = [] } = this.props;
        if (books.length < 1) return <EmptyLibrary />;
        return (
            <div className="mt-library__container">
                <Helmet>
                    <title>Your personal bilingual library</title>
                </Helmet>
                <h1 className="mt-library__header">
                    {books.length} Books in your library
                </h1>
                <div className="mt-library__grid">
                    {books &&
                        books.map((libraryItem, idx) => (
                            <LibraryEntry
                                key={libraryItem.item.isbn + idx}
                                {...this.props}
                                libraryItem={libraryItem}
                            />
                        ))}
                </div>
            </div>
        );
    }
}

Library.propTypes = {
    fetchLibrary: PropTypes.func,
    books: PropTypes.array,
    isLoggedIn: PropTypes.bool,
    isLoading: PropTypes.bool,
    match: PropTypes.object
};

const mapStateToProps = state => ({
    books: state.libraryReducer.books,
    covers: state.libraryReducer.covers,
    isLoggedIn: state.userReducer.isLoggedIn,
    isLoading: state.libraryReducer.isLoading
});
const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            fetchLibrary
        },
        dispatch
    );
export default connect(mapStateToProps, mapDispatchToProps)(Library);
