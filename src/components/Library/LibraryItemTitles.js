import React from "react";
import PropTypes from "prop-types";

const LibraryItemTitles = ({ titles = [] }) => {
    return titles.map((title, idx) => (
        <div key={title.value + idx}>{title.value}</div>
    ));
};

LibraryItemTitles.propTypes = {
    titles: PropTypes.array
};

export default LibraryItemTitles;
