import React from "react";
import PropTypes from "prop-types";
import BookItemTitle from "../Books/BookItemTitle";
import LanguageLabel from "../Label/Language";

const getBookId = ({ item }) => {
    const { id } = item;
    return id;
};

class LibraryEntry extends React.Component {
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }
    handleClick() {
        const { libraryItem } = this.props;
        const { item } = libraryItem;
        this.props.history.push(`/read/${item._id}`);
    }
    render() {
        const { libraryItem } = this.props;
        const item = libraryItem.item || {};
        const bookTitles = [...item.titles].reverse();
        return (
            <div
                className="mt-library__item"
                onClick={this.handleClick}
                id={getBookId(libraryItem)}
            >
                <LanguageLabel languages={[item.language1, item.language2]} />
                <div className="mt-library__item-info">
                    {bookTitles.map((title, idx) => (
                        <BookItemTitle
                            key={`library-bookItemTitle-${title.value}-${title.lang}-${idx}`}
                            number={idx}
                        >
                            {title.value}
                        </BookItemTitle>
                    ))}
                </div>
            </div>
        );
    }
}

LibraryEntry.propTypes = {
    libraryItem: PropTypes.object,
    fetchBookCover: PropTypes.func,
    history: PropTypes.object
};

export default LibraryEntry;
