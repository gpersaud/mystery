export default [
    {
        name: "Shop",
        path: "/books/featured"
    },
    {
        name: "My Library",
        path: "/library"
    }
];
