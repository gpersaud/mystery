import React from "react";
import { NavLink } from "react-router-dom";
import routesConfig from "./routeConfig";

const Navigation = () => (
    <nav className="mt-navigation__nav">
        <ul>
            {routesConfig.map(({ path, name }, idx) => (
                <li key={path + name + idx}>
                    <NavLink
                        to={path}
                        activeClassName="mt-navigation__nav--active"
                    >
                        {name}
                    </NavLink>
                </li>
            ))}
        </ul>
    </nav>
);

export default Navigation;
