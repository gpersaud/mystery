import React from "react";
import PropTypes from "prop-types";
import { Route, Redirect } from "react-router-dom";
import { isAuthenticated } from "../Auth/Auth";
import { connect } from "react-redux";

/**
 * If the user has an valid JWT token,
 * they can continue.
 * The validity of the token is checked by the back-end service.
 * The data request will fail if the token is not valid.
 *
 * The user should be redirected as soon as
 * the auth is no longer valid.
 * This happens by listening to the reducer,
 * even though the provided variable is never used
 * @param {*} param0
 */
const PrivateRoute = ({ component: Component, ...props }) => (
    <Route
        {...props}
        render={props =>
            isAuthenticated() ? (
                <Component {...props} />
            ) : (
                <Redirect
                    to={{
                        pathname: "/login",
                        state: { from: props.location }
                    }}
                />
            )
        }
    />
);

PrivateRoute.propTypes = {
    component: PropTypes.oneOfType([PropTypes.func, PropTypes.object]),
    auth: PropTypes.object,
    location: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    isLoggedIn: PropTypes.bool
};

const mapStateToProps = state => ({
    isLoggedIn: state.userReducer.isLoggedIn
});

export default connect(mapStateToProps)(PrivateRoute);
