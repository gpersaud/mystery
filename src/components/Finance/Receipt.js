import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { fetchOrder } from "../../actions/orderActions";
import ReceiptHeader from "./ReceiptHeader";
import ReceiptInfo from "./ReceiptInfo";
import { fetchBook, fetchBookCover } from "../../actions/bookActions";
import { isAuthenticated } from "../../Auth/Auth";
import Alert from "react-s-alert";
class Receipt extends React.Component {
    render() {
        if (!isAuthenticated) {
            Alert.warning("You are no longer logged in. Please login again to continue.");
            this.props.history.push("/login");
        }
        return (
            <div className="mt-receipt-container">
                <ReceiptHeader receiptNumber={this.props.match.params.id} />
                <ReceiptInfo
                    {...this.props}
                    receiptNumber={this.props.match.params.id}
                />
            </div>
        );
    }
}
Receipt.propTypes = {
    match: PropTypes.object,
    fetchOrder: PropTypes.func,
    order: PropTypes.object,
    history: PropTypes.object
};

const mapStateToProps = state => ({
    order: state.orderReducer.order,
    orders: state.orderReducer.orders,
    isFetchingOrder: state.orderReducer.isFetchingOrder,
    book: state.booksReducer.selectedBook,
    covers: state.booksReducer.covers
});

const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            fetchOrder,
            fetchBook,
            fetchBookCover
        },
        dispatch
    );

export default connect(mapStateToProps, mapDispatchToProps)(Receipt);
