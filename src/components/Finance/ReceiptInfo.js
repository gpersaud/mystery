import React from "react";
import PropTypes from "prop-types";
import BookCover from "../Books/BookCover";
import { format } from "date-fns";
import { Link } from "react-router-dom";

class ReceiptInfo extends React.Component {
    UNSAFE_componentWillMount() {
        const { receiptNumber, fetchOrder } = this.props;
        fetchOrder(receiptNumber);
    }
    render() {
        const { orders = [], isFetchingOrder, receiptNumber } = this.props;

        if (isFetchingOrder || !orders || orders.length < 1)
            return <div>loading...</div>;

        const order = orders.find((order) => order._id === receiptNumber);
        if (!order) return null;
        const { payments = [] } = order;
        return (
            <div>
                <div className="mt-receipt__info-container">
                    <div>
                        {format(
                            order.date.created,
                            "dddd Do of MMMM YYYY HH:mm"
                        )}
                    </div>
                    <ReceiptThanks order={order} />
                    <ReceiptTitles order={order} />
                    <ReceiptDetails
                        {...this.props}
                        bookId={order.item._id}
                        order={order}
                    />

                    {payments.map((payment, idx) => (
                        <ReceiptPaymentInfo
                            key={payment + idx}
                            payment={payment}
                        />
                    ))}

                    <PurchasedBooksInfo order={order} />
                    <OpenPurchasedBook order={order} />
                </div>
            </div>
        );
    }
}

ReceiptInfo.propTypes = {
    receiptNumber: PropTypes.string,
    fetchOrder: PropTypes.func,
    orders: PropTypes.array,
    isFetchingOrder: PropTypes.bool,
    order: PropTypes.object,
};

export default ReceiptInfo;

class ReceiptDetails extends React.Component {
    componentDidMount() {
        const { book, fetchBook, bookId } = this.props;
        if (!book) fetchBook(bookId);
    }
    render() {
        if (!this.props.book) return null;
        const { order } = this.props;
        const { item } = order;
        if (!(order && item)) return null;
        const { titles = [] } = item;
        return (
            <div className="mt-receipt-info">
                <BookCover {...this.props} />
                <div className="mt-receipt-info__detail">
                    {titles.map((title, idx) => (
                        <div key={title.value + idx}>
                            <span className="mt-receipt__language-label">
                                {title.lang}
                            </span>
                            <span>{title.value}</span>
                        </div>
                    ))}
                </div>
            </div>
        );
    }
}

ReceiptDetails.propTypes = {
    book: PropTypes.object,
    fetchBook: PropTypes.func,
    bookId: PropTypes.string,
    order: PropTypes.object,
};

const ReceiptPaymentInfo = ({ payment }) => (
    <table className="mt-receipt__payment-info">
        <tbody>
            <tr>
                <td>Updated</td>
                <td>
                    {format(payment.paidDatetime, "dddd Do of MMMM YYYY HH:mm")}
                </td>
            </tr>
            <tr>
                <td>Price</td>
                <td>€ {payment.amount}</td>
            </tr>
            <tr>
                <td>Status</td>
                <td>{payment.status}</td>
            </tr>
        </tbody>
    </table>
);

ReceiptPaymentInfo.propTypes = {
    payment: PropTypes.object,
    order: PropTypes.object,
};

const ReceiptThanks = ({ order }) => {
    return order.status === "paid" ? (
        <h1>Thank you for your purchase</h1>
    ) : (
        <div>
            <h2>Oops, something went wrong</h2>
            <p>
                You have not been charged. <br />
                <Link to={`/book/${order.item.titles[0].value}`}>
                    Please try again
                </Link>{" "}
                or contact us at{" "}
                <a href="mailto:info@mytoori.com">info@mytoori.com</a>
            </p>
        </div>
    );
};

ReceiptThanks.propTypes = {
    order: PropTypes.object,
};

const ReceiptTitles = ({ order }) => {
    const { titles = [] } = order.item;
    const nodes = titles.map(({ lang = "", value }, idx) => (
        <div className="mt-receipt__title" key={lang + value + idx}>
            {value} - {lang.toUpperCase()}
        </div>
    ));
    return <div className="mt-receipt__title-container">{nodes}</div>;
};

ReceiptTitles.propTypes = {
    order: PropTypes.object,
};

const PurchasedBooksInfo = ({ order }) => {
    if (order.status !== "paid") return null;
    return (
        <div className="mt-receipt__info-text">
            Purchased books are automatically stored in your personal{" "}
            <Link to="/library">library</Link>. When you open mytoori again,
            simply login to your library to pick up where you left off.
        </div>
    );
};

PurchasedBooksInfo.propTypes = {
    order: PropTypes.object,
};

const OpenPurchasedBook = ({ order = {} }) => {
    if (order.status !== "paid") return null;
    return (
        <Link to={`/read/${order.item._id}`} className="btn mt-receipt__button">
            Read
        </Link>
    );
};

OpenPurchasedBook.propTypes = {
    order: PropTypes.object,
};
