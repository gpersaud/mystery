import React from "react";
import PropTypes from "prop-types";

const ReceiptHeader = ({ receiptNumber }) => (
    <div className="mt-receipt__header">
        <div>Receipt #: {receiptNumber}</div>
    </div>
);
ReceiptHeader.propTypes = {
    receiptNumber: PropTypes.string
};

export default ReceiptHeader;
