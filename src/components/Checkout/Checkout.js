import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { StripeProvider, Elements } from "react-stripe-elements";
import CheckoutForm from "./CheckoutForm";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { fetchBook, resetPurchaseState } from "../../actions/bookActions";
import { purchaseStripeBook } from "../../actions/orderActions";
import CheckoutSuccess from "./CheckoutSuccess";
import "../../stylesheets/Checkout.scss";

const Checkout = (props) => {
    useEffect(() => {
        window.gtag("event", "purchase_started", {
            event_category: "checkout",
        });
    }, []);

    const cancelCheckout = () => {
        window.gtag("event", "purchase_cancelled", {
            event_category: "checkout",
        });
        props.history.go(-1);
    };

    if (props.purchaseSuccess) return <CheckoutSuccess {...props} />;

    const { REACT_APP_STRIPE_API_KEY } = process.env;
    return (
        <div>
            <StripeProvider apiKey={REACT_APP_STRIPE_API_KEY}>
                <Elements>
                    <CheckoutForm {...props} />
                </Elements>
            </StripeProvider>
            <div className="mt-checkout__cancel-container">
                <button onClick={cancelCheckout}>cancel</button>
            </div>
        </div>
    );
};

Checkout.propTypes = {
    isPurchasing: PropTypes.bool,
    purchaseSuccess: PropTypes.bool,
    book: PropTypes.object,
    match: PropTypes.object,
    history: PropTypes.object,
};

const mapStateToProps = (state) => ({
    isLoading: state.booksReducer.isLoading,
    book: state.booksReducer.selectedBook,
    isPurchasing: state.orderReducer.isPurchasing,
    purchaseSuccess: state.orderReducer.purchaseSuccess,
    isbn: state.orderReducer.purchasedBook,
});

const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            purchaseStripeBook,
            fetchBook,
            resetPurchaseState,
        },
        dispatch
    );

export default connect(mapStateToProps, mapDispatchToProps)(Checkout);
