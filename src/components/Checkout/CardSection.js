import React from "react";
import {
    CardNumberElement,
    CardExpiryElement,
    CardCVCElement
} from "react-stripe-elements";

const styles = { base: { fontSize: "22px" } };
const CardSection = () => {
    return (
        <div className="mt-checkout__card-container">
            <div>
                <label htmlFor="cc-name">name on card</label>
                <br />
                <input
                    type="text"
                    name="name"
                    id="cc_name"
                    placeholder="J. Pulitzer"
                    aria-label="credit card holder name"
                />
            </div>
            <div>
                <label htmlFor="cc_number">Card Number</label>
                <CardNumberElement style={styles} id="cc_number" />
            </div>
            <div>
                <label htmlFor="cc_expiration">Expiration date</label>
                <CardExpiryElement style={styles} id="cc_expiration" />
            </div>
            <div>
                <label htmlFor="cc_cvc">CVC code on back of card</label>
                <CardCVCElement id="cc_cvc" style={styles} />
            </div>
        </div>
    );
};

export default CardSection;
