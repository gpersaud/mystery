import React from "react";
import PropTypes from "prop-types";
import { injectStripe } from "react-stripe-elements";
import CardSection from "./CardSection";
import Alert from "react-s-alert";
import * as Sentry from "@sentry/browser";
import Loading from "../Loading/Loading";

export class CheckoutForm extends React.Component {
    componentDidMount() {
        // get the book that is being purchased
        const { book } = this.props;
        !(book && book._id) &&
            this.props.fetchBook(this.props.match.params.bookId);
    }

    componentDidCatch(error, errorInfo) {
        Sentry.withScope((scope) => {
            scope.setExtras(errorInfo);
            Sentry.captureException(error);
        });
    }

    handleSubmit = async (e) => {
        e.preventDefault();
        const nameOnCard = e.currentTarget.querySelector("input[name='name']")
            .value;
        if (!nameOnCard)
            return Alert.error("A name is required to process the payment");
        const { book, stripe } = this.props;
        // const data = await startPaymentProcess(book, name, stripe);
        // Step 3: handle challenge
        // TODO: fix this, it needs to go through the actions with redux
        // const response = handlePaymentChallenge(data, stripe);
        this.props.purchaseStripeBook(book._id, nameOnCard, stripe);
        return false; // prevent the form from actually submitting
    };
    render() {
        const { book, isPurchasing } = this.props;
        if (!(book && book._id)) return null;
        const { titles = [] } = book;
        return (
            <form className="mt-checkout__form" onSubmit={this.handleSubmit}>
                <h1>Checkout</h1>
                <div className="mt-checkout__items-container">
                    {titles.map((title, idx) => (
                        <div
                            className="mt-checkout__book-title"
                            key={idx + title.value}
                        >
                            {title.value}
                        </div>
                    ))}
                    <div className="mt-checkout__total">
                        Total:&nbsp;&#8364;&nbsp;{book.price}
                    </div>
                </div>
                <CardSection />
                <div className="mt-checkout__action-container">
                    {isPurchasing ? (
                        <Loading />
                    ) : (
                        <button
                            className="mt-shared__button"
                            data-type="primary"
                        >
                            Pay &nbsp;&#8364;&nbsp;{book.price}
                        </button>
                    )}
                </div>
            </form>
        );
    }
}

CheckoutForm.propTypes = {
    handleSubmit: PropTypes.func,
    stripe: PropTypes.object,
    purchaseStripeBook: PropTypes.func,
    match: PropTypes.object,
    fetchBook: PropTypes.func,
    book: PropTypes.object,
    isPurchasing: PropTypes.bool,
};
export default injectStripe(CheckoutForm);
