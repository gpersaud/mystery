import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

class CheckoutSuccess extends React.Component {
    componentWillUnmount() {
        // update the reducer for the next purchase
        this.props.resetPurchaseState();
        window.gtag("event", "purchase_completed", {
            book: this.props.isbn,
            event_category: "checkout",
        });
    }
    render() {
        return (
            <div className="mt-checkout__success-container">
                <h1>Thank you for your purchase</h1>
                <CheckoutSuccessCard {...this.props} />
                <div className="mt-checkout__success-info">
                    Your book has been added to your{" "}
                    <Link to={`/library/${this.props.id}`}>library</Link>. You
                    can alwas access it from there. Bookmarks are created
                    everytime you translate a piece of text.{" "}
                </div>
            </div>
        );
    }
}

CheckoutSuccess.propTypes = {
    resetPurchaseState: PropTypes.func.isRequired,
    isbn: PropTypes.string,
    id: PropTypes.string,
};

export default CheckoutSuccess;

const CheckoutSuccessCard = ({ book }) => (
    <div className="mt-checkout__success-card">
        <div className="mt-checkout__success-card-titles">
            {book &&
                book.titles &&
                book.titles.map(({ lang, value }, idx) => (
                    <div key={idx + value}>
                        <label>{lang}</label> - {value}
                    </div>
                ))}
        </div>
        <Link to={`/read/${book._id}`}>read</Link>
    </div>
);

CheckoutSuccessCard.propTypes = {
    book: PropTypes.object,
};
