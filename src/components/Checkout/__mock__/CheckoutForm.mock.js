export const props = {
    history: {
        length: 8,
        action: "PUSH",
        location: "Object",
        createHref: "createHref()",
        push: "push()",
        replace: "replace()",
        go: "go()",
        goBack: "goBack()",
        goForward: "goForward()",
        block: "block()",
        listen: "listen()"
    },
    location: {
        pathname: "/checkout/5ceda4adace87fd1c4a17b70",
        search: "",
        hash: "",
        key: "b6jlyn"
    },
    match: {
        path: "/checkout/:bookId",
        url: "/checkout/5ceda4adace87fd1c4a17b70",
        isExact: true,
        params: "Object"
    },
    isLoading: false,
    book: {
        dates: {
            published: "2017-12-14T11:00:00.000Z",
            updated: "2018-01-11T12:00:00.000Z"
        },
        languages: [],
        isbn: ["com.mytoori.poem.thepoetryoflife"],
        _id: "5ceda4adace87fd1c4a17b70",
        author: "Emanuel Borges",
        title1: "A Poesia da Vida",
        title2: "The Poetry of Life",
        description1:
            "Um livro de poema que retrata a vida como ela é,e funciona como quer.Um livro que mostra a realidade natural e cultural,cuja outra não há igual.Um livro que te ensina a viver,te faz refletir, te ensina e te faz amor sentir.",
        description2:
            'A book which shows the life how it is,and works how wants.A book which shows the natural and cultural realilty,that do not have equality.A book which teaches how to live, meditate and put you in "Love State"',
        notes1:
            "Emanuel Borges Alves Menezes\nEu sou brasileiro e estudei Inglês por três anos e agora eu estou cursando o Ensino Médio.",
        notes2:
            "Emanuel Borges Alves Menezes\nI'm Brazilian and studied english for three years and now I'm coursing the high school.",
        language1: "pt",
        language2: "EN",
        userId: "jo29XqmA4mAQiCFuE",
        cover: "28117181962",
        status: "published",
        authorFirstname: "Emanuel",
        authorLastname: "Borges",
        translatorFirstname: "Emanuel",
        translatorLastname: "Borges",
        cover2: "25559832321",
        isbn2: "",
        titles: [
            {
                lang: "pt",
                value: "A Poesia da Vida"
            },
            {
                lang: "EN",
                value: "The Poetry of Life"
            }
        ],
        descriptions: [
            {
                lang: "pt",
                value:
                    "Um livro de poema que retrata a vida como ela é,e funciona como quer.Um livro que mostra a realidade natural e cultural,cuja outra não há igual.Um livro que te ensina a viver,te faz refletir, te ensina e te faz amor sentir."
            },
            {
                lang: "EN",
                value:
                    'A book which shows the life how it is,and works how wants.A book which shows the natural and cultural realilty,that do not have equality.A book which teaches how to live, meditate and put you in "Love State"'
            }
        ],
        notes: [
            {
                lang: "pt",
                value:
                    "Emanuel Borges Alves Menezes\nEu sou brasileiro e estudei Inglês por três anos e agora eu estou cursando o Ensino Médio."
            },
            {
                lang: "EN",
                value:
                    "Emanuel Borges Alves Menezes\nI'm Brazilian and studied english for three years and now I'm coursing the high school."
            }
        ],
        covers: [
            {
                lang: "pt",
                value: "28117181962"
            },
            {
                lang: "EN",
                value: "25559832321"
            }
        ],
        translators: [
            {
                firstName: "Emanuel",
                lastName: "Borges"
            }
        ],
        price: 3,
        authors: [
            {
                firstName: "Emanuel",
                lastName: "Borges"
            }
        ],
        coverImages: [
            {
                _id: "5d0e45c513056484315f92f5",
                value:
                    "https://res.cloudinary.com/mytoori/image/upload/v1560608643/mytoori/covers/28117181962_1ffa870fcb_o.png.png"
            }
        ],
        id: "5dafeb978d5c210015bcc492"
    },
    isPurchasing: false,
    isbn: null,
    purchaseStripeBook: "fn()",
    fetchBook: "fn()",
    resetPurchaseState: "fn()",
    stripe: {
        elements: "fn()",
        createToken: "fn()",
        createSource: "fn()",
        retrieveSource: "fn()",
        paymentRequest: "fn()",
        _apiKey: "pk_test_nEWQQabdEP6G02wHP78xwqdg",
        _keyMode: "test",
        _locale: null,
        _betas: "Array[0]",
        _stripeAccount: null,
        _isCheckout: false,
        _controller: "Object",
        createPaymentMethod: "fn()",
        retrievePaymentIntent: "fn()",
        retrieveSetupIntent: "fn()",
        handleCardAction: "fn()",
        handleCardPayment: "fn()",
        confirmPaymentIntent: "fn()",
        handleCardSetup: "fn()",
        confirmSetupIntent: "fn()",
        fulfillPaymentIntent: "fn()",
        handleSepaDebitPayment: "fn()",
        handleSepaDebitSetup: "fn()",
        handleIdealPayment: "fn()",
        handleFpxPayment: "fn()",
        redirectToCheckout: "fn()"
    }
};
