import React from "react";

const AddressSection = () => {
    return(
        <div>
            <label>
                <div>Address</div>
                <input type="text" name="address" placeholder="smart street 101"/>
            </label>
        </div>
    );
};

export default AddressSection;
