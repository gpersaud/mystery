import React from "react";
import { mount, shallow } from "enzyme";
import { CheckoutForm } from "../CheckoutForm";
import { props } from "../__mock__/CheckoutForm.mock";

describe("The checkout form", () => {
    let wrapper;
    beforeEach(() => {
        props.purchaseStripeBook = jest.fn();
        props.fetchBook = jest.fn();
        props.resetPurchaseState = jest.fn();
        wrapper = shallow(<CheckoutForm {...props} />);
    });
    it("should render the form", () => {
        expect(wrapper).toBeDefined();
    });

    it("should have all required fields", () => {
        expect(wrapper).toMatchSnapshot();
    });
});

describe("get the book on checkout", () => {
    it("should request the book if it's not there", () => {
        const updatedProps = {
            ...props,
            book: null,
            purchaseStripeBook: jest.fn(),
            fetchBook: jest.fn(),
            resetPurchaseState: jest.fn()
        };
        const wrapper = shallow(<CheckoutForm {...updatedProps} />);
        expect(updatedProps.fetchBook).toHaveBeenCalled();
        // There is no book so the form is not rendered
        expect(wrapper.find("form")).toHaveLength(0);
    });
    it("should request the book if the given object is emtpy", () => {
        const updatedProps = {
            ...props,
            book: {},
            purchaseStripeBook: jest.fn(),
            fetchBook: jest.fn(),
            resetPurchaseState: jest.fn()
        };
        const wrapper = shallow(<CheckoutForm {...updatedProps} />);
        expect(updatedProps.fetchBook).toHaveBeenCalled();
        // There is no book so the form is not rendered
        expect(wrapper.find("form")).toHaveLength(0);
    });
    it("should show the form if the book is there", () => {
        props.purchaseStripeBook = jest.fn();
        props.fetchBook = jest.fn();
        props.resetPurchaseState = jest.fn();
        const wrapper = shallow(<CheckoutForm {...props} />);
        expect(props.fetchBook).toHaveBeenCalledTimes(0);
        expect(wrapper.find("form")).toHaveLength(1);
    });
});

describe("check for alert message on incomplete form", () => {
    it("should show an alert if the user doesn't enter a name", () => {
        props.purchaseStripeBook = jest.fn();
        props.fetchBook = jest.fn();
        props.resetPurchaseState = jest.fn();
        const wrapper = shallow(<CheckoutForm {...props} />);
        expect(wrapper.find("form")).toHaveLength(1);

        const purchaseButton = wrapper.find("button.mt-shared__button");
        expect(purchaseButton).toBeDefined();
        expect(purchaseButton.length).toBe(1);
        purchaseButton.simulate("click");
        expect(wrapper.find(".s-alert-wrapper")).toBeDefined();
    });
});
