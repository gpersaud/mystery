import React, { Component } from "react";
import PropTypes from "prop-types";

class Intro extends Component {
    render() {
        if (this.props.match.params.collection !== "featured") return null;
        return (
            <div className="mt-books__intro">
                <h1>Bilingual Stories</h1>
                <h2>Keep improving your language skills</h2>
            </div>
        );
    }
}

Intro.propTypes = {
    match: PropTypes.object
};

export default Intro;
