import React from "react";
import PropTypes from "prop-types";

const BuyButton = ({ isAuthenticated, price = 0, handlePurchase }) => (
    <button className="mt-cta-button" onClick={handlePurchase}>
        {isAuthenticated ? (price > 0 ? "buy" : "add to library") : "Login"}
    </button>
);

BuyButton.propTypes = {
    isAuthenticated: PropTypes.bool,
    price: PropTypes.number,
    handlePurchase: PropTypes.func
};

export default BuyButton;
