import React from "react";
import { Helmet } from "react-helmet";

const Contact = () => {
    return (
        <div className="mt-page__container">
            <Helmet>
                <title>Contact Mytoori</title>
            </Helmet>
            <h1>Contact</h1>
            <h2>Have a question or want to provide feedback?</h2>
            <div className="mt-contact__link-container">
                <a className="mt-cta-button" href="mailto:info@duolir.com">
                    info@duolir.com
                </a>
            </div>
            <p>
                We would love to hear from you. Let us know what you like about
                the platform and what should be improved.
            </p>
        </div>
    );
};

export default Contact;
