import React from "react";
import PropTypes from "prop-types";
import { Route } from "react-router-dom";
import About from "./About";
import AboutLibrary from "./AboutLibrary";
import AboutShop from "./AboutShop";

const AboutContainer = ({ match }) => (
    <>
        <Route path={match.url + "/library"} component={AboutLibrary} />
        <Route path={match.url + "/shop"} component={AboutShop} />
        <Route exact path={match.url} component={About} />
    </>
);

AboutContainer.propTypes = {
    match: PropTypes.object,
};

export default AboutContainer;
