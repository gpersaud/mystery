import React, { useEffect } from "react";
import { scrollToTop } from "../../utils/helpers";
import { Helmet } from "react-helmet";
import { Link } from "react-router-dom";
import "../../stylesheets/Page.scss";

const About = () => {
    useEffect(() => {
        scrollToTop();
    }, []);
    return (
        <div className="mt-page__container">
            <article>
                <Helmet>
                    <title>About Mytoori</title>
                </Helmet>
                <nav>
                    <ul>
                        <li>
                            <Link to="/about/shop" alt="Shop help page">
                                Shop
                            </Link>
                        </li>
                        <li>
                            <Link to="/about/library" alt="Library help page">
                                Library
                            </Link>
                        </li>
                    </ul>
                </nav>
                <h1>E-books can be so much more</h1>
                <section>
                    <h2>
                        What if you could improve your language skills with fun
                        and engaging stories?
                    </h2>
                    <p>
                        The concept of Mytoori started a long time ago. The
                        goal? Create a novel digital reading experience with a
                        focus on learning and reading.
                        <br />
                        While learning French I wondered if I could improve my
                        vocabulary while reading the French classics. This would
                        require a dictionary at the side to constantly look up
                        all the words I didn’t understand. Not a great
                        experience. Couldn’t that be improved?
                        <br />
                        What if you could blend two versions of a book together?
                        This blend would allow users to switch on a sentence
                        level between languages.
                    </p>
                </section>
                <hr />
                <section>
                    <h2>A bilingual platform for writers and readers</h2>
                    <p>
                        Fast forward several years and the platform now offers
                        bilinguals from anywhere, the chance to create bilingual
                        stories and sell them online. Many today are learning a
                        second language; often required work and travel. With
                        mytoori they are able to capitalize and do more with
                        their the languages they master.
                    </p>
                </section>
            </article>
        </div>
    );
};

export default About;
