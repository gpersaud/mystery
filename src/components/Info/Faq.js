import React, { useEffect } from "react";
import { scrollToTop } from "../../utils/helpers";
import { Helmet } from "react-helmet";
import "../../stylesheets/Page.scss";

const Faq = () => {
    useEffect(() => {
        scrollToTop();
    }, []);
    return (
        <div className="mt-page__container">
            <article className="mt-page">
                <Helmet>
                    <title>FAQ Mytoori</title>
                </Helmet>

                <h1>FAQ</h1>
                <section>
                    <h2>What is Mytoori?</h2>
                    <p>
                        Mytoori is a bilingual book reader. These are books are
                        effectively written in two languages. The text is broken
                        up in short paragraphs. These are typically one or two
                        sentences. As a reader, you can read in the language
                        you&aposre learning. If the text becomes too difficult
                        to read, you can simply tap the &quotparagraph&quot.
                        That paragraph will now switch to the second language.
                        This allows you the reader to quickly understand what is
                        happening in the story.
                    </p>
                </section>
                <hr />
                <section>
                    <h2>A bilingual platform for writers and readers</h2>
                    <p>
                        Fast forward several years and the platform now offers
                        bilinguals from anywhere, the chance to create bilingual
                        stories and sell them online. Many today are learning a
                        second language; often required work and travel. With
                        mytoori they are able to capitalize and do more with
                        their the languages they master.
                    </p>
                </section>
            </article>
        </div>
    );
};

export default Faq;
