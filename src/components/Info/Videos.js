import React from "react";
import { scrollToTop } from "../../utils/helpers";
import { Helmet } from "react-helmet";
class Videos extends React.Component {
    componentDidMount() {
        scrollToTop();
    }
    componentDidUpdate() {
        scrollToTop();
    }
    render() {
        return (
            <div className="mt-page__container">
                <Helmet>
                    <title>Videos about language learning</title>
                </Helmet>
                <h1>About language learning</h1>
                <p>
                    Irina&apos;s funny talk, discusses a casual approach to
                    learning Finnish. The title suggests that it&apos;s the
                    hardest language in the world. Didn&apos;t Icelandic have
                    that particular title?
                </p>
                <div className="mt-page__video-container">
                    <iframe
                        title="Can you learn the hardest language in the world? by Irina Pravet"
                        src="https://www.youtube.com/embed/QBKrXdbAcgo?html5=1"
                        frameBorder="0"
                        allow="autoplay; encrypted-media"
                        allowFullScreen
                    />
                </div>
                <hr />
                <p>
                    Naja studies the brain processes of bilingual babies and
                    highlights the value of code mixing. Great talk for new
                    parents and educators.
                </p>
                <div className="mt-page__video-container">
                    <iframe
                        title="Creating bilingual minds by Naja Ferjan Ramirez"
                        src="https://www.youtube.com/embed/Bp2Fvkt-TRM?rel=0"
                        frameBorder="0"
                        allow="autoplay; encrypted-media"
                        allowFullScreen
                    />
                </div>
            </div>
        );
    }
}

export default Videos;

/**
 *
 */
