import React from "react";
import { Helmet } from "react-helmet";
import { Link } from "react-router-dom";
import "../../stylesheets/Page.scss";

const AboutLibrary = () => (
    <div className="mt-page__container">
        <Helmet>
            <title>About Mytoori library</title>
        </Helmet>
        <article>
            <header>
                <h1>What is the Mytoori library?</h1>
            </header>
            <section>
                <h2>The library</h2>
                <p>
                    If you have purchased or downloaded a free book, these are
                    all stored in your own library. This means you can always
                    access your books from your library.
                </p>
                <p>
                    To start reading, click any of the books in your library.
                    Tap the sentence of the book to see the translation for that
                    book.
                </p>
                <p>
                    In the top rigth corner of the reader, there&aposs a button
                    to switch the complete language of the book.
                </p>
            </section>
            <section>
                <nav>
                    <ul>
                        <Link to="/library" alt="go to Library">
                            open Library
                        </Link>
                    </ul>
                </nav>
            </section>
        </article>
    </div>
);

export default AboutLibrary;
