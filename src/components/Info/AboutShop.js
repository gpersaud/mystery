import React from "react";
import { Helmet } from "react-helmet";
import { Link } from "react-router-dom";
import "../../stylesheets/Page.scss";

const AboutLibrary = () => (
    <div className="mt-page__container">
        <Helmet>
            <title>About Mytoori Bookshop</title>
        </Helmet>
        <article>
            <header>
                <h1>How does Mytoori shop work?</h1>
            </header>
            <section>
                <h2>The books</h2>
                <p>
                    The books listed in the bookshop are from bilingual authors.
                    They have either translated an existing (copyright free)
                    book or created their own.{" "}
                </p>
                <p>
                    When you purchase a book you&aposre getting the book in the
                    two languages that the author has prepared it in
                </p>
            </section>
            <section>
                <nav>
                    <ul>
                        <Link to="/shop" alt="go to the book shop">
                            Go to Bookshop
                        </Link>
                    </ul>
                </nav>
            </section>
        </article>
    </div>
);

export default AboutLibrary;
