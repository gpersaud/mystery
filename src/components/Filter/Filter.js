import React from "react";
import PropTypes from "prop-types";

export default class Filter extends React.Component {
  render() {
    return (
      <div className="mt-language">
        <div className="mt-language-filter mt-language-filter__expanded">
          <FilterItem {...this.props} items={this.props.selectedLanguages} />
          <FilterItem {...this.props} title="topics" />
        </div>
      </div>
    );
  }
}

Filter.propTypes = {
  selectedLanguages: PropTypes.array
};

const FilterItem = ({ title, items = [], options, selectLanguage }) => {
  return (
    <div className="mt-language-filter__cell">
      <div className="mt-language-filter__row">
        <label>{title}</label>
        <div className="mt-language-filter__languages-selected">
          {items.map((item, idx) => {
            const selectedOption = options.find(
              option => option.id === item._id
            );
            return (
              <div key={item + idx}>
                {selectedOption && selectedOption.label}
              </div>
            );
          })}
        </div>
      </div>
      <div className="mt-language-filter__row">
        <label>languages</label>
        <select
          onChange={e =>
            selectLanguage(
              options.find(option => option._id === e.currentTarget.value)
            )
          }
        >
          {options &&
            options.map(option => (
              <option key={option._id} value={option._id}>
                {option.label}
              </option>
            ))}
        </select>
      </div>
    </div>
  );
};

FilterItem.propTypes = {
  title: PropTypes.string,
  items: PropTypes.array,
  options: PropTypes.array.isRequired,
  selectLanguage: PropTypes.func
};

FilterItem.defaultProps = {
  title: "selected"
};
