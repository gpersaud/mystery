import React from "react";

const TermsNotice = () => (
    <div className="mt-register-form__disclaimer">
        By signing up you are consenting to our{" "}
        <a href="/terms" target="_blank">
            terms or service
        </a>
    </div>
);

export default TermsNotice;
