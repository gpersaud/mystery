import React from "react";
import PropTypes from "prop-types";
import * as Sentry from "@sentry/browser";
import { Link } from "react-router-dom";
import Loading from "../Loading/Loading";

class LoginForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            credentials: {},
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    componentDidCatch(error, errorInfo) {
        Sentry.withScope((scope) => {
            scope.setExtras(errorInfo);
            Sentry.captureException(error);
        });
    }

    onChange(e) {
        e.preventDefault();
        let { credentials } = this.state;
        credentials[e.currentTarget.name] = e.currentTarget.value;
        this.setState(() => ({ credentials }));
    }

    handleSubmit(e) {
        e.preventDefault();
        window.gtag("event", "login", {
            event_category: "access",
            event_label: "login",
        });
        this.props.action(this.state.credentials);
    }
    render() {
        return (
            <div className="mt-login-form">
                {this.props.isLoading ? (
                    <Loading />
                ) : (
                    <FormComponent
                        {...this.props}
                        handleChange={this.onChange}
                        handleSubmit={this.handleSubmit}
                    />
                )}
                <div className="mt-login-form__alternative">
                    <p>
                        Don&apost have an account yet?
                        <Link to="/signup">Signup</Link> (It&aposs free)
                    </p>
                    <p>
                        Forgot your password?{" "}
                        <Link to="/password">Reset password.</Link>
                    </p>
                </div>
            </div>
        );
    }
}

LoginForm.propTypes = {
    action: PropTypes.func,
    isLoading: PropTypes.bool,
    handleSubmit: PropTypes.func,
    handleChange: PropTypes.func,
};

export default LoginForm;

export const FormComponent = ({ handleSubmit, handleChange }) => (
    <form className="login-form" onSubmit={handleSubmit}>
        <label className="Login_label">
            <div>email</div>
            <input
                type="email"
                label="email"
                name="email"
                autoComplete="username"
                placeholder="yourname@example.com"
                onChange={handleChange}
                required
            />
        </label>
        <label className="Login_label">
            <div>Password</div>
            <input
                type="password"
                label="password"
                name="password"
                autoComplete="current-password"
                onChange={handleChange}
                required
            />
        </label>
        <div>
            <button>Login</button>
        </div>
    </form>
);

FormComponent.propTypes = {
    handleSubmit: PropTypes.func,
    handleChange: PropTypes.func,
};
