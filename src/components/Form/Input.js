import React from 'react';
import PropTypes from 'prop-types';

const Input = ({
    type,
    label,
    name,
    onChange,
    autocomplete,
    placeholder,
    value
}) => (
    <div className="mt-shared__input">
        <label htmlFor={`input-${name}`}>{label}</label>
        <input
            type={type}
            name={name}
            id={`input-${name}`}
            onChange={onChange}
            autoComplete={autocomplete}
            placeholder={placeholder}
            defaultValue={value}
        />
    </div>
);

Input.propTypes = {
    type: PropTypes.string,
    label: PropTypes.string,
    onChange: PropTypes.func,
    autocomplete: PropTypes.string,
    name: PropTypes.string,
    placeholder: PropTypes.string
};

export default Input;
