import React from "react";
import { shallow, mount } from "enzyme";
import LoginForm from "../LoginForm";
import { FormComponent } from "../LoginForm";

describe("Login form", () => {
    it("Should show login form", () => {
        const wrapper = shallow(<LoginForm isLoading={false} />);
        expect(wrapper).toMatchSnapshot();
        expect(wrapper.find(FormComponent)).toBeDefined();
    });

    it("should show the loading screen", () => {
        const wrapper = shallow(<LoginForm isLoading={true} />);
        expect(wrapper).toMatchSnapshot();
        expect(wrapper.find(FormComponent)).toEqual({});
    });

    it("should show the html form FormComponent ", () => {
        const wrapper = shallow(<FormComponent />);
        expect(wrapper).toMatchSnapshot();
    });

    it("should check that the action method is beign called", () => {
        const handleSubmit = jest.fn();
        const handleChange = jest.fn();
        const wrapper = mount(
            <FormComponent
                handleSubmit={handleSubmit}
                handleChange={handleChange}
            />
        );
        wrapper.find("button").simulate("click");
        expect(handleSubmit).toHaveBeenCalledTimes(0); // no data was entered in fields

        const emailInput = wrapper.find('[type="email"]');
        const passwordInput = wrapper.find("[type='password']");

        emailInput.simulate("change", {
            target: { value: "fakeemail@lexample.com" },
            currentTarget: { name: "email" },
            preventDefault: jest.fn()
        });

        expect(handleChange).toHaveBeenCalledTimes(1);

        passwordInput.simulate("change", {
            target: { value: "thisisafakepassword" },
            currentTarget: { name: "password" },
            preventDefault: jest.fn()
        });

        expect(handleChange).toHaveBeenCalledTimes(2);
        wrapper.find("form").simulate("submit");

        expect(handleSubmit).toHaveBeenCalledTimes(1);
    });
});
