import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import {
    validatePassword,
    logger,
    isEmailValid,
    termsAccepted,
} from "../../utils/helpers";
import Alert from "react-s-alert";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Loading from "../Loading/Loading";
import { signup } from "../../actions/userActions";
import { Redirect } from "react-router-dom";
import "../../stylesheets/LPSignup.scss";
import * as Sentry from "@sentry/browser";

class SignupForm extends React.Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.acceptTerms = this.acceptTerms.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.element = null;

        this.state = {
            email: props.match.params.email,
            tos: Boolean(props.match.params.email),
            password: "",
            repeatPassword: "",
        };
    }

    componentDidCatch(error, errorInfo) {
        Sentry.withScope((scope) => {
            scope.setExtras(errorInfo);
            Sentry.captureException(error);
        });
    }

    componentDidMount() {
        if (
            this.element &&
            `#${this.element.id}` === this.props.location.hash
        ) {
            this.element.scrollIntoView({
                block: "start",
                behaviour: "smooth",
            });
        }
    }

    acceptTerms() {
        this.setState((state) => ({ tos: !state.tos }));
    }

    handleChange(e) {
        e.preventDefault();
        this.setState({ [e.currentTarget.name]: e.currentTarget.value });
    }

    handleSubmit(e) {
        e.preventDefault();
        // handle user signup

        try {
            const email = e.currentTarget.querySelector("input[name='email']")
                .value;
            const tos = e.currentTarget.querySelector("input[name='tos']")
                .checked;
            const password = e.currentTarget.querySelector(
                "input[name='password']"
            ).value;
            const repeatPassword = e.currentTarget.querySelector(
                "input[name='repeatPassword']"
            ).value;
            termsAccepted(tos);
            isEmailValid(email);
            validatePassword({ password, repeatPassword });
            window.gtag("event", "sign_up", {
                value: 1,
            });
            this.props.signup(email, password);
        } catch (e) {
            logger("error", e);
            Alert.error(e.message);
        }
    }
    render() {
        if (this.props.isLoggedIn) return <Redirect to="/library" />;
        if (this.props.isSigningUp) return <Loading />;

        return (
            <div
                className="mt-register-form__container"
                id="signup"
                ref={(el) => (this.element = el)}
            >
                <form onSubmit={this.handleSubmit} className="mt-register-form">
                    <fieldset>
                        <label>
                            email
                            <input
                                type="email"
                                name="email"
                                autoComplete="email"
                                title="enter your email address"
                                required
                                value={this.state.email}
                                onChange={this.handleChange}
                            />
                        </label>
                    </fieldset>

                    <fieldset>
                        <label>
                            password
                            <input
                                type="password"
                                name="password"
                                title="enter your password"
                                value={this.state.password}
                                onChange={this.handleChange}
                                required
                            />
                        </label>
                    </fieldset>
                    <fieldset>
                        <label>
                            repeat password
                            <input
                                type="password"
                                name="repeatPassword"
                                title="type your password again"
                                value={this.state.repeatPassword}
                                onChange={this.handleChange}
                                required
                            />
                        </label>
                    </fieldset>
                    <div
                        className="mt-register-form__tos"
                        onClick={this.acceptTerms}
                    >
                        <label>
                            <div>
                                We promise not to abuse your data. You can
                                easily request it removed from our database.
                            </div>
                        </label>
                        <div>
                            <input
                                title="Check this box to indicate you agree with the terms of service"
                                type="checkbox"
                                name="tos"
                                readOnly
                                checked={this.state.tos}
                                required
                            />

                            <label>
                                I agree with the{" "}
                                <a
                                    href="/terms"
                                    alt="terms of service link"
                                    title="Opens the terms of serivce documentation page"
                                    target="_blank"
                                >
                                    Terms of Service (ToS) and privacy policy
                                </a>
                            </label>
                            <svg
                                width="87"
                                height="51"
                                viewBox="0 0 87 51"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                            >
                                <path
                                    d="M0 7.5L12.9904 0L37.9904 43.3013L25 50.8013L0 7.5Z"
                                    fill="#26A65B"
                                />
                                <path
                                    d="M78.6218 3.00002L86.1218 15.9904L25.5 50.9904L18 38L78.6218 3.00002Z"
                                    fill="#26A65B"
                                />
                            </svg>
                        </div>
                    </div>

                    <div>
                        <div>
                            <button className="mt-register-form__action-prime">
                                create account
                            </button>
                        </div>

                        <div className="mt-register-form__action-extra">
                            Already have an account?{" "}
                            <Link
                                className="mt-register-form__action-extra-button"
                                to="/login"
                            >
                                login
                            </Link>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}

SignupForm.propTypes = {
    action: PropTypes.string,
    isSigningUp: PropTypes.bool,
    signup: PropTypes.func,
    isLoggedIn: PropTypes.bool,
    location: PropTypes.object,
    match: PropTypes.object,
};
const mapStateToProps = (state) => ({
    isSigningUp: state.userReducer.isSigningUp,
    isLoggedIn: state.userReducer.isLoggedIn,
});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            signup,
        },
        dispatch
    );
export default connect(mapStateToProps, mapDispatchToProps)(SignupForm);
