import React from "react";
import PropTypes from "prop-types";

const PrimaryButton = ({ dataType = "primary", ...props }) => (
    <button
        className="mt-shared__button"
        data-type={dataType}
        onClick={props.onClick}
    >
        {props.children}
    </button>
);

PrimaryButton.propTypes = {
    dataType: PropTypes.string,
    children: PropTypes.string,
    onClick: PropTypes.func
};

export default PrimaryButton;
