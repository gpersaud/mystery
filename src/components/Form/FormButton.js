import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

const FormButton = ({ label, target = "" }) => (
    <Link to={target} className="mt-shared__button-secondary">
        {label}
    </Link>
);

FormButton.propTypes = {
    label: PropTypes.string,
    target: PropTypes.string
};

export default FormButton;
