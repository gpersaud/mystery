import React from "react";
import PropTypes from "prop-types";
import PrimaryButton from "../Form/PrimaryButton";
import FormButton from "../Form/FormButton";

const FormAction = ({ formAction = "", altAction = "", altActionLabel }) => (
    <div className="mt-register-form__action-container">
        <div className="mt-register-form__action-prime">
            <PrimaryButton
                dataType={formAction === "sign up" ? "hot" : "primary"}
            >
                {formAction}
            </PrimaryButton>
        </div>
        <div className="mt-register-form__action-extra">
            <FormButton label={altActionLabel} target={altAction} />
            {formAction.toLowerCase() === "login" ? (
                <FormButton label="Password?" target="/password" />
            ) : null}
        </div>
    </div>
);

FormAction.propTypes = {
    formAction: PropTypes.string,
    altAction: PropTypes.string,
    altActionLabel: PropTypes.string
};

export default FormAction;
