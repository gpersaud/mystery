import React from "react";
import PropTypes from "prop-types";

/**
 * Receives an Array with two languages and just checks
 * If during the iteration, this is the first item
 * show the hyphen
 * @param {Integer} idx The index of the given language
 * @param {Array} arr The array with a list of languages
 */
const Hyphen = ({ idx = 0, arr = [] }) =>
    idx < arr.length - 1 ? <span>{" & "}</span> : null;

Hyphen.propTypes = {
    idx: PropTypes.number,
    arr: PropTypes.array
};

export default Hyphen;
