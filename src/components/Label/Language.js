import React from "react";
import PropTypes from "prop-types";
import LanguageName from "./LanguageName";
import Hyphen from "./Hyphen";
/**
 * Takes a list of languages and shows them as
 * a label with a hyphen in between
 * @param {Array} languages The list of languages to be shown
 */
const LanguageLabel = ({ languages = [] }) => (
    <div className="mt-label__container">
        <div className="mt-label__language">
            {languages &&
                languages.map((langCode, idx, arr) => {
                    if (!langCode) return null;
                    return (
                        <span key={`language-span-${langCode}-${idx}`}>
                            <LanguageName code={langCode} />
                            <Hyphen idx={idx} arr={arr} />
                        </span>
                    );
                })}
        </div>
    </div>
);

LanguageLabel.propTypes = {
    languages: PropTypes.arrayOf(PropTypes.string)
};

export default LanguageLabel;
