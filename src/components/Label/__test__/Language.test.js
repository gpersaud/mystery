import React from "react";
import { shallow } from "enzyme";
import Language from "../Language";
import LanguageName from "../LanguageName";
import Hyphen from "../Hyphen";

describe("language label", () => {
    let props;
    let mountedComponent;
    let mountComponent = props =>
        mountedComponent ? mountedComponent : shallow(<Language {...props} />);
    beforeEach(() => {
        props = {
            languages: ["en", "nl"]
        };
    });
    it("should render a div", () => {
        const languageLabel = mountComponent(props);
        expect(languageLabel.length).toBeGreaterThan(0);
        expect(languageLabel).toMatchSnapshot();
    });
    it("should have a props", () => {
        const languageLabel = mountComponent(props);
        // [ 'className', 'children' ]
        expect(Object.keys(languageLabel.props()).length).toBe(2);
    });
    it("should have a language name component", () => {
        const languageName = mountComponent(props).find(LanguageName);
        expect(languageName.length).toBeGreaterThan(0);
        expect(languageName.length).toBe(2);
    });
    // There are 2 hyphe components
    it("should have a hyphen component", () => {
        const hyphen = mountComponent(props).find(Hyphen);
        expect(hyphen.length).toBeGreaterThan(1);
    });
});

/**
 * Tests if only one language is passed
 */

describe("check with only one language", () => {
    let props;
    let mountedComponent;
    let mountComponent = props =>
        mountedComponent ? mountedComponent : shallow(<Language {...props} />);
    beforeEach(() => {
        props = {
            languages: ["en"]
        };
    });

    it("should match snapshot", () => {
        const languageLabel = mountComponent(props);
        expect(languageLabel).toMatchSnapshot();
    });

    it("should have only one langaugeName component", () => {
        const languageName = mountComponent(props).find(LanguageName);
        expect(languageName.length).toBeGreaterThan(0);
        expect(languageName.length).toBe(1);
    });
    it("should not show a hyphen", () => {
        const hyphen = mountComponent(props).find(Hyphen);
        expect(hyphen.length).toBe(1);
        expect(hyphen.props()).toEqual({ idx: 0, arr: ["en"] });
        expect(hyphen.text()).toEqual("<Hyphen />");
        expect(hyphen.html()).toEqual("");
    });
});
