export const languageList = [
    {
        _id: "27aG4aPd8PBkBXfDq",
        languageCode: "vi",
        languageName: "Vietnamese",
        languageNativeName: "Tiếng Việt"
    },
    {
        _id: "28QJjkBMQLqY3QepD",
        languageCode: "hu",
        languageName: "Hungarian",
        languageNativeName: "Magyar"
    },
    {
        _id: "2E84KYPbBanC3qcvp",
        languageCode: "ks",
        languageName: "Kashmiri",
        languageNativeName: "कश्मीरी, كشميري‎"
    },
    {
        _id: "2NJBBom7sFH5FQxXf",
        languageCode: "tg",
        languageName: "Tajik",
        languageNativeName: "тоҷикӣ, toğikī, تاجیکی‎"
    },
    {
        _id: "2Xbh3JhuzhFDbbaGq",
        languageCode: "it",
        languageName: "Italian",
        languageNativeName: "Italiano"
    },
    {
        _id: "2jeBC7zc8MMWyboLi",
        languageCode: "uk",
        languageName: "Ukrainian",
        languageNativeName: "українська"
    },
    {
        _id: "2qJAjhxRKmbw66gYD",
        languageCode: "oc",
        languageName: "Occitan",
        languageNativeName: "Occitan"
    },
    {
        _id: "33qKx6CQA8RZRgsJC",
        languageCode: "es",
        languageName: "Spanish",
        languageNativeName: "español, castellano"
    },
    {
        _id: "37tr8RKvfpnetrY9i",
        languageCode: "bh",
        languageName: "Bihari",
        languageNativeName: "भोजपुरी"
    },
    {
        _id: "3YjeCrwqpiHC53SCk",
        languageCode: "sv",
        languageName: "Swedish",
        languageNativeName: "svenska"
    },
    {
        _id: "3o6S6Z7447wpyCcjZ",
        languageCode: "gl",
        languageName: "Galician",
        languageNativeName: "Galego"
    },
    {
        _id: "44KDwTtXGGjhCQApy",
        languageCode: "sw",
        languageName: "Swahili",
        languageNativeName: "Kiswahili"
    },
    {
        _id: "4L9w8RtaxXta7YSJm",
        languageCode: "az",
        languageName: "Azerbaijani",
        languageNativeName: "azərbaycan dili"
    },
    {
        _id: "4kRBQfmofn64aL6ne",
        languageCode: "ay",
        languageName: "Aymara",
        languageNativeName: "aymar aru"
    },
    {
        _id: "4pufiqovLgGSt7E3w",
        languageCode: "lv",
        languageName: "Latvian",
        languageNativeName: "latviešu valoda"
    },
    {
        _id: "4w364FEGcMGFvGmFz",
        languageCode: "tl",
        languageName: "Tagalog",
        languageNativeName: "Wikang Tagalog, ᜏᜒᜃᜅ᜔ ᜆᜄᜎᜓᜄ᜔"
    },
    {
        _id: "54vuBYAo9uezHpfT7",
        languageCode: "ti",
        languageName: "Tigrinya",
        languageNativeName: "ትግርኛ"
    },
    {
        _id: "5dQv9muomLxkYgik3",
        languageCode: "nr",
        languageName: "South Ndebele",
        languageNativeName: "isiNdebele"
    },
    {
        _id: "5zaQHtc7axuqHT2Zi",
        languageCode: "ch",
        languageName: "Chamorro",
        languageNativeName: "Chamoru"
    },
    {
        _id: "65fQR9FZKuffMMogg",
        languageCode: "sr",
        languageName: "Serbian",
        languageNativeName: "српски језик"
    },
    {
        _id: "69hFkusuLJGprL37F",
        languageCode: "nb",
        languageName: "Norwegian Bokmål",
        languageNativeName: "Norsk bokmål"
    },
    {
        _id: "7859kFpCs8EN5F7Kt",
        languageCode: "ta",
        languageName: "Tamil",
        languageNativeName: "தமிழ்"
    },
    {
        _id: "7KnJciyed4fYWe4Wi",
        languageCode: "or",
        languageName: "Oriya",
        languageNativeName: "ଓଡ଼ିଆ"
    },
    {
        _id: "7cLQkCH4q6kMoWPDK",
        languageCode: "ak",
        languageName: "Akan",
        languageNativeName: "Akan"
    },
    {
        _id: "7h5Sd3eA56eBPxbuY",
        languageCode: "lb",
        languageName: "Luxembourgish, Letzeburgesch",
        languageNativeName: "Lëtzebuergesch"
    },
    {
        _id: "7xE9ZdqZkmAr2Wgj7",
        languageCode: "lu",
        languageName: "Luba-Katanga",
        languageNativeName: ""
    },
    {
        _id: "887NpvgFETzFdZWgf",
        languageCode: "sg",
        languageName: "Sango",
        languageNativeName: "yângâ tî sängö"
    },
    {
        _id: "8PaaaHtCEGxCj7Ndc",
        languageCode: "hr",
        languageName: "Croatian",
        languageNativeName: "hrvatski"
    },
    {
        _id: "8ScvFRjiFRDe4SD4B",
        languageCode: "cs",
        languageName: "Czech",
        languageNativeName: "česky, čeština"
    },
    {
        _id: "8XWErDmsxepAhfNHe",
        languageCode: "ig",
        languageName: "Igbo",
        languageNativeName: "Asụsụ Igbo"
    },
    {
        _id: "8o9FgjYrnPbhaZFDM",
        languageCode: "su",
        languageName: "Sundanese",
        languageNativeName: "Basa Sunda"
    },
    {
        _id: "8rmsvq3HfubQyJtpR",
        languageCode: "tt",
        languageName: "Tatar",
        languageNativeName: "татарча, tatarça, تاتارچا‎"
    },
    {
        _id: "95Tg7xAedCfdh6ssw",
        languageCode: "da",
        languageName: "Danish",
        languageNativeName: "dansk"
    },
    {
        _id: "9SgXF4xysXLhjumL5",
        languageCode: "ky",
        languageName: "Kirghiz, Kyrgyz",
        languageNativeName: "кыргыз тили"
    },
    {
        _id: "ApFvXgQxfZD6rJxBH",
        languageCode: "bm",
        languageName: "Bambara",
        languageNativeName: "bamanankan"
    },
    {
        _id: "At5BzMW3KkKF6NpRf",
        languageCode: "ho",
        languageName: "Hiri Motu",
        languageNativeName: "Hiri Motu"
    },
    {
        _id: "AtddJpfAkaSTFFvrb",
        languageCode: "gn",
        languageName: "Guaraní",
        languageNativeName: "Avañeẽ"
    },
    {
        _id: "BrA8rWsSeyLjieeuu",
        languageCode: "sk",
        languageName: "Slovak",
        languageNativeName: "slovenčina"
    },
    {
        _id: "CBMN2SwLpDGf6kKfC",
        languageCode: "si",
        languageName: "Sinhala, Sinhalese",
        languageNativeName: "සිංහල"
    },
    {
        _id: "D2Nk9SM6EnSdhsi7u",
        languageCode: "id",
        languageName: "Indonesian",
        languageNativeName: "Bahasa Indonesia"
    },
    {
        _id: "DA77vkPP7GGien4rc",
        languageCode: "ms",
        languageName: "Malay",
        languageNativeName: "bahasa Melayu, بهاس ملايو‎"
    },
    {
        _id: "DXCqMNm66wnyDF6o6",
        languageCode: "et",
        languageName: "Estonian",
        languageNativeName: "eesti, eesti keel"
    },
    {
        _id: "DXn8mcLevzLfdjCuS",
        languageCode: "ga",
        languageName: "Irish",
        languageNativeName: "Gaeilge"
    },
    {
        _id: "E5mcdHBhJSF5inBDk",
        languageCode: "ff",
        languageName: "Fula; Fulah; Pulaar; Pular",
        languageNativeName: "Fulfulde, Pulaar, Pular"
    },
    {
        _id: "E62y2N59E6885cwth",
        languageCode: "jv",
        languageName: "Javanese",
        languageNativeName: "basa Jawa"
    },
    {
        _id: "ECEFkxHwvLsNXe9Wp",
        languageCode: "yo",
        languageName: "Yoruba",
        languageNativeName: "Yorùbá"
    },
    {
        _id: "EHgZManCEXkyeXca3",
        languageCode: "ne",
        languageName: "Nepali",
        languageNativeName: "नेपाली"
    },
    {
        _id: "EecndzDqqrcARuDHd",
        languageCode: "ss",
        languageName: "Swati",
        languageNativeName: "SiSwati"
    },
    {
        _id: "F78Fm2c8h2R75CTNj",
        languageCode: "tk",
        languageName: "Turkmen",
        languageNativeName: "Türkmen, Түркмен"
    },
    {
        _id: "FEP8YMNq4QCixFWMv",
        languageCode: "is",
        languageName: "Icelandic",
        languageNativeName: "Íslenska"
    },
    {
        _id: "FS69bZY7MKvDFydfN",
        languageCode: "en",
        languageName: "English",
        languageNativeName: "English"
    },
    {
        _id: "FYCM7P3mZFhyqbtqm",
        languageCode: "ja",
        languageName: "Japanese",
        languageNativeName: "日本語 (にほんご／にっぽんご)"
    },
    {
        _id: "FYXKP4KaqvvywCvBR",
        languageCode: "iu",
        languageName: "Inuktitut",
        languageNativeName: "ᐃᓄᒃᑎᑐᑦ"
    },
    {
        _id: "FfA5nj3tSbtiydHW7",
        languageCode: "ba",
        languageName: "Bashkir",
        languageNativeName: "башҡорт теле"
    },
    {
        _id: "GCagyPMb2QGs3qmnD",
        languageCode: "th",
        languageName: "Thai",
        languageNativeName: "ไทย"
    },
    {
        _id: "HHbZXEwQjpsykbP54",
        languageCode: "se",
        languageName: "Northern Sami",
        languageNativeName: "Davvisámegiella"
    },
    {
        _id: "HoRDEi9cQRTCQNve6",
        languageCode: "sl",
        languageName: "Slovene",
        languageNativeName: "slovenščina"
    },
    {
        _id: "Hqh8wLcFYKdmGbiq5",
        languageCode: "nd",
        languageName: "North Ndebele",
        languageNativeName: "isiNdebele"
    },
    {
        _id: "JMjA6AnHowrr2dqwx",
        languageCode: "co",
        languageName: "Corsican",
        languageNativeName: "corsu, lingua corsa"
    },
    {
        _id: "JnWLPoBESLDaDfRGt",
        languageCode: "fr",
        languageName: "French",
        languageNativeName: "français, langue française"
    },
    {
        _id: "JshbWg5jYeKJGJ2yP",
        languageCode: "hz",
        languageName: "Herero",
        languageNativeName: "Otjiherero"
    },
    {
        _id: "KmzxcRNAxzJEAQTLF",
        languageCode: "pl",
        languageName: "Polish",
        languageNativeName: "polski"
    },
    {
        _id: "KvCPBJm9y4o99j2oP",
        languageCode: "am",
        languageName: "Amharic",
        languageNativeName: "አማርኛ"
    },
    {
        _id: "Kyfv6nANkHqyeSPd8",
        languageCode: "dv",
        languageName: "Divehi; Dhivehi; Maldivian;",
        languageNativeName: "ދިވެހި"
    },
    {
        _id: "LCfwYab3t5TNSYnYP",
        languageCode: "sa",
        languageName: "Sanskrit (Saṁskṛta)",
        languageNativeName: "संस्कृतम्"
    },
    {
        _id: "LQWxhLxWfiy3ehf3i",
        languageCode: "ha",
        languageName: "Hausa",
        languageNativeName: "Hausa, هَوُسَ"
    },
    {
        _id: "LZzYcFq8T7jWrWiMz",
        languageCode: "rm",
        languageName: "Romansh",
        languageNativeName: "rumantsch grischun"
    },
    {
        _id: "Lq2XanNW4guWbWEsn",
        languageCode: "hy",
        languageName: "Armenian",
        languageNativeName: "Հայերեն"
    },
    {
        _id: "Lqp9u5NoS7FhyWxdo",
        languageCode: "oj",
        languageName: "Ojibwe, Ojibwa",
        languageNativeName: "ᐊᓂᔑᓈᐯᒧᐎᓐ"
    },
    {
        _id: "LxdBhhfreqK2jt2uN",
        languageCode: "cy",
        languageName: "Welsh",
        languageNativeName: "Cymraeg"
    },
    {
        _id: "LzKK8hCcewhCFAXT4",
        languageCode: "ng",
        languageName: "Ndonga",
        languageNativeName: "Owambo"
    },
    {
        _id: "M73m3YWgXWfQzzfoT",
        languageCode: "wa",
        languageName: "Walloon",
        languageNativeName: "Walon"
    },
    {
        _id: "M8ya5bphRBPb6p7yk",
        languageCode: "he",
        languageName: "Hebrew (modern)",
        languageNativeName: "עברית"
    },
    {
        _id: "MaLYkocSR6niZ6Ldc",
        languageCode: "cu",
        languageName:
            "Old Church Slavonic, Church Slavic, Church Slavonic, Old Bulgarian, Old Slavonic",
        languageNativeName: "ѩзыкъ словѣньскъ"
    },
    {
        _id: "MpNMQJ8Yy8NoPDYpQ",
        languageCode: "sc",
        languageName: "Sardinian",
        languageNativeName: "sardu"
    },
    {
        _id: "N3Lf8TAGWXASFaKbE",
        languageCode: "rw",
        languageName: "Kinyarwanda",
        languageNativeName: "Ikinyarwanda"
    },
    {
        _id: "N3vuvSjyNCcBTfXL5",
        languageCode: "eu",
        languageName: "Basque",
        languageNativeName: "euskara, euskera"
    },
    {
        _id: "NGqWCfQXv4oGG3aCD",
        languageCode: "cr",
        languageName: "Cree",
        languageNativeName: "ᓀᐦᐃᔭᐍᐏᐣ"
    },
    {
        _id: "Nk7LheGg8oWnty7CP",
        languageCode: "rn",
        languageName: "Kirundi",
        languageNativeName: "kiRundi"
    },
    {
        _id: "PNkmQyoXSM6TaCdua",
        languageCode: "mk",
        languageName: "Macedonian",
        languageNativeName: "македонски јазик"
    },
    {
        _id: "PZdH6CLh5uyrZxC5e",
        languageCode: "pi",
        languageName: "Pāli",
        languageNativeName: "पाऴि"
    },
    {
        _id: "Q8PmYXcdYZp8QA28g",
        languageCode: "kg",
        languageName: "Kongo",
        languageNativeName: "KiKongo"
    },
    {
        _id: "RF2b7P6XC8DrgBikS",
        languageCode: "fo",
        languageName: "Faroese",
        languageNativeName: "føroyskt"
    },
    {
        _id: "RL3syK3z4Zam5PBtz",
        languageCode: "pa",
        languageName: "Panjabi, Punjabi",
        languageNativeName: "ਪੰਜਾਬੀ, پنجابی‎"
    },
    {
        _id: "RZzu8C3Spy5CBkenz",
        languageCode: "lo",
        languageName: "Lao",
        languageNativeName: "ພາສາລາວ"
    },
    {
        _id: "RxG2YXzdpogeYQjZY",
        languageCode: "sn",
        languageName: "Shona",
        languageNativeName: "chiShona"
    },
    {
        _id: "RzZ4Dg5924pKndtwM",
        languageCode: "kk",
        languageName: "Kazakh",
        languageNativeName: "Қазақ тілі"
    },
    {
        _id: "SHjYxP27bzXqyTBMv",
        languageCode: "na",
        languageName: "Nauru",
        languageNativeName: "Ekakairũ Naoero"
    },
    {
        _id: "SJ5TtqDh46KeFovQn",
        languageCode: "gv",
        languageName: "Manx",
        languageNativeName: "Gaelg, Gailck"
    },
    {
        _id: "SmSB9GStBWTCQWhnq",
        languageCode: "ar",
        languageName: "Arabic",
        languageNativeName: "العربية"
    },
    {
        _id: "TNnd2CFRRmir6MTRh",
        languageCode: "ca",
        languageName: "Catalan; Valencian",
        languageNativeName: "Català"
    },
    {
        _id: "ToF9BefjseDHFd3qQ",
        languageCode: "ln",
        languageName: "Lingala",
        languageNativeName: "Lingála"
    },
    {
        _id: "W7rM5HFAbixpRCbFf",
        languageCode: "ve",
        languageName: "Venda",
        languageNativeName: "Tshivenḓa"
    },
    {
        _id: "W9mqL5FnQDC9GaxHp",
        languageCode: "ny",
        languageName: "Chichewa; Chewa; Nyanja",
        languageNativeName: "chiCheŵa, chinyanja"
    },
    {
        _id: "WacNFoSDPkLgykoCm",
        languageCode: "wo",
        languageName: "Wolof",
        languageNativeName: "Wollof"
    },
    {
        _id: "WdL9BoCDmignPWjBn",
        languageCode: "nl",
        languageName: "Dutch",
        languageNativeName: "Nederlands, Vlaams"
    },
    {
        _id: "X8EQHjQhLXqQTbnmJ",
        languageCode: "mn",
        languageName: "Mongolian",
        languageNativeName: "монгол"
    },
    {
        _id: "Xi62Ss4sSLb6wRDwe",
        languageCode: "fj",
        languageName: "Fijian",
        languageNativeName: "vosa Vakaviti"
    },
    {
        _id: "YAMRYEwqvNbtkyPTt",
        languageCode: "kn",
        languageName: "Kannada",
        languageNativeName: "ಕನ್ನಡ"
    },
    {
        _id: "YTaY4E6P9BcHX9q8x",
        languageCode: "be",
        languageName: "Belarusian",
        languageNativeName: "Беларуская"
    },
    {
        _id: "Ywd8GmxgFwxiXx5NM",
        languageCode: "kr",
        languageName: "Kanuri",
        languageNativeName: "Kanuri"
    },
    {
        _id: "ZC4JfD596wR59YYdr",
        languageCode: "bs",
        languageName: "Bosnian",
        languageNativeName: "bosanski jezik"
    },
    {
        _id: "ZHGPFk4H7tLs3Sdkg",
        languageCode: "mh",
        languageName: "Marshallese",
        languageNativeName: "Kajin M̧ajeļ"
    },
    {
        _id: "Zbx5cmsGj4g6QAB6g",
        languageCode: "om",
        languageName: "Oromo",
        languageNativeName: "Afaan Oromoo"
    },
    {
        _id: "ao4Y6646XAj9noigm",
        languageCode: "bn",
        languageName: "Bengali",
        languageNativeName: "বাংলা"
    },
    {
        _id: "b73zzSSj9o2HMGToh",
        languageCode: "no",
        languageName: "Norwegian",
        languageNativeName: "Norsk"
    },
    {
        _id: "bWDjXB7Z6mkPLAm3m",
        languageCode: "qu",
        languageName: "Quechua",
        languageNativeName: "Runa Simi, Kichwa"
    },
    {
        _id: "c7zYCpvzQbxDxfSFg",
        languageCode: "mi",
        languageName: "Māori",
        languageNativeName: "te reo Māori"
    },
    {
        _id: "c85sEBSNZfPqpi9G7",
        languageCode: "ki",
        languageName: "Kikuyu, Gikuyu",
        languageNativeName: "Gĩkũyũ"
    },
    {
        _id: "c9rvojZqtjfZJbsSi",
        languageCode: "fa",
        languageName: "Persian",
        languageNativeName: "فارسی"
    },
    {
        _id: "cB6i7M7ip6qn7aMry",
        languageCode: "kv",
        languageName: "Komi",
        languageNativeName: "коми кыв"
    },
    {
        _id: "cJCbeirTcgSWDBbGJ",
        languageCode: "os",
        languageName: "Ossetian, Ossetic",
        languageNativeName: "ирон æвзаг"
    },
    {
        _id: "cTBAK3yeSSa3K7tBa",
        languageCode: "my",
        languageName: "Burmese",
        languageNativeName: "ဗမာစာ"
    },
    {
        _id: "cuAwC2Yqywft7GTXw",
        languageCode: "ps",
        languageName: "Pashto, Pushto",
        languageNativeName: "پښتو"
    },
    {
        _id: "d3nDCWtGmLdnLmnvQ",
        languageCode: "ru",
        languageName: "Russian",
        languageNativeName: "русский язык"
    },
    {
        _id: "dFCkTzpN5tQsv3Hfw",
        languageCode: "tr",
        languageName: "Turkish",
        languageNativeName: "Türkçe"
    },
    {
        _id: "dPgRwgiSu3Z5HqjJR",
        languageCode: "ae",
        languageName: "Avestan",
        languageNativeName: "avesta"
    },
    {
        _id: "eefuoT7Xh7JYo3wFd",
        languageCode: "ml",
        languageName: "Malayalam",
        languageNativeName: "മലയാളം"
    },
    {
        _id: "efmKGvxY3fvoAprAo",
        languageCode: "io",
        languageName: "Ido",
        languageNativeName: "Ido"
    },
    {
        _id: "empTbkJGvpWGgYYv8",
        languageCode: "ug",
        languageName: "Uighur, Uyghur",
        languageNativeName: "Uyƣurqə, ئۇيغۇرچە‎"
    },
    {
        _id: "fJduehrxmcESRaDJb",
        languageCode: "ka",
        languageName: "Georgian",
        languageNativeName: "ქართული"
    },
    {
        _id: "gwSZhoxjzJ3HXg7Pj",
        languageCode: "nn",
        languageName: "Norwegian Nynorsk",
        languageNativeName: "Norsk nynorsk"
    },
    {
        _id: "gzRA9JSyf4dGmPxHJ",
        languageCode: "ku",
        languageName: "Kurdish",
        languageNativeName: "Kurdî, كوردی‎"
    },
    {
        _id: "hLZBuPJogaXSSLGtF",
        languageCode: "ii",
        languageName: "Nuosu",
        languageNativeName: "ꆈꌠ꒿ Nuosuhxop"
    },
    {
        _id: "hMu6XAc4KgvA3gyYh",
        languageCode: "hi",
        languageName: "Hindi",
        languageNativeName: "हिन्दी, हिंदी"
    },
    {
        _id: "hnEXjZnCpF43xhSDE",
        languageCode: "km",
        languageName: "Khmer",
        languageNativeName: "ភាសាខ្មែរ"
    },
    {
        _id: "hvyBasQ9YuNtE7mCM",
        languageCode: "ur",
        languageName: "Urdu",
        languageNativeName: "اردو"
    },
    {
        _id: "i2Nrx3ekg4wjss6EQ",
        languageCode: "aa",
        languageName: "Afar",
        languageNativeName: "Afaraf"
    },
    {
        _id: "jD4rCykr7oDvb43y2",
        languageCode: "kl",
        languageName: "Kalaallisut, Greenlandic",
        languageNativeName: "kalaallisut, kalaallit oqaasii"
    },
    {
        _id: "jhXewSDCx8ya3JbX5",
        languageCode: "ik",
        languageName: "Inupiaq",
        languageNativeName: "Iñupiaq, Iñupiatun"
    },
    {
        _id: "joQEYcgjNDjMkrR7e",
        languageCode: "te",
        languageName: "Telugu",
        languageNativeName: "తెలుగు"
    },
    {
        _id: "kSo2k3i7fs4Sfakbx",
        languageCode: "mt",
        languageName: "Maltese",
        languageNativeName: "Malti"
    },
    {
        _id: "kyaecswZGmY9NvQ9c",
        languageCode: "av",
        languageName: "Avaric",
        languageNativeName: "авар мацӀ, магӀарул мацӀ"
    },
    {
        _id: "mKdcEjrWWQ3NWsdaN",
        languageCode: "kw",
        languageName: "Cornish",
        languageNativeName: "Kernewek"
    },
    {
        _id: "mZjntuqe75smzrRg9",
        languageCode: "ro",
        languageName: "Romanian, Moldavian, Moldovan",
        languageNativeName: "română"
    },
    {
        _id: "mZxgLrKraxBiLbAAi",
        languageCode: "cv",
        languageName: "Chuvash",
        languageNativeName: "чӑваш чӗлхи"
    },
    {
        _id: "nJxroaPPxbb7qJNZu",
        languageCode: "fi",
        languageName: "Finnish",
        languageNativeName: "suomi, suomen kieli"
    },
    {
        _id: "nZbePzhvS8R7DhYiz",
        languageCode: "eo",
        languageName: "Esperanto",
        languageNativeName: "Esperanto"
    },
    {
        _id: "ndcfv8Y34WhRLC7dc",
        languageCode: "ty",
        languageName: "Tahitian",
        languageNativeName: "Reo Tahiti"
    },
    {
        _id: "nhaSv8DjHaDQbqXWA",
        languageCode: "sm",
        languageName: "Samoan",
        languageNativeName: "gagana faa Samoa"
    },
    {
        _id: "oXioTkePQSdi7JeMw",
        languageCode: "ht",
        languageName: "Haitian; Haitian Creole",
        languageNativeName: "Kreyòl ayisyen"
    },
    {
        _id: "omvCzCoYBLwBXir25",
        languageCode: "gd",
        languageName: "Scottish Gaelic; Gaelic",
        languageNativeName: "Gàidhlig"
    },
    {
        _id: "p76DLXkgNJ8MMbdm5",
        languageCode: "ia",
        languageName: "Interlingua",
        languageNativeName: "Interlingua"
    },
    {
        _id: "pZwFWcymo7rWc6Kht",
        languageCode: "lg",
        languageName: "Luganda",
        languageNativeName: "Luganda"
    },
    {
        _id: "pvbsCqTweSXqKRhqS",
        languageCode: "tw",
        languageName: "Twi",
        languageNativeName: "Twi"
    },
    {
        _id: "q4zbr2ixjzbwCepHF",
        languageCode: "ee",
        languageName: "Ewe",
        languageNativeName: "Eʋegbe"
    },
    {
        _id: "qjwEDFBGHJSZN5ESw",
        languageCode: "sq",
        languageName: "Albanian",
        languageNativeName: "Shqip"
    },
    {
        _id: "qmytcep4tfZxAGKvE",
        languageCode: "ab",
        languageName: "Abkhaz",
        languageNativeName: "аҧсуа"
    },
    {
        _id: "qsH4BEBPK3DLeappw",
        languageCode: "kj",
        languageName: "Kwanyama, Kuanyama",
        languageNativeName: "Kuanyama"
    },
    {
        _id: "rhQTqQvzf8bDZsNRr",
        languageCode: "an",
        languageName: "Aragonese",
        languageNativeName: "Aragonés"
    },
    {
        _id: "sDe7YyqgRP78vPzfH",
        languageCode: "uz",
        languageName: "Uzbek",
        languageNativeName: "zbek, Ўзбек, أۇزبېك‎"
    },
    {
        _id: "scZ8mL4NjZuh6rp7g",
        languageCode: "ts",
        languageName: "Tsonga",
        languageNativeName: "Xitsonga"
    },
    {
        _id: "sceReK5D3fvt5RmbF",
        languageCode: "gu",
        languageName: "Gujarati",
        languageNativeName: "ગુજરાતી"
    },
    {
        _id: "sjv4pPajvBYkWw9Mq",
        languageCode: "za",
        languageName: "Zhuang, Chuang",
        languageNativeName: "Saɯ cueŋƅ, Saw cuengh"
    },
    {
        _id: "tTHMon3hHMoF5F7Qg",
        languageCode: "ce",
        languageName: "Chechen",
        languageNativeName: "нохчийн мотт"
    },
    {
        _id: "tc9dRuMhxeWbbFrzv",
        languageCode: "la",
        languageName: "Latin",
        languageNativeName: "latine, lingua latina"
    },
    {
        _id: "tdHo2Je3X6Jd5xir5",
        languageCode: "mg",
        languageName: "Malagasy",
        languageNativeName: "Malagasy fiteny"
    },
    {
        _id: "tmdvjLo7TBoY5qQMf",
        languageCode: "bi",
        languageName: "Bislama",
        languageNativeName: "Bislama"
    },
    {
        _id: "tvzYecah68fKmT2z9",
        languageCode: "lt",
        languageName: "Lithuanian",
        languageNativeName: "lietuvių kalba"
    },
    {
        _id: "tz9kiKQCtX5p8tnNC",
        languageCode: "de",
        languageName: "German",
        languageNativeName: "Deutsch"
    },
    {
        _id: "uBFRaL2YF3JkvCypn",
        languageCode: "sd",
        languageName: "Sindhi",
        languageNativeName: "सिन्धी, سنڌي، سندھی‎"
    },
    {
        _id: "uLoHMFThwYE5Yt9TN",
        languageCode: "xh",
        languageName: "Xhosa",
        languageNativeName: "isiXhosa"
    },
    {
        _id: "ujoM9jYeAdsixnZZA",
        languageCode: "el",
        languageName: "Greek, Modern",
        languageNativeName: "Ελληνικά"
    },
    {
        _id: "vG5gmJTThSu6s3SfZ",
        languageCode: "af",
        languageName: "Afrikaans",
        languageNativeName: "Afrikaans"
    },
    {
        _id: "vGrFjc5Dr8CjfcMFC",
        languageCode: "fy",
        languageName: "Western Frisian",
        languageNativeName: "Frysk"
    },
    {
        _id: "vPM64qoPfAFczsJRa",
        languageCode: "zh",
        languageName: "Chinese",
        languageNativeName: "中文 (Zhōngwén), 汉语, 漢語"
    },
    {
        _id: "vovHFt7wudmuNoKoj",
        languageCode: "to",
        languageName: "Tonga (Tonga Islands)",
        languageNativeName: "faka Tonga"
    },
    {
        _id: "vretv5BMssJYfg2Xw",
        languageCode: "bg",
        languageName: "Bulgarian",
        languageNativeName: "български език"
    },
    {
        _id: "wyfap6sQW2n2qtJzS",
        languageCode: "vo",
        languageName: "Volapük",
        languageNativeName: "Volapük"
    },
    {
        _id: "x3iMpF2rdQufap5zM",
        languageCode: "so",
        languageName: "Somali",
        languageNativeName: "Soomaaliga, af Soomaali"
    },
    {
        _id: "xAP38hMXc7byNYtEa",
        languageCode: "as",
        languageName: "Assamese",
        languageNativeName: "অসমীয়া"
    },
    {
        _id: "xDzCkoZAubhXSrPqp",
        languageCode: "ko",
        languageName: "Korean",
        languageNativeName: "한국어 (韓國語), 조선말 (朝鮮語)"
    },
    {
        _id: "xKapi88vDGkd5pL6Z",
        languageCode: "pt",
        languageName: "Portuguese",
        languageNativeName: "Português"
    },
    {
        _id: "xWkibTcPWff893JNp",
        languageCode: "br",
        languageName: "Breton",
        languageNativeName: "brezhoneg"
    },
    {
        _id: "xZ8PJKupXLaTY2HfT",
        languageCode: "bo",
        languageName: "Tibetan Standard, Tibetan, Central",
        languageNativeName: "བོད་ཡིག"
    },
    {
        _id: "xuQNndRjh3ygZgtmE",
        languageCode: "mr",
        languageName: "Marathi (Marāṭhī)",
        languageNativeName: "मराठी"
    },
    {
        _id: "xydTMyPhS7gzyRXGF",
        languageCode: "li",
        languageName: "Limburgish, Limburgan, Limburger",
        languageNativeName: "Limburgs"
    },
    {
        _id: "y4CvSXKdvoScSFSAB",
        languageCode: "ie",
        languageName: "Interlingue",
        languageNativeName:
            "Originally called Occidental; then Interlingue after WWII"
    },
    {
        _id: "yLXpNywjoiAuFqbDi",
        languageCode: "tn",
        languageName: "Tswana",
        languageNativeName: "Setswana"
    },
    {
        _id: "ycuSMjYFe7RhqQE8B",
        languageCode: "yi",
        languageName: "Yiddish",
        languageNativeName: "ייִדיש"
    },
    {
        _id: "z7QcTNf88ovQtvfd2",
        languageCode: "nv",
        languageName: "Navajo, Navaho",
        languageNativeName: "Diné bizaad, Dinékʼehǰí"
    },
    {
        _id: "zc6gTLM7tdBL9DHCX",
        languageCode: "st",
        languageName: "Southern Sotho",
        languageNativeName: "Sesotho"
    }
];
