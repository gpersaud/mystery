import PropTypes from "prop-types";
import { languageList } from "./languageList";

/**
 * Takes the code of a language and returns it's full name
 * Since there's no JSX in this file there's no need to import React...
 * @param {String} code The code of the language
 */
const LanguageName = ({ code = "" }) => {
    if (!(languageList || code)) return null;
    const lang = languageList.find(
        lg => lg.languageCode === code.toLowerCase()
    );
    return lang ? lang.languageName : null;
};

LanguageName.propTypes = {
    code: PropTypes.string
};

/**
 * takes a code and then returns the full language name for that given code.
 * @param {String} code The language code for which the full language name should be returned
 */
export const getLanguageName = code => {
    if (!code) return null;
    const lang = languageList.find(
        lg => lg.languageCode === code.toLowerCase()
    );
    return lang ? lang.languageName : code;
};

export default LanguageName;
