import React from "react";

const Terms = () => (
    <div>
        <h1>Terms and Conditions</h1>
        <p>
            Mytoori is a service offering bilingual books. Users can author
            their own content and re-sell it on the platform. Readers can
            purchase existing books to read and improve their language skills.
        </p>

        <h2>Agreement</h2>
        <p>
            The services are covered by Dutch law and are a contract between
            Mytoori and its users.{" "}
        </p>

        <h2>User Rights</h2>
        <p>
            Users have the right to request a copy of the personal data we hold
            for them.{" "}
        </p>

        <p>
            Users have the right to have their data amended if found to be
            incorrect.
        </p>

        <p>
            Users have the right to have their personal data removed from the
            system.
        </p>

        <h2>Services</h2>
        <p>
            At any time Mytoori may cease to offer its services. Users of the
            platform my request a copy of the content they purchased or created
            on the platform. Depending on the volume of request, Mytoori will be
            allowed appropriate time to respond to this request.{" "}
        </p>

        <p>
            All attempts will be made to provide the service 24 hours per day.
            This can however not be guaranteed.{" "}
        </p>

        <h2>Account Registration</h2>
        <p>
            An account is created and protected with an email address that
            serves as username and password. Other information may also be
            required. It’s not in our interest or intention to share this
            information with any third-parties. Mytoori customers purchase
            reading material on the website or create that reading material.{" "}
        </p>

        <p>
            Mytoori will aim to protect a customer’s account as much as
            possible. However Mytoori is not responsible for any losses users or
            authors incur.{" "}
        </p>

        <p>
            Those under the legal age of consent are advised against using this
            service without the explicit consent of a legal guardian or parent.{" "}
        </p>

        <h2>Limitation of liability</h2>
        <p>Mytoori is not liable for anything. </p>

        <h2>Amendments</h2>
        <p>
            This agreement can be modified at any time. Users will be notified
            of these policy changes.{" "}
        </p>

        <p>
            This Agreement was originally written in English and is the version
            that will be adhered to.{" "}
        </p>
    </div>
);

export default Terms;
