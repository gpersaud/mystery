import React from "react";
import { Link } from "react-router-dom";
import styles from "./NotFound.module.css";
import PropTypes from "prop-types";

const NotFound = ({ history }) => (
    <div className={styles.NotFoundContainer}>
        <div className={styles.NotFound}>
            <h1>404</h1>

            <p>The route {history.location.pathname} was not found.</p>
            <Link to="/">Shall we start over?</Link>
        </div>
    </div>
);

NotFound.propTypes = {
    history: PropTypes.object,
};

export default NotFound;
