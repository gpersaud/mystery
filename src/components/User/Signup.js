import React from "react";
import SignupForm from "../Form/SignupForm";
import { Helmet } from "react-helmet";
const Signup = (props) => (
    <div className="mt-register">
        <Helmet>
            <title>Signup</title>
        </Helmet>
        <SignupTitle />
        <SignupForm {...props} />
    </div>
);

export default Signup;

const SignupTitle = () => (
    <div className="mt-register__title">
        <h1> Create your account</h1>
        <p>
            Creates your personal library where you store free and purchased
            books.
        </p>
    </div>
);
