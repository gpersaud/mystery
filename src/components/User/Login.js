import React from "react";
import PropTypes from "prop-types";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { login } from "../../actions/userActions";
// import { loginRedirect } from "../../Auth/Auth";
import LoginForm from "../Form/LoginForm";
import { Helmet } from "react-helmet";
// import { Redirect } from "react-router-dom";
import "../../stylesheets/Login.scss";

const Login = ({ login, isLoading }) => {
    // if (window && isLoggedIn) {
    //     const path = loginRedirect();
    //     if (path && path !== "/login") return <Redirect to={path} />;
    //     return <Redirect to="/" />;
    // }

    return (
        <div className="mt-login__container">
            <Helmet>
                <title>Mytoori reader Login</title>
            </Helmet>
            <div className="mt-login">
                <div className="mt-login__text">
                    <span>
                        Practice{" "}
                        <strong>English, French, Spanish and more</strong> with
                        a bilingual story
                    </span>
                </div>
                <LoginForm action={login} isLoading={isLoading} />
            </div>
        </div>
    );
};

Login.propTypes = {
    auth: PropTypes.object,
    location: PropTypes.object,
    login: PropTypes.func.isRequired,
    isLoggedIn: PropTypes.bool,
    history: PropTypes.object,
    isLoading: PropTypes.bool,
};

const mapStateToProps = ({ userReducer: { isLoggedIn, isLoading } }) => ({
    isLoggedIn,
    isLoading,
});

const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            login,
        },
        dispatch
    );
export default connect(mapStateToProps, mapDispatchToProps)(Login);
