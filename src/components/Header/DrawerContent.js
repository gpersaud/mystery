import React from "react";
import PropTypes from "prop-types";
import DrawerLink from "./DrawerLink";
import Logo from "./Logo";
import { loginRedirect } from "../../Auth/Auth";
import LoginButton from "./LoginButton";

const DrawerContent = ({ toggleDrawer, logout, isLoggedIn, history }) => {
    const handleAuth = action => {
        toggleDrawer();
        if (action && typeof action === "function") action();
    };
    const login = () =>
        isLoggedIn
            ? null
            : history.push(loginRedirect(window.location.pathname));
    return (
        <div className="mt-drawer">
            <div className="mt-drawer__header">
                <Logo />
                <button className="btn-icon" onClick={toggleDrawer}>
                    <i className="material-icons">close</i>
                </button>
            </div>
            <div className="mt-drawer__buttons-container">
                <DrawerLink to="/" icon="home" onClick={toggleDrawer}>
                    home
                </DrawerLink>
                <DrawerLink
                    to="/books/featured"
                    onClick={toggleDrawer}
                    icon="store"
                >
                    shop
                </DrawerLink>
                {isLoggedIn ? (
                    <DrawerLink
                        to="/library"
                        onClick={toggleDrawer}
                        icon="local_library"
                    >
                        library
                    </DrawerLink>
                ) : null}
                <hr />
                <LoginButton
                    isLoggedIn={isLoggedIn}
                    action={handleAuth}
                    actionOptions={[login, logout]}
                />
            </div>

            <DrawerLink icon="info" to="/about" onClick={toggleDrawer}>
                about
            </DrawerLink>
            <DrawerLink
                icon="contact_support"
                to="/contact"
                onClick={toggleDrawer}
            >
                contact
            </DrawerLink>
        </div>
    );
};
DrawerContent.propTypes = {
    isOpen: PropTypes.bool,
    toggleDrawer: PropTypes.func,
    match: PropTypes.object,
    logout: PropTypes.func,
    isLoggedIn: PropTypes.bool,
    history: PropTypes.object
};

export default DrawerContent;
