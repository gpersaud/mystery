import React from "react";
import PropTypes from "prop-types";
import { Motion, spring } from "react-motion";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { logout } from "../../actions/userActions";
import DrawerContent from "./DrawerContent";
import { getLoginState } from "../../store/selectors/userSelectors";
import "../../stylesheets/Drawer.scss";

class Drawer extends React.Component {
    render() {
        let defaultStyle, endStyle;

        defaultStyle = {
            x: 0
        };

        if (this.props.isOpen) {
            endStyle = {
                x: spring(330, { stiffness: 271, damping: 16 })
            };
        } else {
            endStyle = {
                x: spring(0, { stiffness: 350, damping: 26 })
            };
        }
        return (
            <Motion defaultStyle={defaultStyle} style={endStyle}>
                {({ x }) => (
                    <div
                        className="mt-drawer__container"
                        style={{
                            transform: `translate3d(${x}px, 0, 0)`
                        }}
                    >
                        <DrawerContent {...this.props} />
                    </div>
                )}
            </Motion>
        );
    }
}

Drawer.propTypes = {
    isOpen: PropTypes.bool
};

const mapStateToProps = state => ({
    isLoggedIn: getLoginState(state)
});

const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            logout
        },
        dispatch
    );
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Drawer);
