import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import LanguageSwitch from "./LanguageSwitch";
import "../../stylesheets/_layout.scss";

/**
 * Check where the user is and have the close button behave differently.
 * @param {Object} props properties
 */
const ReaderButtons = props => {
    const reReadPage = new RegExp("^/read/+");
    const reBookPage = new RegExp("^/book/+");
    let returnVal = <div />;

    // only on the library reader page
    if (reReadPage.test(props.location.pathname)) {
        returnVal = (
            <div className="mt-navigation__context-buttons">
                <LanguageSwitch {...props}>
                    <i className="material-icons">loop</i>
                </LanguageSwitch>
                <ExitReaderButton {...props} />
            </div>
        );
    }

    // Only if it's the public book page
    // TODO: Clean up this code
    if (reBookPage.test(props.location.pathname)) {
        if (
            props.location.state &&
            props.location.state.sender === "bookItem"
        ) {
            returnVal = (
                <div className="mt-navigation__context-buttons">
                    <button onClick={() => props.history.goBack()}>
                        <i className="material-icons">close</i>
                    </button>
                </div>
            );
        } else {
            returnVal = (
                <div className="mt-navigation__context-buttons">
                    <Link to="/books/featured">Bookshop</Link>
                </div>
            );
        }
    }

    return <div>{returnVal}</div>;
};
ReaderButtons.propTypes = {
    location: PropTypes.object,
    history: PropTypes.object
};

const ExitReaderButton = () => {
    return (
        <span className="mt-navigation__actions">
            <Link to="/library">
                <i className="material-icons">close</i>
            </Link>
        </span>
    );
};
ExitReaderButton.propTypes = {
    location: PropTypes.object
};

export default ReaderButtons;
