import React from "react";
import PropTypes from "prop-types";

const IconButton = ({ children, icon }) => (
    <div className="mt-icon-button">
        <i className="material-icons">{icon ? icon : children}</i>
        {children}
    </div>
);

IconButton.propTypes = {
    children: PropTypes.any,
    onClick: PropTypes.func,
    to: PropTypes.string,
    icon: PropTypes.string
};
export default IconButton;
