import React from "react";
import PropTypes from "prop-types";
import "../../stylesheets/_layout.scss";

const HeaderActions = ({ toggleDrawer }) => (
    <div className="mt-navigation__actions">
        <Menu onClick={toggleDrawer} />
    </div>
);

HeaderActions.propTypes = {
    toggleDrawer: PropTypes.func
};

const Menu = ({ onClick }) => (
    <button onClick={onClick}>
        <i className="material-icons">menu</i>
    </button>
);
Menu.propTypes = {
    onClick: PropTypes.func
};

export default HeaderActions;
