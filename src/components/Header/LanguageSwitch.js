import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

const LanguageSwitch = ({ location, children }) => {
    const pathname = location.pathname.match(/\/i$/)
        ? location.pathname.replace(/\/i$/, "")
        : `${location.pathname}/i`;
    return (
        <Link
            replace
            className="btn btn-icon"
            to={{
                pathname
            }}
        >
            {children}
        </Link>
    );
};
LanguageSwitch.propTypes = {
    location: PropTypes.object,
    children: PropTypes.any
};

export default LanguageSwitch;
