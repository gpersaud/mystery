import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import IconButton from "./IconButton";

const DrawerLink = ({ children, onClick, to, icon }) => (
    <Link to={to} className="mt-drawer__button" onClick={onClick}>
        <IconButton icon={icon}>{children}</IconButton>
    </Link>
);

DrawerLink.propTypes = {
    children: PropTypes.any,
    onClick: PropTypes.func,
    to: PropTypes.string,
    icon: PropTypes.string
};
export default DrawerLink;
