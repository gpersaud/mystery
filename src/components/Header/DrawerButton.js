import React from "react";
import PropTypes from "prop-types";
import IconButton from "./IconButton";

const DrawerButton = ({ children, onClick, icon }) => (
    <button className="mt-drawer__button" onClick={onClick}>
        <IconButton icon={icon}>{children}</IconButton>
    </button>
);

DrawerButton.propTypes = {
    children: PropTypes.any,
    onClick: PropTypes.func,
    icon: PropTypes.string
};
export default DrawerButton;
