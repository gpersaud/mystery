import React from "react";
import PropTypes from "prop-types";
import Logo from "./Logo";
import { Link } from "react-router-dom";
import Drawer from "./Drawer";
import HeaderActions from "./HeaderActions";
import ReaderButtons from "./ReaderButtons";
import "../../stylesheets/_layout.scss";

class Header extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isOpen: false
        };
    }

    toggleDrawer = () => {
        this.setState(currentState => ({
            isOpen: !currentState.isOpen
        }));
    };

    closeDrawerAndGo = target => {
        this.setState(() => ({ isOpen: false }));
        this.props.history && this.props.history.push(target);
    };

    render() {
        return (
            <div className="mt-layout__header">
                <div className="mt-navigation__container">
                    <HeaderActions
                        {...this.props}
                        toggleDrawer={this.toggleDrawer}
                    />
                    <LogoContainer />
                    <ReaderButtons {...this.props} />
                </div>
                <Drawer
                    {...this.props}
                    isOpen={this.state.isOpen}
                    toggleDrawer={this.toggleDrawer}
                    action={this.closeDrawerAndGo}
                />
            </div>
        );
    }
}

Header.propTypes = {
    history: PropTypes.object
};

const LogoContainer = () => (
    <div className="mt-navigation__logo-container">
        <Link to="/" title="Mytoori home page">
            <Logo />
        </Link>
    </div>
);

export default Header;
