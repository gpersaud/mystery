import React from "react";
import PropTypes from "prop-types";
import DrawerButton from "./DrawerButton";

const LoginButton = ({ isLoggedIn, action, actionOptions }) => (
    <DrawerButton
        icon="exit_to_app"
        onClick={() => action(actionOptions[Number(isLoggedIn)])}
    >
        {isLoggedIn ? "logout" : "login"}
    </DrawerButton>
);

LoginButton.propTypes = {
    isLoggedIn: PropTypes.bool,
    parent: PropTypes.object,
    action: PropTypes.func,
    actionOptions: PropTypes.array
};

export default LoginButton;
