import React from "react";
import { mount } from "enzyme";
import Header from "../Header";
import Drawer from "../Drawer";
import { MemoryRouter } from "react-router-dom";
import { Provider } from "react-redux";
import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";

const props = {
    location: {
        pathname: "test"
    }
};

const createMockStore = configureMockStore([thunk]);
const store = createMockStore({
    userReducer: {
        isLoggedIn: true
    }
});

describe("Header", () => {
    let header;
    beforeEach(() => {
        const auth = {
            login: jest.fn(),
            logout: jest.fn(),
            isAuthenticated: jest.fn()
        };
        header = mount(
            <MemoryRouter>
                <Provider store={store}>
                    <Header auth={auth} {...props} />
                </Provider>
            </MemoryRouter>
        );
    });
    describe("should have rendered the header", () => {
        it("header should have a menu button", () => {
            const menuIcon = header.find("button > i.material-icons");
            expect(menuIcon).toBeDefined();
        });
        it("clicking the menu should render the drawer", () => {
            expect(header.find(Drawer)).toHaveProp({ isOpen: false });
            const menuButton = header.find(".mt-navigation__actions button");
            menuButton.simulate("click");
            const drawer = header.find("div.mt-drawer__container");
            expect(drawer).toBeDefined();
            expect(header.find(Drawer)).toHaveProp({ isOpen: true });
        });
    });
});
