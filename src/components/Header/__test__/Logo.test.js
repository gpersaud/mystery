import React from "react";
import { render } from "enzyme";
import Logo from "../Logo";

describe("Logo", () => {
  it("should have a mytoori title", () => {
    const logo = render(<Logo />);
    expect(logo.find("title").text()).toEqual("Mytoori");
  });
});
