import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

const LandingCollectionsList = ({ collections = [] }) => {
    const nodes = collections && collections.length > 0 && collections.map(({ _id, target, titles }, idx) => (
        <Link to={`books/${target}`} key={_id + idx} className="btn">
            {titles && titles.length ? titles[0].value : null}
        </Link>
    ));
    return <div className="mt-page__list">{nodes}</div>;
};

LandingCollectionsList.propTypes = {
    collections: PropTypes.array
};

export default LandingCollectionsList;
