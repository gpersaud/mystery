import React from "react";
import PropTypes from "prop-types";

const LPSteps = ({ data = [] }) => {
    const steps = data.map((item, i) => (
        <LPStepItem key={i + item.text} item={item} />
    ));

    return (
        <div>
            <h2>How it works</h2>
            <div className="mt-steps__container">{steps}</div>
        </div>
    );
};

LPSteps.propTypes = {
    data: PropTypes.array
};

export default LPSteps;

const LPStepItem = ({ item = {} }) => (
    <div className="mt-steps">
        <div className="mt-steps__header">
            <span />
            <span>{item.number}</span>
        </div>
        <div className="mt-steps__text">{item.text}</div>
    </div>
);

LPStepItem.propTypes = {
    item: PropTypes.object
};
