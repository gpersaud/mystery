export const header1Data = [
    `Read with subtitles <span
    class="pulse"
    role="img"
    aria-label="green circle indicating that the user should click on this title"
>
    
</span>`,
    `Lire avec sous-titres <span
    class="pulse"
    role="img"
    aria-label="green circle indicating that the user should click on this title"
>
    
</span>`,
    `Leer con subtítulos <span
    class="pulse"
    role="img"
    aria-label="green circle indicating that the user should click on this title"
>
    
</span>`,
    `Leggi con i sottotitoli <span
    class="pulse"
    role="img"
    aria-label="green circle indicating that the user should click on this title"
>
    
</span>`,
    `Leia com legendas <span
    class="pulse"
    role="img"
    aria-label="green circle indicating that the user should click on this title"
>
    
</span>`,
    `Lees met ondertiteling <span
    class="pulse"
    role="img"
    aria-label="green circle indicating that the user should click on this title"
>
    
</span>`,
    `Lesen mit Untertiteln <span
    class="pulse"
    role="img"
    aria-label="green circle indicating that the user should click on this title"
>
    
</span>`
];
export const header2Data = [
    `Tap
  <span
      class="pulse"
      role="img"
      aria-label="green circle indicating that the user should click on this title"
  >
      
  </span>   
  to easily translate and continue`,
    `Tik <span class="pulse"
  role="img"
  aria-label="green circle indicating that the user should click on this title"
></span> voor een makkelijke vertaling en lees verder`
];
