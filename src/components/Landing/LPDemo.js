import React from "react";
import PropTypes from "prop-types";
import LPDemoSentence from "./LPDemoSentence";
class LPDemo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            language: "en",
            index: 0
        };
    }

    handleClick() {
        this.setState(
            currentState =>
                currentState.index < this.props.data.length - 1
                    ? { index: ++currentState.index }
                    : { index: 0 }
        );
    }

    render() {
        return (
            <LPDemoSentence
                data={this.props.data}
                language={this.state.language}
                index={this.state.index}
                handleClick={this.handleClick.bind(this)}
                className={this.props.className}
            />
        );
    }
}

LPDemo.propTypes = {
    data: PropTypes.array,
    className: PropTypes.string
};

export default LPDemo;
