import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { fetchCollections } from "../../actions/collectionActions";
import LandingCollectionsList from "./LandingCollectionsList";
import { scrollToTop } from "../../utils/helpers";
// import LPCallToAction from "./LPCallToAction";
import LPSteps from "./LPSteps";
import { data as LPStepsData } from "./LPStepsData";
import Footer from "../Footer/Footer";
import LPDemo from "./LPDemo";
import { header1Data, header2Data } from "./landingData";
import { Helmet } from "react-helmet";
import { Link } from "react-router-dom";

class Landing extends React.Component {
    componentDidMount() {
        this.props.fetchCollections();
        scrollToTop();
    }
    componentDidUpdate() {
        scrollToTop();
    }
    render() {
        const filteredCollections = this.props.collections.filter(
            collection =>
                collection.titles[0].value !== "Featured" &&
                collection.titles[0].value !== "Free"
        );
        return (
            <div className="mt-page__container">
                <Helmet>
                    <title>Read with subtitles on Mytoori</title>
                </Helmet>
                <LPDemo data={header1Data} className="blurb-header active" />

                <div className="mt-page__hero-container">
                    <strong>Custom made stories in two languages.</strong>{" "}
                    Switch language while reading.{" "}
                    <Link to="/books/featured">Browse Book Store.</Link>
                </div>
                <LPDemo data={header2Data} />
                <div className="mt-page__hero-container">
                    Our specially made books are great if you already have the
                    basics but need to{" "}
                    <strong>improve vocabulary and structure</strong>
                </div>

                <div className="mt-page__video-container">
                    <iframe
                        title="Creating bilingual minds by Naja Ferjan Ramirez"
                        src="https://www.youtube.com/embed/Bp2Fvkt-TRM?rel=0"
                        frameBorder="0"
                        allow="autoplay; encrypted-media"
                        allowFullScreen
                    />
                </div>

                {/* <LPCallToAction {...this.props} /> */}

                <LPSteps data={LPStepsData} />
                <p className="blurb">
                    Spend time with a{" "}
                    <span role="img" aria-label="heart icon">
                        💛
                    </span>{" "}
                    <strong>foreign language</strong>
                </p>
                <p>Pick one from the list below to get started.</p>
                <LandingCollectionsList collections={filteredCollections} />

                <div className="mt-page__container-section">
                    <p className="blurb">
                        Library in the cloud{" "}
                        <span role="img" aria-label="cloud icon">
                            🌤
                        </span>
                    </p>
                    <p>
                        <strong>
                            A personal library is created for every new account.
                        </strong>{" "}
                        Purchase a book and it is immediately added to your
                        library. This includes free books. Your books will
                        always be available. From any device.
                    </p>
                </div>
                <div className="mt-page__container-section">
                    <p className="blurb">
                        Bookmarking my page{" "}
                        <span role="img" aria-label="search icon">
                            🔍
                        </span>
                    </p>
                    <p>
                        <strong>
                            When you click the text, the app remembers where you
                            are in the text.
                        </strong>{" "}
                        This is used as the last bookmark. If you open a book
                        again, it will take you to the last translation you
                        made. Want to make sure you can start reading again from
                        where you left off? Simply tap the text. The app will
                        remember that text and take you back there next time you
                        open that particular story.
                    </p>
                </div>
                <Footer />
            </div>
        );
    }
}

Landing.propTypes = {
    collections: PropTypes.array,
    fetchCollections: PropTypes.func
};

const mapStateToProps = state => ({
    collections: state.booksReducer.collections
});

const mapDispatchToProps = dispatch =>
    bindActionCreators({ fetchCollections }, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Landing);
