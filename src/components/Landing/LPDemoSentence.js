import React from "react";
import PropTypes from "prop-types";

const LPDemoSentence = ({
    index,
    data = {},
    handleClick,
    className = "blurb active"
}) => (
    <div
        onClick={handleClick}
        className={className}
        dangerouslySetInnerHTML={{ __html: data[index] }}
    />
);
LPDemoSentence.propTypes = {
    index: PropTypes.number,
    data: PropTypes.array,
    handleClick: PropTypes.func,
    className: PropTypes.string
};
export default LPDemoSentence;
