import React from "react";
import PropTypes from "prop-types";
import { isAuthenticated } from "../../Auth/Auth";
import { Link } from "react-router-dom";

import SignupForm from "../Form/SignupForm";
const LPCallToAction = props =>
    isAuthenticated() ? (
        <Link className="mt-page__library button" to="/library">
            <label>Library</label>
        </Link>
    ) : (
        <SignupForm {...props} />
    );

const LPCallToActionMessage = () =>
    isAuthenticated() ? null : (
        <div className="mt-page__signup-message">
            <span>Claim your bilingual library</span>
        </div>
    );
LPCallToActionMessage.propTypes = {
    isAuthenticated: PropTypes.bool
};

const LPCallToActionContainer = props =>
    isAuthenticated() ? (
        <LPCallToAction {...props} />
    ) : (
        <div className="mt-page__signup-container">
            <LPCallToActionMessage {...props} />
            <LPCallToAction {...props} />
        </div>
    );

export default LPCallToActionContainer;
