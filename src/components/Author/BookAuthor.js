import React from "react";
import PropTypes from "prop-types";

const BookAuthor = ({ book }) => {
    return (
        <div className="mt-author">
            <label>Author</label>
            <div className="mt-author__text">
                {book.authorFirstname || book.authorLastname
                    ? book.authorFirstname + " " + book.authorLastname
                    : book.author}
            </div>
        </div>
    );
};

BookAuthor.propTypes = {
    book: PropTypes.shape({
        authorFirstname: PropTypes.string,
        authorLastname: PropTypes.string,
        author: PropTypes.string
    })
};

export default BookAuthor;
