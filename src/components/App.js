import React from "react";
import "../stylesheets/_main.scss";
import Header from "./Header/Header";
import { Switch, Route } from "react-router-dom";
import FreeReader from "../components/Reader/FreeReader";
import Alert from "react-s-alert";
import "react-s-alert/dist/s-alert-default.css";
import "react-s-alert/dist/s-alert-css-effects/stackslide.css";

import Content from "./Content/Content";

const App = props => (
    <Switch>
        <Route path="/book/read/:bookId/:inverted?" component={FreeReader} />

        <Route
            render={() => (
                <div className="mt-layout__container">
                    <Header {...props} />
                    <Content {...props} />
                    <Alert
                        stack={{ limit: 3 }}
                        timeout={3000}
                        position="top-right"
                        effect="stackslide"
                        offset={100}
                    />
                </div>
            )}
        />
    </Switch>
);

export default App;
