import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { signup } from "../../actions/userActions";
import LPSignupForm from "./LPSignupForm";
import { LazyLoadImage } from "react-lazy-load-image-component";
import "../../stylesheets/LPSignup.scss";
import "../../stylesheets/AuthorPromo1.scss";

const LPSignup = props => (
    <div id="signup" className="mt-lpsignup">
        <div className="mt-lpsignup__form-container">
            <LPSignupForm {...props} />

            <Link
                className="mt-lpsignup__action-alternative"
                to="/login"
                alt="go to login page"
            >
                login
            </Link>
        </div>
        <AuthorPromo1 />
    </div>
);

const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            signup
        },
        dispatch
    );

export default connect(
    null,
    mapDispatchToProps
)(LPSignup);

const booksList = [
    {
        title: "Voyage au centre de la terre",
        imageUrl:
            "https://res.cloudinary.com/mytoori/image/upload/c_scale,f_auto,w_100/v1544730140/mytoori/covers/voyageaucentredelaterre-sample"
    },
    {
        title: "Pride and Prejudice",
        imageUrl:
            "https://res.cloudinary.com/mytoori/image/upload/c_scale,f_auto,w_100/v1544730141/mytoori/covers/prideandprejudice"
    },
    {
        title: "De vliegende hollander",
        imageUrl:
            "https://res.cloudinary.com/mytoori/image/upload/c_scale,f_auto,w_100/v1558464997/mytoori/covers/devliegendehollander"
    }
];

const AuthorPromo1 = () => {
    return (
        <div className="AuthorPromo1-container">
            <div className="AuthorPromo1">
                <section>
                    <ul>
                        {booksList.map(item => (
                            <li key={item.imageUrl}>
                                <LazyLoadImage
                                    src={item.imageUrl}
                                    alt={item.title}
                                />
                            </li>
                        ))}
                    </ul>
                </section>
                <p>
                    Get published stories from our bookshop{" "}
                    <Link to="/shop">mytoori.com/shop</Link>
                </p>
            </div>
        </div>
    );
};
