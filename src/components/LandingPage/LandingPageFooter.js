import React from "react";
import { Link } from "react-router-dom";

const AuthorFooter = () => (
    <div className="AuthorFooter-container">
        <footer className="AuthorFooter">
            <nav>
                <ul>
                    <li>
                        <Link to="/about">About</Link>
                    </li>
                    <li>
                        <Link to="/contact">Contact</Link>
                    </li>
                    <li>
                        <Link to="/faq">FAQ</Link>
                    </li>
                </ul>
            </nav>
        </footer>
    </div>
);

export default AuthorFooter;
