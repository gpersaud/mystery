import React from "react";

const LPHowTo = () => (
    <>
        <section className="mt-landing__section-h">
            <div className="mt-landing__section-h-img-container">
                <img
                    src="/mytoori.gif"
                    alt="iphone demo of mytoori showing the text of a book"
                    className="img-iphone-demo"
                />
            </div>
            <article>
                <h2>1 book, 2 languages</h2>
                <p>
                    Tap the sentence and you’ll immediately see it in the second
                    language of the book.{" "}
                    <strong>
                        Not a dictionary translation but text edited and
                        reviewed by real people.
                    </strong>
                </p>
            </article>
        </section>
        <section className="mt-landing__section-h">
            <div className="mt-landing__section-h-img-container">
                <img src="/girl_studying.svg" alt="vector girl studying" />
            </div>
            <article>
                <h2>Fluency can be easy and fun</h2>
                <p>
                    Tap the sentence and you’ll immediately see it in the second
                    language of the book. Have fun reading a simple story and
                    maybe forget you’re learning.
                    <br />
                    Our stories are either classics or works created by writers
                    and translators. People who already know the language you’re
                    learning.
                </p>
            </article>
        </section>
    </>
);

export default LPHowTo;
