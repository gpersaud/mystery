import React from "react";
import { isAuthenticated } from "../../Auth/Auth";
import { getEditorLink } from "../../utils/helpers";

const LaunchEditor = ({ full = true }) => {
    if (!isAuthenticated()) return "Not loggedin";
    return (
        <div className="AuthorSignup-loggedin">
            <a
                href={getEditorLink()}
                target="MytooriEditor"
                alt="Launches Edytoori, a bilingual book editing and management platform"
            >
                Launch Editor
            </a>
            {full && (
                <p>
                    Launches the bilingual book editing and management platform
                </p>
            )}
        </div>
    );
};

export default LaunchEditor;
