import React from "react";
import { Link } from "react-router-dom";
import "../../stylesheets/LoggedInOptions.scss";
import { getEditorLink } from "../../utils/helpers";
import constants from "../../config/constants";

const LoggedInOptions = () => (
    <nav className="LoggedInOptions">
        <Link to="/library">
            <img src="/local_library-24px.svg" alt="local library icon" />
            <h1>Library</h1>
            <label>All your books in one place</label>
        </Link>
        <Link to="/shop">
            <img src="/shop-24px.svg" alt="shop icon" />
            <h1>Shop</h1>
            <label>Buy & download books</label>
        </Link>
        <a
            href={`${getEditorLink()}/token/${window.localStorage.getItem(
                constants.ACCESS_TOKEN
            )}`}
            target="MytooriEditor"
            alt="Launches Edytoori, a bilingual book editing and management platform"
        >
            <img src="/edit-24px.svg" alt="shop icon" />
            <h1>Editor</h1>
            <label>Create your own content</label>
        </a>
    </nav>
);

export default LoggedInOptions;
