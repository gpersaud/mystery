import React from "react";
import PropTypes from "prop-types";
import "../../stylesheets/LandingPage.scss";
import LPSignup from "./LPSignup";
import LandingPageAnimation1 from "./LandingPageAnimation1";
import LoggedInOptions from "./LoggedInOptions";
import { connect } from "react-redux";
import { getLoginState } from "../../store/selectors/userSelectors";
import InstallSection from "./InstallButton";
import AuthorLandingPage from "./AuthorLandingPage";
import LandingPageFooter from "./LandingPageFooter";

const LandingPage = (props) => (
    <div className="mt-landing">
        <div className="mt-landing__intro">
            <img
                src="https://res.cloudinary.com/mytoori/image/upload/f_auto,fl_progressive/v1572297459/bilingual_stories"
                alt="bilingual stories"
            />
            <article className="mt-landing__section">
                <h1 className="mt-landing__heading">
                    <div>Practice</div> English, French, Spanish and more
                </h1>
                <div className="mt-landing__section-2-text">
                    <LandingPageAnimation1 />
                </div>
            </article>
            <div className="mt-landing__intro-video">
                <video
                    controls
                    height="480"
                    autoPlay={true}
                    loop={true}
                    muted={true}
                    playsInline={true}
                >
                    <source
                        src="https://res.cloudinary.com/mytoori/video/upload/c_scale,f_auto,h_480,q_auto,w_222/v1573673300/Mytoori-tap.webm"
                        type="video/webm"
                    />
                    <source
                        src="https://res.cloudinary.com/mytoori/video/upload/c_scale,f_auto,q_auto,h_480/v1573673300/Mytoori-tap.mp4"
                        type="video/mp4"
                    />
                    It looks like your browser doesn&apost support embedded
                    videos. Sorry.
                </video>
            </div>
        </div>

        {props.isLoggedIn ? <LoggedInOptions /> : <LPSignup {...props} />}
        <InstallSection />
        <AuthorLandingPage isLoggedIn={props.isLoggedIn} />
        <LandingPageFooter />
    </div>
);

const mapStateToProps = (state) => ({
    isLoggedIn: getLoginState(state),
});

LandingPage.propTypes = {
    isLoggedIn: PropTypes.bool,
};

export default connect(mapStateToProps)(LandingPage);
