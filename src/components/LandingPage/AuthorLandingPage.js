import React from "react";
import PropTypes from "prop-types";
import { LazyLoadImage } from "react-lazy-load-image-component";
import "../../stylesheets/AuthorLandingPage.scss";
import { Link } from "react-router-dom";

const AuthorLandingPage = ({ isLoggedIn }) => (
    <>
        <div className="AuthorLandingPage">
            <LazyLoadImage
                className="AuthorLandingPage--promo"
                src="https://res.cloudinary.com/mytoori/image/upload/f_auto,fl_any_format.preserve_transparency.progressive/v1572295884/publish_your_story"
                alt="publish your own story"
            />
            <div className="card">
                <h1>Already speak multiple languages?</h1>
                <p>
                    Write or translate your own stories and offer them up for
                    sale. Help your readers practice and improve their skills in
                    a language you already master.
                </p>
                {!isLoggedIn && <Link to="/signup">Signup</Link>}
            </div>
            <HowDoesItWork />
        </div>
    </>
);

AuthorLandingPage.propTypes = {
    isLoggedIn: PropTypes.bool,
};

export default AuthorLandingPage;

const HowDoesItWork = () => (
    <>
        <section className="AuthorLandingPage--write">
            <h1>Write</h1>
            <p>
                Create a story in two languages. Easily match the languages
                together creating value for readers and language learners.
            </p>
        </section>
        <section className="AuthorLandingPage--translation">
            <h1>Translate</h1>
            <p>
                Give your readers an incontext “translation”. Help them follow
                the story when it becomes to difficult in the first language
            </p>
        </section>
        <LazyLoadImage
            className="AuthorLandingPage--editor"
            srcSet="
                https://res.cloudinary.com/mytoori/image/upload/c_scale,w_240/v1571121010/editor_with_lens 240w, 
                https://res.cloudinary.com/mytoori/image/upload/c_scale,w_480/v1571121010/editor_with_lens 480w, 
                https://res.cloudinary.com/mytoori/image/upload/c_scale,w_960/v1571121010/editor_with_lens 960w"
            sizes="(max-width: 240px) 240px,(max-width: 480px) 480px, 960px"
            src="https://res.cloudinary.com/mytoori/image/upload/c_scale,w_960/v1571121010/editor_with_lens"
            alt="editoory editor screenshot"
        />
    </>
);
