import React from 'react';
import PropTypes from 'prop-types';
import '../../stylesheets/LPSteps.scss';

const LPSteps = ({ data = [] }) => {
    const steps = data.map((item, i) => (
        <LPStepItem key={i + item.text} item={item} />
    ));

    return (
        <div className="mt-lpsteps__container">
            <h2>How it works</h2>
            <div className="mt-lpsteps">{steps}</div>
        </div>
    );
};

LPSteps.propTypes = {
    data: PropTypes.array
};

export default LPSteps;

const LPStepItem = ({ item = {} }) => (
    <div className="mt-lpsteps__item">
        <div className="mt-lpsteps__item-header">
            <span className="mt-lpsteps__item-number">{item.number}</span>
            <span className="mt-lpsteps__item-title">{item.title}</span>
        </div>
        <div className="mt-lpsteps__item-text">{item.text}</div>
    </div>
);

LPStepItem.propTypes = {
    item: PropTypes.object
};
