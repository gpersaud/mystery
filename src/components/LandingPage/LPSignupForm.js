import React from "react";
import PropTypes from "prop-types";
import "../../stylesheets/LPSignup.scss";
import Alert from "react-s-alert";

class LPSignupForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: null,
            tos: false,
        };

        this.handleChange = this.handleChange.bind(this);
        this.acceptTerms = this.acceptTerms.bind(this);
        this.validateSubmit = this.validateSubmit.bind(this);
    }

    handleChange(e) {
        const { type, checked, value, name } = e.currentTarget;
        const newValue = type === "checkbox" ? checked : value;
        this.setState({ [name]: newValue });
    }

    acceptTerms() {
        this.setState((curr) => ({ tos: !curr.tos }));
    }

    validateSubmit(e) {
        e.preventDefault();
        const { tos, email } = this.state;
        validateFirstAccountStep(tos, email) &&
            this.props.history.push(`/signup/${email}`);
        return false;
    }

    // Format the url where the form will be pointed to
    actionUrl({ tos, email }) {
        return tos && email ? `/signup/${email}` : "";
    }

    render() {
        return (
            <form
                action={this.actionUrl(this.state)}
                onSubmit={this.validateSubmit}
                className="mt-lpsignup__form"
            >
                <div className="mt-lpsignup__input">
                    <label>Email</label>
                    <input
                        title="Fill in your email address"
                        type="email"
                        name="email"
                        placeholder="Enter email address here"
                        onChange={this.handleChange}
                        required
                    />
                </div>
                <div className="mt-lpsignup__tos" onClick={this.acceptTerms}>
                    <label>
                        <div>
                            We promise not to abuse your data. You can easily
                            request it removed from our database.
                        </div>
                    </label>
                    <div>
                        <input
                            title="Check this box to indicate you agree with the terms of service"
                            type="checkbox"
                            name="tos"
                            readOnly
                            checked={this.state.tos}
                            required
                        />

                        <label>
                            I agree with the{" "}
                            <a
                                href="/terms"
                                alt="terms of service link"
                                title="Opens the terms of serivce documentation page"
                                target="_blank"
                            >
                                Terms of Service (ToS) and privacy policy
                            </a>
                        </label>
                        <svg
                            width="87"
                            height="51"
                            viewBox="0 0 87 51"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                        >
                            <path
                                d="M0 7.5L12.9904 0L37.9904 43.3013L25 50.8013L0 7.5Z"
                                fill="#26A65B"
                            />
                            <path
                                d="M78.6218 3.00002L86.1218 15.9904L25.5 50.9904L18 38L78.6218 3.00002Z"
                                fill="#26A65B"
                            />
                        </svg>
                    </div>
                </div>
                <button
                    className="mt-lpsignup__action"
                    title="Takes you to next step where you can choose a password"
                >
                    Create Account
                </button>
            </form>
        );
    }
}

LPSignupForm.propTypes = {
    history: PropTypes.object,
};

export default LPSignupForm;

const validateFirstAccountStep = (tos, email) => {
    function isValid(field, message) {
        if (!field) {
            Alert.info(message);
            return null;
        } else {
            return true;
        }
    }

    const result = [
        { field: email, message: "Please enter your email address to proceed" },
        {
            field: tos,
            message: "Please accept the terms and condition to proceed",
        },
    ];
    return result.every((item) => isValid(item.field, item.message));
};
