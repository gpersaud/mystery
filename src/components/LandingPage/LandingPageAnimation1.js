import React, { useState } from "react";

export const LandingPageAnimation1 = () => {
    const [isActive, toggle] = useState(false);
    return (
        <div className="mt-toggle" onClick={() => toggle(!isActive)}>
            <p className={`mt-toggle-text ${!isActive}`}>
                A single tap <strong>flips the sentence</strong> from{" "}
                <em>French</em> to <em>English</em>. Other language combinations
                also available
            </p>
            <p className={`mt-toggle-text ${isActive}`}>
                <strong>Tapp le texte et ça change</strong> du <em>français</em>{" "}
                en <em>anglais</em>. Autres combinaisons sons aussi disponibles
            </p>
        </div>
    );
};

export default LandingPageAnimation1;
