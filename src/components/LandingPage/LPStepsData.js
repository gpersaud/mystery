export default [
    {
        number: 1,
        title: 'Purchase Book',
        text:
            'Purchased books are stored in your libary where you can access them from anywhere. Free content is available as well of course.'
    },
    {
        number: 2,
        title: 'Read Anywhere',
        text:
            'Access your books from anywhere. Click to start reading from where you left off and keep brushing up your language skills'
    },
    {
        number: 3,
        title: 'Tap to Switch Language',
        text:
            'Don’t understand the text? Simply tap on it. The block of text switches to your mother tongue. You can now easily understand what the text is about.'
    }
];
