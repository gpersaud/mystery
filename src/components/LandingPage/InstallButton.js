import React, { useState, useEffect } from "react";

const InstallButton = () => {
    // clear the global object
    const postInstall = () => {
        window.deferredInstallPrompt = null;
        setInstallReady(false);
        window.gtag("event", "install", {
            event_category: "engagement",
            event_label: "installed",
            value: 1
        });
    };

    const installReady = () => {
        setInstallReady(true);
        window.addEventListener("appinstalled", postInstall);
    };
    const [isInstallReady, setInstallReady] = useState(false);

    useEffect(() => {
        if (window.deferredInstallPrompt) {
            installReady();
        } else {
            window.addEventListener("beforeinstallprompt", installReady);
        }
    });

    const performInstall = () => {
        window.gtag("event", "install", {
            event_category: "engagement",
            event_label: "install_initiated",
            value: 1
        });
        window.deferredInstallPrompt.prompt();
        setInstallReady(false);
    };

    if (!isInstallReady) return null;
    return (
        <div className="mt-install-button__container">
            <div>
                <header>
                    <h1>Install on your device</h1>
                </header>
                <p>
                    Mytoori is easier and faster to use when installed on your
                    device. Click the button below to install no matter what
                    device you have. Be it Android, iOS, MacOS or Windows. It
                    all just works. Install and start reading.
                </p>
            </div>
            <button onClick={performInstall}>Install</button>
        </div>
    );
};

export default InstallButton;
