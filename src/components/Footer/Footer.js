import React from "react";
import Logo from "../Header/Logo";

const Footer = () => (
    <footer className="mt-footer">
        <Logo size="large" styles={{ color: "#212121" }} />
        <div className="mt-footer-copyright">
            Amsterdam {new Date().getFullYear()} &copy; &trade;
        </div>
    </footer>
);

export default Footer;
