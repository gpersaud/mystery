import { createStore, applyMiddleware } from "redux";
// import { logger } from "redux-logger";
import reducer from "./reducers/index";
import thunkMiddleware from "redux-thunk";
// import { composeWithDevTools } from "redux-devtools-extension";

/**
 * Check if redux data is provided by the server
 * If there is use it. Otherwise continue as normal
 */
const getReducerData = () => {
    if (typeof window === "undefined") return reducer;

    // get REDUX data from server
    const { __REDUX_INITIAL_DATA__ = "" } = window;
    if (
        __REDUX_INITIAL_DATA__ &&
        typeof __REDUX_INITIAL_DATA__ === "string" &&
        __REDUX_INITIAL_DATA__.length > 0
    ) {
        delete window.__REDUX_INITIAL_DATA__;
        return () => JSON.parse(__REDUX_INITIAL_DATA__); // return a function
    }
    return reducer; // this is also a function
};

let store;

// if (process.env.NODE_ENV !== "production" && typeof window !== "undefined") {
//     //** Normal dev mode so the dev tools should be loaded if available */
//     store = createStore(
//         getReducerData(),
//         undefined,
//         composeWithDevTools(applyMiddleware(thunkMiddleware, logger))
//     );

//     delete window.__REDUX_INITIAL_DATA__;
// } else

if (typeof window === "undefined") {
    /**
     * Server side rendering
     * Get the redux data and load the middlewares without dev tools
     */
    store = createStore(
        getReducerData(),
        undefined,
        applyMiddleware(thunkMiddleware)
    );
} else {
    /**
     * client side rendering for production
     */
    store = createStore(reducer, undefined, applyMiddleware(thunkMiddleware));
}

export default store;
