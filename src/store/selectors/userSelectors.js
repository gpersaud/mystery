export const getLoginState = (state) => {
    if (state && state.userReducer) {
        return state.userReducer.isLoggedIn;
    }
};
