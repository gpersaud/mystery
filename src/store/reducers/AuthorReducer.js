import constants from "../../config/constants";

let init = {
    authors: []
};

export function authorReducer(state = init, { data, type }) {
    switch (type) {
        case constants.FETCHED_AUTHOR: {
            const newAuthors = state.authors;
            const authorIndex = newAuthors.findIndex(
                author => author.userId === data._id
            );
            const newData = {
                userId: data._id,
                emails: [...data.emails]
            };
            authorIndex > -1
                ? newAuthors.splice(authorIndex, 1, newData)
                : newAuthors.push(newData);

            return {
                ...state,
                authors: [...newAuthors]
            };
        }

        default:
            return state;
    }
}
