import constants from "../../config/constants";
let init = {
    languages: []
};
export function filterReducer(state = init, { data, type }) {
    switch (type) {
        case constants.LANGUAGE_SELECTED: {
            return {
                ...state,
                languages: [...state.languages, data]
            };
        }
        // silences eslint warning as this is intentional
        // falls through
        default:
            return state;
    }
}
