import { booksReducer } from "../BooksReducer";
import constants from "../../../config/constants";
import casual from "casual";

describe("Books reducer", () => {
    it("should return the initial state", () => {
        expect(booksReducer(undefined, {})).toEqual({
            books: [],
            collections: [],
            covers: [],
            currentBookId: "",
            filteredBooks: [],
            isLoading: false,
            languages: [],
            selectedBook: {},
            didFetchLibrary: false,
            selectedBookStats: [],
            isLoadingStats: false,
            allBooksLoaded: false
        });
    });

    it("should handle BOOKS_RECEIVED", () => {
        const data = {
            books: [
                {
                    title1: casual.title,
                    title2: casual.title,
                    author: casual.author
                }
            ],
            isLoading: false,
            allBooksLoaded: false
        };
        expect(
            booksReducer(
                {},
                {
                    type: constants.BOOKS_RECEIVED,
                    data: data.books
                }
            )
        ).toEqual(data);
    });

    it("should store covers received", () => {
        const data = {
            bookId: casual.integer(0, 1000),
            coverId: casual.integer(0, 1000),
            coverUrl: casual.url
        };

        const action = {
            type: constants.COVER_RECEIVED,
            data
        };
        const result = booksReducer({ covers: [data] }, action);
        expect(result).toEqual({ covers: [data] });
    });

    it("should store the collections received", () => {
        const data = [
            {
                _id: "d48qdrDaMGQW6aA9E",
                id: "25338999275",
                title1: "Featured",
                title2: "",
                cover_image: "",
                cover_image_type: "",
                status: "public",
                password: "",
                target: "featured"
            }
        ];

        const action = {
            type: constants.COLLECTIONS_RECEIVED,
            data
        };
        const result = booksReducer({}, action);
        expect(result).toEqual({ collections: data, allBooksLoaded: false });
    });
});
