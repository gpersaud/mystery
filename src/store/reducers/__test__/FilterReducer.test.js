import { filterReducer } from "../FilterReducer";
import constants from "../../../config/constants";
import casual from "casual";

describe("Filter reducer", () => {
  it("should return the initial state", () => {
    const state = undefined,
      action = {};
    expect(filterReducer(state, action)).toEqual({
      languages: []
    });
  });
  it("should handle a selected language", () => {
    const data = {
      type: constants.LANGUAGE_SELECTED,
      data: casual.word
    };

    const state = {
      languages: []
    };

    /**
     * The final state object will have an array
     * of selected language ids
     */
    expect(filterReducer(state, data)).toEqual({ languages: [data.data] });
  });
});
