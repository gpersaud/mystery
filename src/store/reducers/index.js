import { combineReducers } from "redux";
import { booksReducer } from "./BooksReducer";
// import { filterReducer } from "./FilterReducer";
import { readerReducer } from "./ReaderReducer";
import { libraryReducer } from "./LibraryReducer";
import { orderReducer } from "./OrderReducer";
import { messageReducer } from "./MessageReducer";
import { authorReducer } from "./AuthorReducer";
import { userReducer } from "./userReducer";

export default combineReducers({
    booksReducer,
    readerReducer,
    libraryReducer,
    orderReducer,
    messageReducer,
    authorReducer,
    userReducer
});
