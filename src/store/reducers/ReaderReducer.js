import constants from "../../config/constants";

let init = {
    book: {},
    bookmarks: {},
    isLoading: false
};

export function readerReducer(state = init, { data, type }) {
    switch (type) {
        case constants.FETCHING_BOOK_CONTENT: {
            return {
                ...state,
                isLoading: true
            };
        }
        case constants.FETCHED_BOOK_CONTENT: {
            return {
                ...state,
                isLoading: false,
                book: {
                    ...data
                }
            };
        }

        // added to support the free reader
        case constants.BOOK_RECEIVED: {
            return {
                ...state,
                isLoading: false,
                book: { ...data }
            };
        }

        case constants.GET_BOOKMARK: {
            return {
                ...state,
                bookmarks: { ...data }
            };
        }

        default: {
            return {
                ...state
            };
        }
    }
}
