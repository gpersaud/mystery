import { messageConstants } from "../../config/constants";

let init = {
    message: { level: "error", message: "this is a test from the reducer" }
};

export function messageReducer(state = init, { data, type }) {
    switch (type) {
        case messageConstants.MESSAGE_RECEIVED: {
            return {
                ...state,
                message: data
            };
        }

        default:
            return state;
    }
}
