import constants from "../../config/constants";

let init = {
    orders: [],
    isFetchingOrder: undefined,
    order: {},
    isPurchasing: false,
    purchaseSuccess: undefined,
    purchasedBook: null
};

export const orderReducer = (state = init, { data, type }) => {
    switch (type) {
        case constants.FETCHING_BOOK: {
            return {
                ...state,
                isPurchasing: false
            };
        }
        case constants.PURCHASING_BOOK: {
            return {
                ...state,
                isPurchasing: true,
                purchaseSuccess: undefined
            };
        }

        case constants.PURCHASE_STATUS: {
            return {
                ...state,
                purchaseSuccess: data && data.purchaseSuccess,
                purchasedBook: data && data.isbn,
                isPurchasing: false
            };
        }
        case constants.FETCHING_ORDER: {
            return {
                ...state,
                isFetchingOrder: true,
                isPurchasing: false
            };
        }

        // TODO: optimize
        case constants.RECEIVED_ORDER: {
            const orders = [...state.orders];
            let orderIndex = orders.findIndex(o => o._id === data._id);
            if (orderIndex > -1) {
                orders.splice(orderIndex, 1, data);
            } else {
                orders[orders.length] = data;
            }
            return {
                ...state,
                isFetchingOrder: false,
                orders
            };
        }

        default: {
            return {
                ...state
            };
        }
    }
};
