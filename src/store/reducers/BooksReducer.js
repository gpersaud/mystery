import constants from "../../config/constants";

let init = {
    books: [],
    filteredBooks: [],
    covers: [],
    languages: [],
    isLoading: false,
    isLoadingStats: false,
    collections: [],
    selectedBook: {},
    didFetchLibrary: false,
    currentBookId: "",
    selectedBookStats: [],
    allBooksLoaded: false
};
export function booksReducer(state = init, { data, type }) {
    switch (type) {
        case constants.FETCHING_BOOK_STATS: {
            return {
                ...state,
                isLoadingStats: true
            };
        }
        case constants.BOOKS_RECEIVED: {
            const { books: currentBooks = {} } = state;
            return {
                ...state,
                books: [...data],
                isLoading: false,
                allBooksLoaded: currentBooks.length === data.length
            };
        }
        case constants.COVER_RECEIVED: {
            const covers = [...state.covers];
            const coverIndex = covers.findIndex(
                cover => cover.coverId === data.coverId
            );
            coverIndex > -1
                ? covers.splice(coverIndex, 1, data)
                : covers.push(data);

            return {
                ...state,
                covers
            };
        }
        case constants.COLLECTIONS_RECEIVED: {
            return {
                ...state,
                allBooksLoaded: false,
                collections: [...data]
            };
        }
        // falls through
        case constants.FETCHING_BOOKS: {
            return {
                ...state,
                isLoading: true
            };
        }

        case constants.FETCHING_BOOK: {
            // store the book data in user's profile
            return {
                ...state,
                isLoading: true,
                currentBookId: data
            };
        }

        case constants.BOOK_RECEIVED: {
            return {
                ...state,
                isLoading: false,
                books: [{ ...data }],
                selectedBook: { ...data }
            };
        }
        case constants.BOOK_SELECTED: {
            if (state.selectedBook._id === data._id) return state;
            const selectedBook = state.books.find(
                book => book._id === data._id
            );
            return {
                ...state,
                selectedBook,
                selectedBookStats: []
            };
        }

        case constants.DOES_USER_OWN_SELECTED_BOOK: {
            return {
                ...state,
                didFetchLibrary: true
            };
        }
        case constants.RECEIVED_BOOK_STATS: {
            return {
                ...state,
                isLoadingStats: false,
                selectedBookStats: data
            };
        }
        // silences eslint warning as this is intentional
        // falls through
        default:
            return state;
    }
}
