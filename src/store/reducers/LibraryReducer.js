import constants from "../../config/constants";

const init = {
    books: [],
    covers: [],
    isLoading: false
};

export function libraryReducer(state = init, { data, type }) {
    switch (type) {
        case constants.FETCHING_LIBRARY:
            return {
                ...state,
                isLoading: true
            };

        case constants.LIBRARY_RECEIVED:
            return {
                ...state,
                books: data.books,
                isLoading: false
            };
        case constants.PURCHASE_STATUS: {
            let books = [...state.books];
            // only overwrite books if new collection is available
            if (data && data.books) {
                books = [...data.books];
            }
            return {
                ...state,
                books,
                isLoading: false
            };
        }

        case constants.COVER_RECEIVED: {
            const covers = [...state.covers];
            const coverIndex = covers.findIndex(
                cover => cover.coverId === data.coverId
            );
            coverIndex > -1
                ? covers.splice(coverIndex, 1, data)
                : covers.push(data);

            return {
                ...state,
                covers
            };
        }

        default:
            return {
                ...state
            };
    }
}
