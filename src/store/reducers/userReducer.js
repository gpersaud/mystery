import constants from "../../config/constants";
import { login, logout, isAuthenticated } from "../../Auth/Auth";

let init = {
    isLoggedIn: isAuthenticated(),
    user: null,
    email: null,
    isRequestingPassword: false,
    hasRequestedPassword: false,
    isValidatingHash: false,
    isResetHashValid: null,
    isChangingPassword: undefined,
    isPasswordUpdated: undefined,
    isSigningUp: undefined,
    signedUp: undefined,
    isLoading: false
};

export function userReducer(state = init, { data, type }) {
    switch (type) {
        case constants.IS_LOGGING_IN: {
            return {
                ...state,
                isLoading: true
            };
        }

        case constants.PROCESS_LOGIN: {
            login(data);
            return {
                ...state,
                isLoggedIn: true,
                isLoading: false
            };
        }

        case constants.LOGIN_FAILED: {
            return {
                ...state,
                isLoading: false
            };
        }

        case constants.PROCESS_LOGOUT: {
            logout();
            return {
                ...state,
                isLoggedIn: false
            };
        }
        case constants.REQUESTING_PASSWORD_RESET: {
            return {
                ...state,
                isRequestingPassword: true
            };
        }
        case constants.RESET_PASSWORD_REQUESTED: {
            return {
                ...state,
                isRequestingPassword: false,
                hasRequestedPassword: true,
                email: data.email
            };
        }
        case constants.RESET_PASSWORD_AGAIN: {
            return {
                ...state,
                hasRequestedPassword: false
            };
        }
        case constants.IS_VALIDATING_HASH: {
            return {
                ...state,
                isValidatingHash: true
            };
        }
        case constants.PASSWORD_HASH_VALIDATED: {
            return {
                ...state,
                isValidatingHash: false
            };
        }
        case constants.IS_CHANGING_PASSWORD: {
            return {
                ...state,
                isChangingPassword: true,
                isPasswordUpdated: undefined
            };
        }
        case constants.PASSWORD_CHANGE_COMPLETED: {
            return {
                ...state,
                isChangingPassword: false,
                isPasswordUpdated: data.status
            };
        }
        case constants.IS_SIGNING_UP: {
            return {
                ...state,
                isSigningUp: true
            };
        }
        case constants.SIGNUP_COMPLETE: {
            login(data);
            return {
                ...state,
                isSigningUp: false,
                isLoggedIn: true
            };
        }

        case constants.SIGNUP_FAILED: {
            return {
                ...state,
                isSigningUp: false
            };
        }
        default:
            return state;
    }
}
