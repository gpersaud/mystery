import { configure } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import "jest-enzyme";

export default configure({ adapter: new Adapter() });
global.window = {};
