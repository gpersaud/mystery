const saveBeforeInstallPromptEvent = event => {
    window.deferredInstallPrompt = event;
};

window.addEventListener("beforeinstallprompt", saveBeforeInstallPromptEvent);
