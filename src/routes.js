import Books from "./components/Books/Books";
import LandingPage from "./components/LandingPage/LandingPage";
import BookPage from "./components/BookPage/BookPage";
import Login from "./components/User/Login";

const preloadBooks = () => {};
const preloadBook = () => {};

export default [
    {
        path: "/shop/:collection?",
        component: Books,
        data: preloadBooks,
    },
    {
        path: "/books/:collection?",
        component: Books,
        data: preloadBooks,
    },
    {
        path: "/book/:bookId",
        component: BookPage,
        data: preloadBook,
    },
    {
        path: "/login",
        component: Login,
    },
    {
        path: "/",
        component: LandingPage,
    },
];
