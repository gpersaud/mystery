import constants from "../config/constants";
import jwtDecode from "jwt-decode";
import Alert from "react-s-alert";
/**
 * Pure login function takes the access token
 * and stores it in local storage
 */
export const login = (data) => {
    try {
        Boolean(data && data.token) &&
            window.localStorage.setItem(constants.ACCESS_TOKEN, data.token);
        Alert.success("Logged in");
    } catch (e) {
        throw new Error("failed to store token in local storage: ", e);
    }
};

/**
 * Pure logout function
 */
// TODO use constants
export const logout = () => {
    localStorage.removeItem(constants.ACCESS_TOKEN);
    localStorage.removeItem("id_token");
    localStorage.removeItem("expires_at");
    Alert.info("Logged out");
};

/**
 * Checks if there is a JWT token.
 * If there is it's expiry date is checked.
 *
 * window does not exist on the server where the first render
 * is done. Therefore we need to avoid that error on the server
 * by explicitly checking for window.
 */
export const isAuthenticated = () => {
    if (typeof window === "undefined") return;
    const { localStorage } = window;
    if (localStorage && localStorage.getItem && localStorage.setItem) {
        const token = window.localStorage.getItem(constants.ACCESS_TOKEN);
        if (!token || typeof token !== "string") return false;
        const { exp = 0 } = jwtDecode(token);
        return exp < new Date().getTime();
    }
};

/**
 * Redirect the user to the login page but before doing that
 * save the path that they were on so they can be redirected
 * after login. (The user might be in the middle of purchasing a book)
 * @param {String} route The route that should be saved
 * @param {Object} history The history object of the browser
 * @param {String} path The path where the user should be directed to
 */
export const loginRedirect = (route = "", path = "/login") => {
    if (typeof window === "undefined") return;
    if (route && route.indexOf("password") > -1) route = "/library"; // don't redirect the user to the change password screen
    route
        ? window.localStorage.setItem(constants.LAST_ROUTE, route)
        : (path = window.localStorage.getItem(constants.LAST_ROUTE));
    if (path && path !== "/") return path;
    else return "/";
};
