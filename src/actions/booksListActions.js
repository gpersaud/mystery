import constants from "../config/constants";
import api from "../config/api";
import { logger } from "../utils/helpers";
import fetch from "isomorphic-fetch";

/**
 * Fetch the list of books available
 */
export const fetchBooks = (collection = "featured", limit) => async (
    dispatch
) => {
    dispatch &&
        dispatch({
            type: constants.FETCHING_BOOKS,
        });

    try {
        const response = await fetch(api.books.collection(collection, limit));

        if (!(response && response.ok)) {
            const { status = 500 } = response;
            throw new Error(
                `failed to fetch books for collection "${collection}". Status: ${status}`
            );
        }

        const data = await response.json();
        data &&
            dispatch &&
            dispatch({
                type: constants.BOOKS_RECEIVED,
                data: data.books,
            });
        return data.books;
    } catch (e) {
        logger("error", e.message);
        return { status: "failed" };
    }
};

export const selectBook = (bookId) => (dispatch) =>
    dispatch({
        type: constants.BOOK_SELECTED,
        data: bookId,
    });

export const networkFetchBooks = (collection = "featured", limit = 9) => {
    const url = api.books.collection(collection, limit);
    return fetch(url)
        .then((rsp) => rsp.json())
        .then((data) => data)
        .catch((e) =>
            logger.error(`Failed to retrieve books list: ${e.message}`)
        );
};
