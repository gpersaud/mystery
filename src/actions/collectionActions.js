import constants from "../config/constants";
import api from "../config/api";
import { logger } from "../utils/helpers";
import fetch from "isomorphic-fetch";

/**
 * Fetch the list of collections available
 */

export const fetchCollections = () => async (dispatch) => {
    dispatch({ type: constants.COLLECTIONS_REQUESTED });
    try {
        const data = await networkFetchCollection();
        if (!data || data.length < 1) {
            throw new Error("failed to fetch collection data", data.status);
        }
        data &&
            data instanceof Array &&
            dispatch(receivedCollectionsData(data));
    } catch (e) {
        logger("error", `Failed to retrieve collection list: ${e.message}`);
    }
};

export const networkFetchCollection = () =>
    fetch(api.books.collectionsList)
        .then((rsp) => rsp.json())
        .then((data) => data)
        .catch((e) =>
            logger("error", `Failed to retrieve collection list: ${e.message}`)
        );

export const receivedCollectionsData = (data) => ({
    type: constants.COLLECTIONS_RECEIVED,
    data: data,
});

// TODO: double check if this function should work like this
export const selectLanguage = (lang) => (dispatch) => {
    dispatch({
        type: constants.LANGUAGE_SELECTED,
        data: lang,
    });
};
