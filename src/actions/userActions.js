import constants from "../config/constants";
import api from "../config/api";
import { logger } from "../utils/helpers";
import Alert from "react-s-alert";

export const login = credentials => async dispatch => {
    dispatch(waitForLogin());
    try {
        if (!(credentials && Object.keys(credentials).length === 2)) {
            throw new Error("no credentials received");
        }
        const data = await networkLogin(credentials);
        data && dispatch(processLogin(data));
    } catch (e) {
        dispatch({ type: constants.LOGIN_FAILED });
        logger("error", e);
    }
};

export const waitForLogin = () => ({
    type: constants.IS_LOGGING_IN
});

export const networkLogin = credentials =>
    fetch(api.auth.login, postHeader(credentials))
        .then(rsp => {
            if (rsp.status === 200) return rsp.json();
            else {
                Alert.error(
                    `${rsp.statusText} - Please verify your username and password`
                );
                throw new Error(
                    "did not receive status 200 when logging in ",
                    rsp
                );
            }
        })
        .then(data => data);

export const processLogin = data => ({
    type: constants.PROCESS_LOGIN,
    data
});

export const logout = () => async dispatch => {
    dispatch(waitForLogout());
    await networkLogout();
    dispatch(processLogout());
};

const waitForLogout = () => ({
    type: constants.IS_LOGGING_OUT
});

const networkLogout = () =>
    fetch(api.auth.logout)
        .then(rsp => rsp.json)
        .then(data => data)
        .catch(e => logger("error", e));

const processLogout = () => ({ type: constants.PROCESS_LOGOUT });

/**
 * Reset the user's password using their email address
 * for verification
 */

export const resetPassword = credentials => async dispatch => {
    dispatch({ type: constants.REQUESTING_PASSWORD_RESET });
    const data = await networkRequestPasswordReset(credentials);
    data &&
        dispatch({
            type: constants.RESET_PASSWORD_REQUESTED,
            data: credentials
        });
};

export const resetPasswordAgain = () => dispatch =>
    dispatch({
        type: constants.RESET_PASSWORD_AGAIN
    });

export const networkRequestPasswordReset = credentials =>
    fetch(api.auth.passwordReset, postHeader(credentials))
        .then(rsp => rsp.json())
        .then(data => data)
        .catch(e => logger("error", e));

/**
 * Validate user hash before password reset
 *
 */

export const validateHash = hash => async dispatch => {
    dispatch({ type: constants.IS_VALIDATING_HASH });
    const data = await networkValidateHash(hash);
    data && dispatch({ type: constants.PASSWORD_HASH_VALIDATED, data });
};

export const networkValidateHash = hash =>
    fetch(api.auth.validateHash, postHeader({ hash }))
        .then(rsp => rsp.json())
        .then(data => data)
        .catch(e => logger("error", e));

export const changePassword = (password, hash) => async dispatch => {
    dispatch({ type: constants.IS_CHANGING_PASSWORD });
    const data = await networkChangePassword(password, hash);
    dispatch({
        type: constants.PASSWORD_CHANGE_COMPLETED,
        data: { status: Boolean(data) }
    });
};

export const networkChangePassword = (password, hash) =>
    fetch(api.auth.changePassword, {
        ...postHeader({ password }),
        headers: {
            Authorization: `Bearer ${hash}`,
            "Content-Type": "application/json"
        }
    })
        .then(rsp => {
            if (rsp.status === 200) return rsp.json();
            else {
                throw new Error(`${rsp.status}: ${rsp.statusText}`);
            }
        })
        .then(rsp => rsp)
        .catch(e => logger("error", e));

/**
 * user signup actions
 * @param  {String} email    The email the user is signing up with
 * @param  {String} password The password being created
 */
export const signup = (email, password) => async dispatch => {
    dispatch({ type: constants.IS_SIGNING_UP });
    const data = await networkSignup(email, password);
    if (data) {
        Alert.info("Great. You are now logged in with your new account");
        dispatch({
            type: constants.SIGNUP_COMPLETE,
            data
        });
    } else {
        dispatch({
            type: constants.SIGNUP_FAILED
        });
    }
};

export const networkSignup = (email, password) =>
    fetch(api.auth.signup, postHeader({ email, password }))
        .then(rsp => {
            if (rsp.status === 200) return rsp.json();
            else if (rsp.status === 403) {
                Alert.error("Email address in use");
                throw new Error(`${rsp.status}: ${rsp.statusText}`);
            }
        })
        .then(rsp => rsp)
        .catch(e => {
            logger("error", e);
            return false;
        });

/**
 * Creates the standard header used to send post requests
 * @param {Object} body The content to be sent in the body
 */
export const postHeader = body => ({
    method: "POST",
    mode: "cors",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify(body),
    json: true
});
