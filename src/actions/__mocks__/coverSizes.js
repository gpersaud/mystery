export default {
  sizes: {
    canblog: 0,
    canprint: 0,
    candownload: 1,
    size: [
      {
        label: "Square",
        width: 75,
        height: 75,
        source:
          "https://farm2.staticflickr.com/1702/25533801482_731de0e86f_s.jpg",
        url:
          "https://www.flickr.com/photos/139782750@N07/25533801482/sizes/sq/",
        media: "photo"
      },
      {
        label: "Large Square",
        width: "150",
        height: "150",
        source:
          "https://farm2.staticflickr.com/1702/25533801482_731de0e86f_q.jpg",
        url: "https://www.flickr.com/photos/139782750@N07/25533801482/sizes/q/",
        media: "photo"
      },
      {
        label: "Thumbnail",
        width: "65",
        height: "100",
        source:
          "https://farm2.staticflickr.com/1702/25533801482_731de0e86f_t.jpg",
        url: "https://www.flickr.com/photos/139782750@N07/25533801482/sizes/t/",
        media: "photo"
      },
      {
        label: "Small",
        width: "156",
        height: "240",
        source:
          "https://farm2.staticflickr.com/1702/25533801482_731de0e86f_m.jpg",
        url: "https://www.flickr.com/photos/139782750@N07/25533801482/sizes/s/",
        media: "photo"
      },
      {
        label: "Small 320",
        width: 209,
        height: "320",
        source:
          "https://farm2.staticflickr.com/1702/25533801482_731de0e86f_n.jpg",
        url: "https://www.flickr.com/photos/139782750@N07/25533801482/sizes/n/",
        media: "photo"
      },
      {
        label: "Medium",
        width: "326",
        height: "500",
        source:
          "https://farm2.staticflickr.com/1702/25533801482_731de0e86f.jpg",
        url: "https://www.flickr.com/photos/139782750@N07/25533801482/sizes/m/",
        media: "photo"
      },
      {
        label: "Medium 640",
        width: "360",
        height: "552",
        source:
          "https://farm2.staticflickr.com/1702/25533801482_731de0e86f_z.jpg",
        url: "https://www.flickr.com/photos/139782750@N07/25533801482/sizes/z/",
        media: "photo"
      },
      {
        label: "Original",
        width: "360",
        height: "552",
        source:
          "https://farm2.staticflickr.com/1702/25533801482_05737d8beb_o.png",
        url: "https://www.flickr.com/photos/139782750@N07/25533801482/sizes/o/",
        media: "photo"
      }
    ]
  },
  stat: "ok"
};
