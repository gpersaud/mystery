import constants from "../../config/constants";
import { fetchBooks } from "../booksListActions";
import {
    receivedBookCover,
    fetchBookCover,
    originalImage,
    fetchBook,
    networkFetchBook,
} from "../bookActions";
import casual from "casual";
import fetchMock from "fetch-mock";
import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import api from "../../config/api";
import mockedResponse from "../__mocks__/coverSizes";
import mockedData from "../__mocks__/booksData";

jest.mock("isomorphic-fetch"); // stops the import of isomorphic-fetch in the real code

const mockStore = configureMockStore([thunk]);

describe("Book actions", () => {
    it("should fetch a book", () => {
        const dispatchFunction = jest.fn();
        const bookId = "5ceda4b2ace87fd1c4a17be8";
        const expected = {
            type: constants.FETCHING_BOOK,
            data: bookId,
        };

        fetchBook(bookId)(dispatchFunction);

        expect(dispatchFunction).toHaveBeenCalledTimes(1);
        expect(dispatchFunction.mock.calls[0][0]).toEqual(expected);
    });

    it("should network fetch the book", async () => {
        jest.mock("isomorphic-fetch");
        const fetch = require("isomorphic-fetch");

        const [fakeBook] = mockedData.books;

        fakeBook._id = "test";

        const resp = {
            ok: true,
            json: () => Promise.resolve(fakeBook),
        };
        fetch.mockResolvedValue(resp);

        const results = await networkFetchBook(fakeBook._id);

        expect(await fetch()).toEqual(resp);
        expect(results).toEqual(fakeBook);
    });

    it("should fire an action when books are received", async () => {
        jest.mock("isomorphic-fetch");
        const fetch = require("isomorphic-fetch");

        const data = [
            {
                title1: casual.title,
                title2: casual.title,
                author: casual.name,
            },
        ];

        const expectedActions = {
            type: constants.BOOKS_RECEIVED,
            data,
        };

        const dispatchFunction = jest.fn();

        fetch.mockResolvedValue({
            ok: true,
            json: () => Promise.resolve(data),
        });

        const result = await fetchBooks("featured", 9)(dispatchFunction);
        expect(dispatchFunction).toHaveBeenCalledTimes(2);
        // expect(result).toEqual(expectedActions);
    });

    it("should fire an action when book covers are received", () => {
        const data = {
            bookId: casual.integer(0, 1000),
            coverId: casual.integer(0, 1000),
            coverUrl: casual.url,
        };

        const expectedActions = {
            type: constants.COVER_RECEIVED,
            data,
        };

        expect(receivedBookCover(data)).toEqual(expectedActions);
    });
});

describe("Books helper functions", () => {
    const result = originalImage(mockedResponse);
    expect(result).toEqual(
        expect.stringContaining(
            "https://farm2.staticflickr.com/1702/25533801482_05737d8beb_o.png"
        )
    );
});

describe("Books async actions", () => {
    afterEach(() => {
        fetchMock.reset();
        fetchMock.restore();
    });

    it("fetches books", async () => {
        jest.mock("isomorphic-fetch");
        const fetch = require("isomorphic-fetch");

        const resp = {
            ok: true,
            json: () => Promise.resolve(mockedData),
        };
        fetch.mockResolvedValue(resp);

        const store = mockStore(mockedData);
        await fetchBooks("featured", 10)(store.dispatch);

        // The action that should be triggered after data is received.
        const expectedActions = [
            {
                type: constants.FETCHING_BOOKS,
            },
            {
                type: constants.BOOKS_RECEIVED,
                data: mockedData.books,
            },
        ];

        expect(store.getActions()).toEqual(expectedActions);
        expect(store.getState()).toEqual(mockedData);
    });

    it("should fetch a book cover and store it in redux", async () => {
        const coverUrl =
            "https://farm2.staticflickr.com/1702/25533801482_05737d8beb_o.png";

        const coverId = casual.integer(0, 1000);
        const bookId = casual.integer(0, 1000);

        jest.mock("isomorphic-fetch");
        const fetch = require("isomorphic-fetch");

        const mockedData = {
            books: [
                {
                    covers: {
                        123: coverUrl,
                    },
                },
            ],
        };

        fetch.mockResolvedValue({
            ok: true,
            json: () => Promise.resolve(mockedData),
        });

        const store = mockStore(mockedData);

        await store.dispatch(fetchBookCover(coverId, bookId));
        expect(store.getActions()).toEqual([]);
        expect(store.getState()).toEqual(mockedData);
    });
});
