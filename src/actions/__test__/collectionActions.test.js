import constants from "../../config/constants";
import { fetchCollections } from "../collectionActions";
import api from "../../config/api";
import { logger } from "../../utils/helpers";
jest.mock("../../utils/helpers");
jest.mock("isomorphic-fetch");

describe("Collection actions", () => {
    it("should fetch the collections", () => {
        const dispatchFunction = jest.fn();
        const expected = {
            type: constants.COLLECTIONS_REQUESTED,
        };

        jest.mock("isomorphic-fetch");
        const fetch = require("isomorphic-fetch");

        const resp = {
            ok: true,
            json: () => Promise.resolve([]),
        };
        fetch.mockResolvedValue(resp);

        fetchCollections()(dispatchFunction);

        expect(dispatchFunction).toHaveBeenCalledTimes(1);
        expect(dispatchFunction.mock.calls[0][0]).toEqual(expected);
        expect(fetch).toHaveBeenCalledWith(api.books.collectionsList);
    });

    it("should throw an error if it failed to get the collection", () => {
        const dispatchFunction = jest.fn();

        jest.spyOn(global, "fetch").mockImplementation(() =>
            Promise.resolve({
                ok: false,
                status: 500,
            })
        );
        fetchCollections()(dispatchFunction);
        expect(logger).toHaveBeenCalledTimes(1);
    });
});
