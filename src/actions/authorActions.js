import constants from "../config/constants";
import api from "../config/api";
import { createHeader, logger } from "../utils/helpers";
import fetch from "isomorphic-fetch";

/**
 * Get the user's email
 */

export const fetchUser = (userId) => async (dispatch) => {
    const data = await fetchUserData(userId);
    data && dispatch(userReceived(data));
};

export const fetchUserData = (userId) =>
    fetch(api.books.author.info(userId), createHeader())
        .then((rsp) => rsp.json())
        .then((data) => data)
        .catch((e) => logger("error", e));

export const userReceived = (data) => ({
    type: constants.FETCHED_AUTHOR,
    data,
});
