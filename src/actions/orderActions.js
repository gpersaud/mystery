import constants from "../config/constants";
import { createHeader, logger } from "../utils/helpers";
import api from "../config/api";
import Alert from "react-s-alert";
/**
 * When the user has made a purchase an order (receipt) is created
 * This retrieves that order (receipt) so it can be shown to the user
 */

export const fetchOrder = orderId => async dispatch => {
    dispatch(fetchingOrder());
    const data = await fetchOrderData(orderId);
    data && dispatch(orderReceived(data));
};

export const fetchingOrder = () => ({
    type: constants.FETCHING_ORDER
});

export const fetchOrderData = orderId =>
    fetch(api.shop.order(orderId), createHeader())
        .then(rsp => rsp.json())
        .then(data => data)
        .catch(e => logger("error ", e));

export const orderReceived = data => ({
    type: constants.RECEIVED_ORDER,
    data
});

// ----- SCA

export const purchaseStripeBook = (
    bookId,
    nameOnCard,
    stripe
) => async dispatch => {
    dispatch({
        type: constants.PURCHASING_BOOK
    });
    try {
        const response = await startPaymentProcess(bookId, nameOnCard, stripe);
        const library = await handlePaymentChallenge(bookId, response, stripe);

        if (library && library.books) {
            dispatch({
                type: constants.PURCHASE_STATUS,
                data: { books: library.books, bookId, purchaseSuccess: true }
            });
        } else {
            library.error &&
                Alert.error(library.error, {
                    timeout: "none"
                });
            dispatch({
                type: constants.PURCHASE_STATUS,
                data: { purchaseSuccess: false }
            });
        }
    } catch (e) {
        if (e.message) {
            Alert.error(e.message);
            logger("error", e.message);
            window.gtag("event", "purchase_failed", {
                event_category: "checkout"
            });
        }
        dispatch({
            type: constants.PURCHASE_STATUS,
            data: { purchaseSucccess: false }
        });
    }
};

export const startPaymentProcess = async (bookId, nameOnCard, stripe) => {
    // Step 1: Send a request to stripe to start the payment process
    const { paymentMethod, error } = await stripe.createPaymentMethod("card", {
        billing_details: { name: nameOnCard }
    });

    if (error) {
        throw error;
    }

    // Step 2: confirm the id received from stripe on mytoori-service
    const header = createHeader("POST");
    header.body = JSON.stringify({
        payment_method_id: paymentMethod.id,
        bookId
    });
    const response = await fetch(api.shop.confirmPayment(), header);

    if (!response.ok) {
        throw new Error(`Payment confirmation failed: ${response}`);
    }

    return await response.json();
};

export const handlePaymentChallenge = async (bookId, response, stripe) => {
    if (!response || response.error) {
        // show error from server on payment form
        throw new Error(response.error);
    }

    if (response.requires_action) {
        const {
            error: errorAction,
            paymentIntent
        } = await stripe.handleCardAction(
            response.payment_intent_client_secret
        );

        if (errorAction) {
            throw new Error(errorAction.message);
        }
        // The card action has been handled
        // The PaymentIntent can be confirmed again on the server

        const header = createHeader("POST");
        header.body = JSON.stringify({
            payment_intent_id: paymentIntent.id,
            bookId
        });

        const serverResponse = await fetch(api.shop.confirmPayment(), header);

        if (serverResponse.error) {
            return serverResponse;
        }

        // call the same endpoint again to confirm
        return await handlePaymentChallenge(
            bookId,
            await serverResponse.json(),
            stripe
        );
    } else {
        return response;
    }
};
