import constants from "../config/constants";
import api from "../config/api";
import { createHeader, logger } from "../utils/helpers";

/**
 * Get the user's library
 */

export const fetchLibrary = () => async dispatch => {
    dispatch(fetchingLibrary());
    const data = await fetchLibraryData();
    data && dispatch(libraryReceived(data));
};

export const fetchingLibrary = () => ({
    type: constants.FETCHING_LIBRARY
});

export const fetchLibraryData = () =>
    fetch(api.library.all(), createHeader("post"))
        .then(rsp => rsp.json())
        .then(data => data)
        .catch(e => logger("info", e));

export const libraryReceived = data => ({
    type: constants.LIBRARY_RECEIVED,
    data
});
