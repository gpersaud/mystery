import constants from "../config/constants";
import api from "../config/api";
import { logger, createHeader } from "../utils/helpers";
import Alert from "react-s-alert";
import { logout } from "../Auth/Auth";
import fetch from "isomorphic-fetch";

const _BOOKMARKS = "BOOKMARKS";

/**
 * Get the book and update the application
 * @param {String} bookId Unique identifier for the book
 */
export const fetchBook = (bookId, chapterLimit = 2) => async (dispatch) => {
    dispatch &&
        dispatch({
            type: constants.FETCHING_BOOK,
            data: bookId,
        });

    try {
        const data = await networkFetchBook(bookId, chapterLimit);
        data &&
            dispatch &&
            dispatch({
                type: constants.BOOK_RECEIVED,
                data,
            });
        return data; // returns the book for renderer.js
    } catch (e) {
        Alert.error(`Failed to fetch book ${bookId}. Error: ${e.message}`);
    }
};

/**
 * Used by server renderer to also get the book
 * @param {String} bookId The book identifier
 */
export const networkFetchBook = async (bookId, chapterLimit = "") => {
    const url = api.item.displayById(bookId, chapterLimit);
    const response = await fetch(url);

    if (!response.ok) {
        throw new Error(`Failed to fetch book. Status: ${response.status}`);
    }

    return await response.json();
};

export const fetchBookStats = (_id) => async (dispatch) => {
    if (!_id) {
        throw new Error("No _id provided");
    }
    dispatch({ type: constants.FETCHING_BOOK_STATS, data: _id });

    try {
        const response = await fetch(api.item.stats(_id));
        if (!(response && response.ok)) {
            throw new Error("failed to get book stats");
        }
        const data = await response.json();
        data && dispatch({ type: constants.RECEIVED_BOOK_STATS, data });
    } catch (e) {
        logger("error", `Failed to retrieve book stats: ${e.message}`);
    }
};

/**
 * Fetches book cover url from remote repository
 * @param {String} coverId
 * @param {String} bookId
 */
export const fetchBookCover = (coverId, bookId) => async (dispatch) => {
    const data = await fetchBookCoverUrl(coverId);
    const coverUrl = originalImage(data);
    coverUrl && dispatch(receivedBookCover({ bookId, coverUrl, coverId }));
};

export const fetchBookCoverUrl = async (coverId) => {
    try {
        const response = await fetch(api.books.cover_server(coverId));
        if (!(response && response.ok)) {
            const { status = 500 } = response;
            throw new Error(`Failed to fetch book cover: ${status}`);
        }
        return await response.json();
    } catch (e) {
        logger("error", e.message);
    }
};

/**
 * Filters out the original image from
 * a list of image sizes
 * @param {Array} data
 */
export const originalImage = (data) => {
    if (data && data.sizes) {
        const { size } = data.sizes;
        return size && size.find((image) => image.label === "Original").source;
    }
};

export const receivedBookCover = (data) => ({
    type: constants.COVER_RECEIVED,
    data,
});

export const networkFetchBookContent = (bookId) =>
    fetch(api.bookContent(bookId), createHeader())
        .then((rsp) => {
            if (rsp.status === 200) return rsp.json();
            if (rsp.status === 403) {
                logout();
                window.location = "/";
            }
        })
        .then((data) => data)
        .catch((e) => logger("error", e));

export const fetchBookContent = (bookId) => async (dispatch) => {
    dispatch({
        type: constants.FETCHING_BOOK_CONTENT,
    });
    const data = await networkFetchBookContent(bookId);
    data &&
        dispatch({
            type: constants.FETCHED_BOOK_CONTENT,
            data,
        });
};

export const purchaseBook = (bookId) => async (dispatch) => {
    dispatch(purchasingBook());
    const data = (await networkPurchaseBook(bookId)) || {};
    if (data && data.paymentUrl) {
        window.location = data.paymentUrl;
    } else if (data && data.result) {
        data && dispatch(newBook(data));
        Alert.success("Added to library");
    } else {
        Alert.warning(`Failed to get book: ${data.error}`);
    }
};

export const purchasingBook = () => ({
    type: constants.PURCHASING_BOOK,
});

export const networkPurchaseBook = (bookId) =>
    fetch(api.item.purchase(bookId), createHeader("post"))
        .then((data) => data.json())
        .then((data) => data)
        .catch((e) => logger("error", e));

export const newBook = (data) => ({
    type: constants.PURCHASE_STATUS,
    data,
});

/**
 * save the user's bookmark on remote server
 * @param {String} id book item identifier
 * @param {String} mark bookmark identifier
 */
export const bookmarkSync = (id, mark) =>
    fetch(api.library.bookmark(id, mark), createHeader("put"))
        .then((data) => data.json())
        .then((data) => data)
        .catch((e) => logger("error", e));

/**
 * Retrieves the remote bookmark for a given book.
 * @param {String} id The id of the book for which the bookmark should be retrieved
 */
export const getRemoteBookmark = (id) =>
    fetch(api.library.getBookmark(id), createHeader())
        .then((data) => data.json())
        .then((data) => data)
        .catch((e) => logger("error", e));

/**
 * Retrieves the bookmark from either local storage or
 * from the network
 * @param {String} id The id of the book for which the bookmark is wanted
 */
export const getBookmark = (id) => async (dispatch) => {
    let bookmarks = JSON.parse(localStorage.getItem(_BOOKMARKS)) || {};

    if (!(bookmarks && bookmarks[id])) {
        const result = await getRemoteBookmark(id);
        bookmarks[id] = result && result.result;
    }
    bookmarks &&
        dispatch({
            type: constants.GET_BOOKMARK,
            data: bookmarks,
        });
};

export const resetPurchaseState = () => (dispatch) =>
    dispatch({
        type: constants.PURCHASE_STATUS,
        data: {
            purchaseSuccess: null,
            purchasedBook: null,
        },
    });

/**
 * First indicate that the free book is being dowloaded
 * download the free book
 * then update the redux store
 * purchaseSuccess is null because the receipt screen
 * is not requried for free books
 * @param {String} _id The id of the book being downloaded
 */
export const getFreeBook = (_id) => async (dispatch) => {
    dispatch(purchasingBook());

    try {
        const response = await fetch(
            api.shop.getItem(_id),
            createHeader("POST")
        );
        if (!(response && response.ok)) {
            throw new Error(`Failed to get free book with _id: ${_id}`);
        }

        const library = await response.json();
        library && dispatch(newBook({ library, _id, purchaseSuccess: null }));
    } catch (e) {
        logger("error", e);
    }
};
