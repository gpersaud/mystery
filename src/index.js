import React from "react";
import { hydrate, render } from "react-dom";
import App from "./components/App";
import * as serviceWorker from "./serviceWorker";
import store from "./store/store";
import { Provider } from "react-redux";
import { BrowserRouter as Router, Route } from "react-router-dom";
// ---
// import history from "./history";
import "./install";

import * as Sentry from "@sentry/browser";

Sentry.init({
    dsn: process.env.REACT_APP_SENTRY_KEY,
});

/**
 * The appContainer has two routes
 * 1. for regular routes and
 * 2. another for authentication callbacks
 */
const AppContainer = () => (
    <Router>
        <Provider store={store}>
            <Route component={App} />
        </Provider>
    </Router>
);

/*
    Required for server side rendering
    If there are root nodes then only hydrate
 */
const root = document.getElementById("root");
root.hasChildNodes()
    ? hydrate(<AppContainer />, root)
    : render(<AppContainer />, root);

serviceWorker.unregister();
