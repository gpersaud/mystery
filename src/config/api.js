/**
 * If the front-end is running on a remote server, the service is not accessible through a subdomain.
 * /api/* will try to go to the domain that the front-end is running on.
 * Instead it should go to the domain of the service
 */

const {
    REACT_APP_API_TARGET,
    REACT_APP_FLICKR_KEY,
    REACT_APP_FLICKR_API_URL,
} = process.env;

const api_version = "api/v2";
const api = `${REACT_APP_API_TARGET}/${api_version}`;

export default {
    auth: {
        login: `${api}/login`,
        logout: `${api}/logout`,
        passwordReset: `${api}/user/password`,
        validateHash: `${api}/user/validate-hash/`,
        changePassword: `${api}/user/change-password/`,
        signup: `${api}/signup`,
    },
    books: {
        collectionsList: `${api}/topic`,
        collection: (collectionName = "featured", limit = 24) =>
            `${api}/collection/${collectionName}/${limit}`,
        cover_server: (cover) =>
            `${REACT_APP_FLICKR_API_URL}flickr.photos.getSizes&api_key=${REACT_APP_FLICKR_KEY}&photo_id=${cover}&format=json&nojsoncallback=1`,
        author: {
            info: (userId) => `${api}/user/${userId}`,
        },
    },
    item: {
        display: (itemId, lang) => `${api}/item/${itemId}/${lang}`,
        displayById: (itemId, chapterLimit = 2) =>
            `${api}/item/id/${itemId}/${chapterLimit}`,
        stats: (itemId) => `${api}/item/stats/${itemId}`,
        purchase: (itemId) => `${api}/shop/purchase/${itemId}`,
        stripePurchase: () => `${api}/shop/stripe/purchase`,
    },
    shop: {
        order: (orderId) => `${api}/shop/order/${orderId}`,
        getItem: (bookId) => `${api}/shop/get/${bookId}`,
        confirmPayment: () => `${api}/shop/stripe/confirm_payment`,
    },
    bookContent: (bookId) => `${api}/document/${bookId}`,
    library: {
        all: () => `${api}/library/`,
        bookmark: (id, mark) => `${api}/library/bookmark/${id}/${mark}`,
        getBookmark: (id) => `${api}/library/bookmark/${id}`,
    },
};
