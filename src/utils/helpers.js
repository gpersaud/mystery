import constants from "../config/constants";

const window = global.window || {};

/**
 * Formats the received array of language objects to return an
 * object that has a value and an id so
 * it can easily be consumed by a standard select option
 */

export const languageDTO = (received = []) =>
    received.map((lang) => {
        lang["label"] = lang.languageName;
        lang["id"] = lang._id;
        return lang;
    });

/**
 * Return token from local storage
 */
export const getToken = () =>
    isBrowser ? window.localStorage.getItem(constants.ACCESS_TOKEN) : undefined;

export const logger = (level, message) => {
    if (process.env.NODE_ENV === "production" || typeof window === "undefined")
        return null;
    if (!level || !["log", "error", "info"].includes(level)) level = "log";
    return (
        console &&
        console[level] &&
        typeof console[level] === "function" &&
        console[level]({ level: level, message: message }) // eslint-disable-line
    );
};

/**
 * Takes a HTTP request type and creates the header
 * Inserts the user's token in the header so content specifically
 * for this user is accessed.
 * If the create header is called, it means that the code is already
 * running on the client
 * @param {String} requestType The type of HTTP request to perform
 */
export const createHeader = (requestType = "get") => ({
    method: requestType,
    headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem(constants.ACCESS_TOKEN)}`,
    },
});

export const isEmpty = (x) => x === "" || x === null || x === undefined;

export const validatePassword = ({ password, repeatPassword }) => {
    if (isEmpty(password)) {
        throw new Error("Please fill in a password");
    }

    if (password.length < 6) {
        throw new Error("6 or more characters required");
    }

    if (isEmpty(repeatPassword)) {
        throw new Error("Please repeat the password for validation");
    }

    if (password !== repeatPassword) {
        throw new Error("passwords should match");
    }
    return true;
};

export const isEmailValid = (email) => {
    const regExp = new RegExp(
        "^[-!#$%&'*+/0-9=?A-Z^_a-z{|}~](.?[-!#$%&'*+/0-9=?A-Z^_a-z{|}~])*@[a-zA-Z](-?[a-zA-Z0-9])*(.[a-zA-Z](-?[a-zA-Z0-9])*)+$",
        "i"
    );
    const isValid = regExp.test(email);
    if (!isValid) {
        throw new Error("Please enter a valid email address");
    }
    return isValid;
};

export const termsAccepted = (isAccepted) => {
    if (!isAccepted) {
        throw new Error(
            "Please accept the terms of service in order to continue"
        );
    }
    return isAccepted;
};

export const doesTheUserOwnTheBook = (isAuthenticated, library, book) =>
    isAuthenticated &&
    library &&
    library.length > 0 &&
    book &&
    Boolean(library.find(({ item }) => item._id === book._id));

/**
 * Finds the book in the user's downloaded library. That book is returned
 * so the user can be directed to it.
 * @param {Bool} isAuthenticated
 * @param {Array} library
 * @param {Object} book
 * @return {Object}
 */
export const getLibraryBook = (
    isAuthenticated = false,
    library = [],
    book = {}
) =>
    isAuthenticated &&
    library.length > 0 &&
    book &&
    library.find(({ item }) => item._id === book._id);

export const sortCollections = (collections = []) => {
    if (!(collections && collections.length > 0)) return;
    collections.sort((itemA, itemB) => {
        const a = itemA.titles[0].value.toUpperCase();
        const b = itemB.titles[0].value.toUpperCase();
        return a < b ? -1 : 1;
    });
    return collections;
};

/**
 * Some pages need to load from the top of the page
 * This takes the scrolling container to to the top
 */
export const scrollToTop = (target = ".mt-layout__content") => {
    const contentTop = document.querySelector(target);
    if (contentTop) contentTop.scrollTop = 0;
};

export const isBrowser = !!(
    window &&
    window.localStorage &&
    window.localStorage.getItem &&
    window.localStorage.setItem
);

export const getEditorLink = () => {
    if (!window) return;
    const {
        location: { hostname },
    } = window;
    const value = /acc|localhost/.test(hostname) ? "acc.editor" : "editor";
    return `//${value}.mytoori.com`;
};

/**
 * Either return the titles array if it exists or create an array
 * from the legacy title format.
 * @param {Object} book
 */
export const getBookTitles = ({ titles, ...book }) =>
    Array.isArray(titles) && titles.length > 0
        ? titles.map(({ value }) => value)
        : [book.title1, book.title2];

export const getLanguages = ({ languages, ...book }) =>
    Array.isArray(languages) && languages.length > 0
        ? languages
        : [book.language1, book.language2];
