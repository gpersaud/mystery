export default [
    {
        _id: "27aG4aPd8PBkBXfDq",
        languageCode: "vi",
        languageName: "Vietnamese",
        languageNativeName: "Tiếng Việt"
    },
    {
        _id: "28QJjkBMQLqY3QepD",
        languageCode: "hu",
        languageName: "Hungarian",
        languageNativeName: "Magyar"
    },
    {
        _id: "2E84KYPbBanC3qcvp",
        languageCode: "ks",
        languageName: "Kashmiri",
        languageNativeName: "कश्मीरी, كشميري‎"
    }
];
