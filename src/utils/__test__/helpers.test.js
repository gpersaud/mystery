import { languageDTO, getBookTitles, getLanguages } from "../helpers";
import mockData from "../__mocks__/languageList";

describe("Language DTO check", () => {
    let result;
    beforeEach(() => {
        result = languageDTO(mockData);
    });
    it("validates the data transformation for the languages object", () => {
        expect(result).not.toBe(null);
        expect(result[0]).not.toBe(null);
        expect(result.length).toEqual(mockData.length);
        expect(result).toEqual(expect.arrayContaining(mockData));
    });
    it("checks that the required properties have been created and are available", () => {
        result.map((lang, idx) => {
            expect(lang.label).toEqual(mockData[idx].languageName);
            expect(lang.id).toEqual(mockData[idx]._id);
        });
    });
});

describe("should return titles", () => {
    it("should return an array given an array of objects", () => {
        const book = {
            titles: [
                {
                    lang: "en",
                    value: "english title"
                },
                {
                    lang: "nl",
                    value: "dutch title"
                }
            ]
        };
        const titles = getBookTitles(book);
        expect(titles.length).toEqual(2);
        expect(titles).toEqual(book.titles.map(({ value }) => value));
    });
    it("should return an array given an array of strings ", () => {
        const book = {
            title1: "test title1",
            title2: "test title2"
        };
        const titles = getBookTitles(book);
        expect(titles.length).toEqual(2);
        expect(titles).toEqual([book.title1, book.title2]);
    });
});

describe("should return languages", () => {
    it("should accept languages is an array", () => {
        const book = {
            languages: ["en", "nl"]
        };
        const languages = getLanguages(book);
        expect(languages).toEqual(book.languages);
    });
    it("should accept languages as properties", () => {
        const book = {
            language1: "en",
            language2: "nl"
        };
        const languages = getLanguages(book);
        expect(languages).toEqual([book.language1, book.language2]);
    });
});
