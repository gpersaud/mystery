# Mytoori Front-end

React in the front-end with a backend API running on NodeJS

## Locally

After doing a `npm i && npm run build`

`npm run dev`

## Getting started with Development

Create a new .env file in the root project folder. 

`touch .env`

Add the following settings

```
REACT_APP_STRIPE_API_KEY=
REACT_APP_API_TARGET=https://mytoori-service-acc.herokuapp.com
REACT_APP_SENTRY_KEY=
REACT_APP_FLICKR_KEY=
REACT_APP_FLICKR_API_URL=

```


## Commiting code

When committing code, husky is configured to run prettier which uses .eslintrc.json apply default code formatting rules.
The terminal should indicate that it's running husky with something to the effect of:

```bash
husky > pre-commit (node v10.5.0)
  ↓ Stashing changes... [skipped]
    → No partially staged files found...
  ✔ Running linters...
[isbn_to_id f3bb541] wip: switch to using _id
 6 files changed, 56 insertions(+), 59 deletions(-)
```


## Hosting

The React frontend was hosted on zeit.co but has been moved to heroku (production DNS not yet updated). This was changed from netlify.com where it's very easy to host static files. The reason for the change is _server side rendering_.
Rendering on the server requires a **server**. Zeit.co allows applications to be hosted for free on a node server. More about this in [Server Side Rendering](#server-side-rendering).

### Deployment with BitBucket Pipelines

In `bitbucket-pipelines.yml` the deployement steps are specified. When code is pushed to the master branch it is automatically built on the acceptance environment. In heroku the Acceptance (staging) environment has environment variables defined which are used at build time when deploying the app. This ensures for example that acc.mytoori.com still connects to the acc backend.

#### Deploying to production

The production environment requires a manual trigger.

## Build process

`npm run build` creates a production ready bundle in the build folder.

`npm run server-dev` runs a node server locally which serves the production bundle that was created with the build step. This is useful when debugging the node server.

When the solution is pushed to zeit.co, the start script in package.json is used to kick off the node server. For this reason the start command has been modified in the package.json. To develop locally with hot-reloading, `npm run start-dev` can be used.

---

## Server Side Rendering

The server side rendering is broken up into three files. These are all in the folder `server`. This folder has been added solely for the purpose of server side rendering.

> The node server serves the build folder.

The build folder holds the production version of the static assets. If this folder does not exist or is outdated, it's re-created or updated by running `npm run build`.

### `server/index.js`

In the server folder the process starts with the file `index.js`. That file creates the express app in node which in turn imports the route configuration.

### `server/controller/index.js`

This file is essentially the route configuration. It creates the express router. The following line ensures that if the user enters `/` that the static assets are served.

`router.use('^/$', serverRenderer());`

In the next section we need to make sure that the other static resources can be found. Think about images and css files.

```
router.use(express.static(
    path.resolve(__dirname, '..', '..', 'build'),
    { maxAge: '30d' },
));
```

When the user refreshes te page on a different route than slash we still want to ensure that all the static content is found. That is handled by the following line.

`router.use("^(?!api$)", serverRenderer());`

This file in turn imports `serverRenderer` and executes it. That rendered is what renders the react app on the server so that it can be served to the client.

### Server/middleware/renderer.js

Here the React App.js main component is imported and rendered. In addition initial data can be added and provided.

---
