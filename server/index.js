import express from "express";
import router from "./controller/index";
import shrinkRay from "shrink-ray-current";
import logger from "./logger"; // server logger

const app = express();
const port = process.env.PORT || 4000; // heroku assigns it's own port (all others default to 4000)

// redirect if the user is not on https
if (process.env.NODE_ENV === "production") {
    app.use((req, res, next) => {
        if (
            /localhost/.test(req.get("origin")) &&
            req.header("x-forwarded-proto") !== "https"
        )
            res.redirect(`https://${req.header("host")}${req.url}`);
        else next();
    });
}

app.use(shrinkRay()); // text compression

// Tell the app to use the routes above
app.use(router);

// start the app
app.listen(port, () =>
    logger.log({ level: "info", message: `running on port ${port}` })
);
