import React from "react";
import { renderToString } from "react-dom/server";
import App from "../../src/components/App";
import { StaticRouter, Route } from "react-router-dom";
import store from "../../src/store/store";
import { Provider } from "react-redux";
import serialize from "serialize-javascript";
import logger from "../logger";
import {
    fetchBooksDataServer,
    fetchBookDataServer,
    fetchCollectionsDataServer,
} from "./dataForRenderer";

const path = require("path");
const fs = require("fs");
const zlib = require("zlib");
const filePath = path.resolve(__dirname, "..", "..", "build", "index.html");

const initialRequest = () => async (request, response) => {
    const collectionsData = await fetchCollectionsDataServer(
        request.url,
        logger
    );
    const booksData = await fetchBooksDataServer(request.url, logger);
    const book = await fetchBookDataServer(request.url, logger);

    // point to the html file created by CRA's build tool
    fs.readFile(filePath, "utf8", (error, htmlData) => {
        if (error) {
            logger.log({ level: "error", message: error });
            return response.status(404).end();
        }

        const reduxState = store.getState();
        reduxState.booksReducer.collections = collectionsData;
        reduxState.booksReducer.books = booksData;
        reduxState.booksReducer.selectedBook = book;

        // render the app as a string
        const html = renderToString(
            <StaticRouter location={request.url} context={{}}>
                <Provider store={store}>
                    <Route component={App} />
                </Provider>
            </StaticRouter>
        );

        // send initial (redux) data
        const initialData = `
                <script>
                    window.__REDUX_INITIAL_DATA__ = ${serialize(reduxState)};
                </script>`;

        // create a buffer for compression
        const buffer = Buffer.from(
            htmlData.replace(
                '<div id="root"></div>',
                `<div id="root">${html}</div>${initialData}`
            )
        );

        // zip the buffer, update the head and send it
        // TODO: use brotli for better compression: https://www.npmjs.com/package/shrink-ray-current#expressconnect
        // TODO: optimize code
        zlib.gzip(buffer, (error, result) => {
            if (error) {
                return logger.log({ level: "error", message: error });
            }
            response.writeHead(200, {
                ...request.headers,
                "Content-Encoding": "gzip",
                "Content-Type": "text/html; charset=UTF-8",
            });
            return response.end(result);
        });
    });
};

export default initialRequest;
