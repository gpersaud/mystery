import { matchPath } from "react-router";

// reuse components from src
// fetch the topics
import { networkFetchCollection } from "../../src/actions/collectionActions";
// fetch books for a given topic
import { networkFetchBooks } from "../../src/actions/booksListActions";
import { networkFetchBook } from "../../src/actions/bookActions";
import logger from "../logger";

/**
 * Takes the requested url and decides if
 * book data should be provided based on that
 * @param  {String}  url The url as a string
 * @return {Array}   The array of book data objects
 */
export const fetchBooksDataServer = async (url = "", logger) => {
    try {
        let booksData = [];
        const match = matchPath(url, { path: "/books/:collection?/:limit?" });

        if (match) {
            const collection = match.params.collection || "featured";
            booksData = await networkFetchBooks(collection);
        }
        return booksData.books || booksData;
    } catch (e) {
        logger.log({ level: "error", message: e });
    }
};

export const fetchBookDataServer = async (url) => {
    try {
        let book = {};

        // reader page match
        const freeReaderMatch = matchPath(url, { path: "/book/read/:id" });

        if (freeReaderMatch) {
            return await networkFetchBook(freeReaderMatch.params.id, 100);
        }

        // Check the path of the request to determine which data to request
        const bookPageMatch = matchPath(url, {
            path: "/book/:id",
        });

        if (bookPageMatch) {
            // fetch book data and inject it into the page before rendering
            const bookId = bookPageMatch.params.id;
            book = await networkFetchBook(bookId, 2); // fetch only 2 chapters
        }

        return book;
    } catch (e) {
        logger.log({ level: "error", message: e });
    }
};

export const fetchCollectionsDataServer = async (url, logger) => {
    try {
        let collections = [];

        const collectionsMatch = matchPath(url, {
            path: "/books/",
        });

        if (collectionsMatch) {
            collections = await networkFetchCollection();
        }

        return collections;
    } catch (e) {
        logger.log({ level: "error", message: e });
    }
};
