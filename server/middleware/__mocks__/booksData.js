export default {
    books: [
        {
            _id: "5ceda4acace87fd1c4a17b6b",
            author: "Samuel Nang'ole\nprimary language English",
            description1:
                "The short story is about a boy of fifteen who becomes a Marathon Champion despite the fact that he is very young.This comes about because he had been challenged to run while on the way to school by a lady after a joke.The story begins in Baringo in Kenya moves on to the USA and back to Nairobi Kenya.",
            description2:
                "Hii hadithi fupi inazungumzia kijana ambaye anaibuka kuwa gwiji wa mbio za nyika ingawaje yeye ni mdogo mno.Ana miaka kumi na mitano tu.Hadithi yenyewe inaanzia Baringo Kenya, halafu inaenda huko Marekani halafu inaishia Nairobi Kenya.",
            language1: "en",
            language2: "sw",
            notes1:
                "The English language written here is the normal English that you would find being spoken by anyone who can speak English.It is simple,easy to understand language with major intention of sharing a story.But the language has no error since I am a teacher of the English language in Kenya.",
            notes2:
                "Mimi nimekuwa msomi wa Kiswahili toka nikiwa katika shule ya msingi hadi shule ya sekondari.Lakini huwa ninazungumza kiswahili kila siku kwa sababu Kiswahili ndio lugha yetu ya Taifa nchini Kenya.Nimejaribu sana kutafsiri hii hadithi kwa lugha ambayo inaeleweka ili anayeisoma hii hadithi aifurahie.Kwa hivyo wewe kaa kidako chukua hadithi uisome na ufurahi.",
            title1: "THE MARATHON CHAMPION\n\n\n         ",
            title2: "GWIJI WA MBIO ZA NYIKA",
            userId: "EDrLsdwwpKtnD2zS8",
            authorFirstname: "Samuel",
            authorLastname: "Nang'ole",
            translatorFirstname: "Samuel",
            translatorLastname: "Nang'ole",
            cover: "28955985393",
            cover2: "25356881160",
            isbn: "com.mytoori.book.themarathonchampion",
            isbn2: "",
            status: "published",
            titles: [
                { lang: "en", value: "THE MARATHON CHAMPION\n\n\n         " },
                { lang: "sw", value: "GWIJI WA MBIO ZA NYIKA" },
            ],
            descriptions: [
                {
                    lang: "en",
                    value:
                        "The short story is about a boy of fifteen who becomes a Marathon Champion despite the fact that he is very young.This comes about because he had been challenged to run while on the way to school by a lady after a joke.The story begins in Baringo in Kenya moves on to the USA and back to Nairobi Kenya.",
                },
                {
                    lang: "sw",
                    value:
                        "Hii hadithi fupi inazungumzia kijana ambaye anaibuka kuwa gwiji wa mbio za nyika ingawaje yeye ni mdogo mno.Ana miaka kumi na mitano tu.Hadithi yenyewe inaanzia Baringo Kenya, halafu inaenda huko Marekani halafu inaishia Nairobi Kenya.",
                },
            ],
            notes: [
                {
                    lang: "en",
                    value:
                        "The English language written here is the normal English that you would find being spoken by anyone who can speak English.It is simple,easy to understand language with major intention of sharing a story.But the language has no error since I am a teacher of the English language in Kenya.",
                },
                {
                    lang: "sw",
                    value:
                        "Mimi nimekuwa msomi wa Kiswahili toka nikiwa katika shule ya msingi hadi shule ya sekondari.Lakini huwa ninazungumza kiswahili kila siku kwa sababu Kiswahili ndio lugha yetu ya Taifa nchini Kenya.Nimejaribu sana kutafsiri hii hadithi kwa lugha ambayo inaeleweka ili anayeisoma hii hadithi aifurahie.Kwa hivyo wewe kaa kidako chukua hadithi uisome na ufurahi.",
                },
            ],
            covers: [
                { lang: "en", value: "28955985393" },
                { lang: "sw", value: "25356881160" },
            ],
            translators: [{ firstName: "Samuel", lastName: "Nang'ole" }],
            price: 10,
            dates: { published: 1513249200000, updated: 1515672000000 },
            authors: [{ firstName: "Samuel", lastName: "Nang'ole" }],
            coverImages: [
                {
                    _id: "5d0e45c513056484315f930b",
                    value:
                        "https://res.cloudinary.com/mytoori/image/upload/v1560608643/mytoori/covers/28955985393_f9c09135e9_o.png.png",
                },
            ],
        },
        {
            _id: "5ceda4acace87fd1c4a17b6d",
            author: "Written by Chen Qiling. Translated by Yuzhi Yang",
            title1: "白衣",
            title2: "Man in White",
            description1:
                "哪里会想到那天有什么不一样呢?\n\n那青年男子长衫雪白，手里一杆黑色长笛。\n\n她不会让阿锦知道，她有多么爱慕那一袭飘飘的白衣，除了他，世上再没有哪个男人，能把白衣裳穿得那样好了。\n",
            description2:
                "It was 1944. A young woman had a crush on her boarding school teacher, Mr. Yun, in war-torn China. Before she could tell him how she felt, he left the school. Devastated, she and a friend went to look for him in the big city of Shanghai. The glamorous city was under Japanese occupation, and her crush was now arrested because of his writing. There was no one who could help him, unless she could ask the notorious mobster Du Yuesheng for help.\n",
            notes1: "资深中英翻译，翻译过小说，散文，及儿童书籍。",
            notes2:
                "Published translator with more than 10 years of Chinese-English translation experience.  ",
            language1: "zh",
            language2: "EN",
            userId: "nyC578kqc9kHAkxqd",
            cover: "25559835851",
            status: "published",
            isbn: "com.mytoori.book.maninwhite",
            authorFirstname: "",
            authorLastname: "",
            translatorFirstname: "",
            translatorLastname: "",
            cover2: "26482701516",
            titles: [
                { lang: "zh", value: "白衣" },
                { lang: "EN", value: "Man in White" },
            ],
            descriptions: [
                {
                    lang: "zh",
                    value:
                        "哪里会想到那天有什么不一样呢?\n\n那青年男子长衫雪白，手里一杆黑色长笛。\n\n她不会让阿锦知道，她有多么爱慕那一袭飘飘的白衣，除了他，世上再没有哪个男人，能把白衣裳穿得那样好了。\n",
                },
                {
                    lang: "EN",
                    value:
                        "It was 1944. A young woman had a crush on her boarding school teacher, Mr. Yun, in war-torn China. Before she could tell him how she felt, he left the school. Devastated, she and a friend went to look for him in the big city of Shanghai. The glamorous city was under Japanese occupation, and her crush was now arrested because of his writing. There was no one who could help him, unless she could ask the notorious mobster Du Yuesheng for help.\n",
                },
            ],
            notes: [
                {
                    lang: "zh",
                    value: "资深中英翻译，翻译过小说，散文，及儿童书籍。",
                },
                {
                    lang: "EN",
                    value:
                        "Published translator with more than 10 years of Chinese-English translation experience.  ",
                },
            ],
            covers: [
                { lang: "zh", value: "25559835851" },
                { lang: "EN", value: "26482701516" },
            ],
            translators: [],
            languages: ["zh", "EN"],
            price: 5,
            dates: { published: 1513249200000, updated: 1515672000000 },
            authors: [{ firstName: "", lastName: "" }],
            coverImages: [
                {
                    _id: "5d0e45c513056484315f9303",
                    value:
                        "https://res.cloudinary.com/mytoori/image/upload/v1560608643/mytoori/covers/25559835851_3907fc64e6_o.png.png",
                },
            ],
        },
        {
            _id: "5ceda4adace87fd1c4a17b80",
            author: "Clara Lairla",
            title1: "Cruce",
            title2: "Crossroads",
            description1:
                "Daniel acaba de sufrir un desengaño amoroso. Toda su vida se ha visto trastocada y necesita huir de su pasado para encontrar un nuevo comienzo. Un Erasmus en Irlanda es la primera piedra de un camino nuevo que se abre a sus pies. \nAna es enfermera en Nigeria, pero debido a la escalada de violencia que sufre la zona, se ve obligada a irse de allí y volver a España, dejando en África algo más que un trabajo. \nAmbos van encontrando poco a poco su sitio y consiguen rehacer sus nuevas vidas dejando atrás su pasado. Pero la vida no les deja acomodarse y parece querer deshacer de un bandazo todo lo reconstruido, obligándoles a volver de nuevo al punto del que habían huido, situado exactamente en un cruce inesperado.",
            description2:
                "Daniel is suffering from a failed relationship. His whole life is disrupted and he needs to run away from his past in order to find a new beginning. Living as an Erasmus in Ireland is the first step of a new life which opens right before him.\nAna is a nurse in Nigeria, but because of the violence rise in that area, she must leave and travel back to Spain, leaving in Africa something more than just a job.\nBoth of them slowly find their place and manage to rebuild their new lives, leaving behind their past. But life doesn't let them settle in and seems to want to break down in a marked shift all what they have rebuilt. It forces them to go back again to the point from which they had fled, which is situated at an unexpected crossroad.",
            notes1:
                "Soy española nativa y licenciada en traducción e interpretación. Soy traductora de inglés y he estudiado el idioma a lo largo de muchos años. También he vivido durante algunos periodos de tiempo en países angloparlantes y actualmente trabajo en inglés. Para asegurar una traducción al inglés lo más natural posible, mi traducción ha sido revisada por Karisa Austin, hablante nativa.",
            notes2:
                "I am a native Spanish speaker and I have a degree on translation and interpreting. I translate from English into Spanish and I have been studying the language for many years. I have also lived for different periods of time in English speaking countries and I am currently working for an English speaking school. In order to assure a translation into English is as natural as possible, my translation has been proofread by Karisa Austin, a native speaker.",
            language1: "ES",
            language2: "EN",
            userId: "wPwRxSW8Rqbzooqz9",
            cover: "25559839131",
            authorFirstname: "Clara",
            authorLastname: "Lairla",
            translatorFirstname: "Clara",
            translatorLastname: "Lairla",
            cover2: "25356888410",
            isbn: "com.mytoori.book.cruce",
            status: "published",
            isbn2: "com.mytoori.book.crossroads",
            titles: [
                { lang: "ES", value: "Cruce" },
                { lang: "EN", value: "Crossroads" },
            ],
            descriptions: [
                {
                    lang: "ES",
                    value:
                        "Daniel acaba de sufrir un desengaño amoroso. Toda su vida se ha visto trastocada y necesita huir de su pasado para encontrar un nuevo comienzo. Un Erasmus en Irlanda es la primera piedra de un camino nuevo que se abre a sus pies. \nAna es enfermera en Nigeria, pero debido a la escalada de violencia que sufre la zona, se ve obligada a irse de allí y volver a España, dejando en África algo más que un trabajo. \nAmbos van encontrando poco a poco su sitio y consiguen rehacer sus nuevas vidas dejando atrás su pasado. Pero la vida no les deja acomodarse y parece querer deshacer de un bandazo todo lo reconstruido, obligándoles a volver de nuevo al punto del que habían huido, situado exactamente en un cruce inesperado.",
                },
                {
                    lang: "EN",
                    value:
                        "Daniel is suffering from a failed relationship. His whole life is disrupted and he needs to run away from his past in order to find a new beginning. Living as an Erasmus in Ireland is the first step of a new life which opens right before him.\nAna is a nurse in Nigeria, but because of the violence rise in that area, she must leave and travel back to Spain, leaving in Africa something more than just a job.\nBoth of them slowly find their place and manage to rebuild their new lives, leaving behind their past. But life doesn't let them settle in and seems to want to break down in a marked shift all what they have rebuilt. It forces them to go back again to the point from which they had fled, which is situated at an unexpected crossroad.",
                },
            ],
            notes: [
                {
                    lang: "ES",
                    value:
                        "Soy española nativa y licenciada en traducción e interpretación. Soy traductora de inglés y he estudiado el idioma a lo largo de muchos años. También he vivido durante algunos periodos de tiempo en países angloparlantes y actualmente trabajo en inglés. Para asegurar una traducción al inglés lo más natural posible, mi traducción ha sido revisada por Karisa Austin, hablante nativa.",
                },
                {
                    lang: "EN",
                    value:
                        "I am a native Spanish speaker and I have a degree on translation and interpreting. I translate from English into Spanish and I have been studying the language for many years. I have also lived for different periods of time in English speaking countries and I am currently working for an English speaking school. In order to assure a translation into English is as natural as possible, my translation has been proofread by Karisa Austin, a native speaker.",
                },
            ],
            covers: [
                { lang: "ES", value: "25559839131" },
                { lang: "EN", value: "25356888410" },
            ],
            translators: [{ firstName: "Clara", lastName: "Lairla" }],
            price: 10,
            dates: { published: 1513249200000, updated: 1515672000000 },
            authors: [{ firstName: "Clara", lastName: "Lairla" }],
            coverImages: [
                {
                    _id: "5d0e45c513056484315f9304",
                    value:
                        "https://res.cloudinary.com/mytoori/image/upload/v1560608643/mytoori/covers/25559839131_bb35c9a4e7_o.png.png",
                },
            ],
        },
        {
            _id: "5ceda4aeace87fd1c4a17b8f",
            author: "Andrea Bruciati",
            title1: "The Sound Of Silence",
            title2: "El ruido del silencìo",
            description1: "Story of a young promise of classical music.",
            description2: "Historia de un jóven talento de musica clasica.",
            language1: "en",
            language2: "es",
            userId: "wbyYGAH7DLtBwwTgp",
            notes1:
                "I studied English at school, I graduated as a B.A. in Languages and I spoke costantly English during an Eramsus period.",
            notes2:
                "Estudie español a la escuela y a la universidad. Tuve el ocasión de hablar con madrelinguas españolas durante un periodo de Erasmus.",
            cover: "27606107053",
            authorFirstname: "Andrea",
            authorLastname: "Bruciati",
            translatorFirstname: "Andrea",
            translatorLastname: "Bruciati",
            cover2: "25626336276",
            isbn: "com.mytoori.book.thesoundofsilence",
            isbn2: "",
            status: "published",
            titles: [
                { lang: "en", value: "The Sound Of Silence" },
                { lang: "es", value: "El ruido del silencìo" },
            ],
            descriptions: [
                {
                    lang: "en",
                    value: "Story of a young promise of classical music.",
                },
                {
                    lang: "es",
                    value: "Historia de un jóven talento de musica clasica.",
                },
            ],
            notes: [
                {
                    lang: "en",
                    value:
                        "I studied English at school, I graduated as a B.A. in Languages and I spoke costantly English during an Eramsus period.",
                },
                {
                    lang: "es",
                    value:
                        "Estudie español a la escuela y a la universidad. Tuve el ocasión de hablar con madrelinguas españolas durante un periodo de Erasmus.",
                },
            ],
            covers: [
                { lang: "en", value: "27606107053" },
                { lang: "es", value: "25626336276" },
            ],
            translators: [{ firstName: "Andrea", lastName: "Bruciati" }],
            price: 10,
            dates: { published: 1513249200000, updated: 1515672000000 },
            authors: [{ firstName: "Andrea", lastName: "Bruciati" }],
            coverImages: [
                {
                    _id: "5d0e45c513056484315f92fe",
                    value:
                        "https://res.cloudinary.com/mytoori/image/upload/v1560608643/mytoori/covers/27606107053_7a95eda848_o.png.png",
                },
            ],
        },
        {
            _id: "5ceda4aeace87fd1c4a17b95",
            author:
                "Ewa Alicja Jablonska (adaptation of a book by Napoleon Hill)",
            title1: "Think and Grow Rich: Thirteen Bombshells for Success",
            title2: "Pensez et Devenez Riches: Treize Principes de Réussite",
            description1:
                "Napoleon Hill devoted his entire life to uncovering the arcana of success; his priceless effort and work have contributed to the magnificent achievement among the most respectable names in the history of this world. \nI decided that it would be a marvellous idea to put the philosophy in a nutshell and promote it in the form of an e-book that serves to study languages. I do not know about you Dear Reader, but when it comes to me, I love reading,\nand even more, I love making it utility reading. \nI honestly hope that through reading this short text, you will not only polish your language skills, but you will also get acquainted with the precious thought of Napoleon Hill.\n",
            description2:
                "Napoleon Hill a consacré sa vie entière à trouver et definir les principes de la réussite. Son effort a pris la forme d'un livre qui a aidé à avoir du succès aux plusieurs gens respectables connus par l'histoire. Ayant lu le livre moi-même,\nj'ai decidé qu'il vaut la peine de le raccourcir et promouvoir comme un e-book qui sert à apprendre aux gens à parler anglais et français. Je ne connais pas vos préférences, mais moi j'aime bien lire,\net particulièrement, j'apprécie la lecture des livres utiles. J'espère que ce texte vous apportera plusieurs bénéfices: ceux du côté linguistique, mais aussi les avantages de faire connaissance avec la pensée de Napoleon Hill.\n",
            notes1:
                "I have been learning and constantly using English for the past 25 years, and thus it has become my second nature and my means of perceiving the world. :) ",
            notes2:
                "J'ai appris à parler français pendant mes séjours en Belgique, en travaillant comme jeune fille au pair. En Pologne, j'ai reçu trois ans d'éducation oficielle qui m'a appris à être capable de corriger mes erreurs à l'écriture. J'espère offrir la qualité de la langue qui sera suffisament bonne pour vous assister dans vos études du français. :)",
            language1: "en",
            language2: "fr",
            userId: "EQjhGxQs6iLdS2AGM",
            cover: "25739788054",
            status: "published",
            authorFirstname: "Ewa A.",
            authorLastname: "Jablonska",
            translatorFirstname: "Ewa A.",
            translatorLastname: "Jablonska",
            isbn: "com.mytoori.book.thinkandgrowrich",
            cover2: "25559831051",
            titles: [
                {
                    lang: "en",
                    value:
                        "Think and Grow Rich: Thirteen Bombshells for Success",
                },
                {
                    lang: "fr",
                    value:
                        "Pensez et Devenez Riches: Treize Principes de Réussite",
                },
            ],
            descriptions: [
                {
                    lang: "en",
                    value:
                        "Napoleon Hill devoted his entire life to uncovering the arcana of success; his priceless effort and work have contributed to the magnificent achievement among the most respectable names in the history of this world. \nI decided that it would be a marvellous idea to put the philosophy in a nutshell and promote it in the form of an e-book that serves to study languages. I do not know about you Dear Reader, but when it comes to me, I love reading,\nand even more, I love making it utility reading. \nI honestly hope that through reading this short text, you will not only polish your language skills, but you will also get acquainted with the precious thought of Napoleon Hill.\n",
                },
                {
                    lang: "fr",
                    value:
                        "Napoleon Hill a consacré sa vie entière à trouver et definir les principes de la réussite. Son effort a pris la forme d'un livre qui a aidé à avoir du succès aux plusieurs gens respectables connus par l'histoire. Ayant lu le livre moi-même,\nj'ai decidé qu'il vaut la peine de le raccourcir et promouvoir comme un e-book qui sert à apprendre aux gens à parler anglais et français. Je ne connais pas vos préférences, mais moi j'aime bien lire,\net particulièrement, j'apprécie la lecture des livres utiles. J'espère que ce texte vous apportera plusieurs bénéfices: ceux du côté linguistique, mais aussi les avantages de faire connaissance avec la pensée de Napoleon Hill.\n",
                },
            ],
            notes: [
                {
                    lang: "en",
                    value:
                        "I have been learning and constantly using English for the past 25 years, and thus it has become my second nature and my means of perceiving the world. :) ",
                },
                {
                    lang: "fr",
                    value:
                        "J'ai appris à parler français pendant mes séjours en Belgique, en travaillant comme jeune fille au pair. En Pologne, j'ai reçu trois ans d'éducation oficielle qui m'a appris à être capable de corriger mes erreurs à l'écriture. J'espère offrir la qualité de la langue qui sera suffisament bonne pour vous assister dans vos études du français. :)",
                },
            ],
            covers: [
                { lang: "en", value: "25739788054" },
                { lang: "fr", value: "25559831051" },
            ],
            translators: [{ firstName: "Ewa A.", lastName: "Jablonska" }],
            price: 10,
            dates: { published: 1513249200000, updated: 1515672000000 },
            authors: [{ firstName: "Ewa A.", lastName: "Jablonska" }],
            coverImages: [
                {
                    _id: "5d0e45c513056484315f9305",
                    value:
                        "https://res.cloudinary.com/mytoori/image/upload/v1560608643/mytoori/covers/25739788054_93e08a2acf_o.jpg.jpg",
                },
            ],
        },
        {
            _id: "5ceda4aeace87fd1c4a17b9a",
            author: "Jules Verne",
            title1: "Journey to the centre of the earth (sample)",
            title2: "Voyage au centre de la Terre (sample)",
            description1: 'Le classic histoire "Voyage au centre de la Terre"',
            description2:
                'The classic story "a journey to the centre of the Earth"',
            language1: "en",
            language2: "fr",
            userId: "bdcrck6HSedRK8bHv",
            authorFirstname: "Jules",
            authorLastname: "Verne",
            translatorFirstname: "",
            translatorLastname: "",
            notes1: "",
            notes2: "",
            cover: "25533797812",
            cover2: "25533791482",
            isbn: "com.mytoori.book.sample5",
            isbn2: "",
            price: 0,
            status: "published",
            titles: [
                {
                    lang: "en",
                    value: "Journey to the centre of the earth (sample)",
                },
                { lang: "fr", value: "Voyage au centre de la Terre (sample)" },
            ],
            descriptions: [
                {
                    lang: "en",
                    value: 'Le classic histoire "Voyage au centre de la Terre"',
                },
                {
                    lang: "fr",
                    value:
                        'The classic story "a journey to the centre of the Earth"',
                },
            ],
            notes: [
                { lang: "en", value: "" },
                { lang: "fr", value: "" },
            ],
            covers: null,
            translators: [],
            languages: ["en", "fr"],
            dates: { published: 1513249200000, updated: 1515672000000 },
            authors: [{ firstName: "Jules", lastName: "Verne" }],
            coverImages: [
                {
                    _id: "5ce456ecb3bed200152d6779",
                    lang: "en",
                    value:
                        "https://res.cloudinary.com/mytoori/image/upload/v1557127274/mytoori/covers/25533797812_e693773c17_o.png",
                },
            ],
        },
        {
            _id: "5ceda4adace87fd1c4a17b89",
            author: "Jane Austen",
            title1: "Pride and Prejudice Sample",
            title2: "Orgueil et Préjugés\nLes cinq filles de Mrs Bennet Sample",
            description1: "The english classic story",
            description2: "Le classic livre anglaise.",
            language1: "en",
            language2: "fr",
            userId: "bdcrck6HSedRK8bHv",
            authorFirstname: "Jane",
            authorLastname: "Austen",
            translatorFirstname: "Jane",
            translatorLastname: "Doe",
            notes1: "",
            notes2: "",
            cover: "25626339916",
            cover2: "25559834921",
            isbn: "com.mytoori.book.prideandprejudicesample",
            isbn2: "com.mytoori.book.sample1",
            price: 0,
            status: "published",
            titles: [
                { lang: "en", value: "Pride and Prejudice Sample" },
                {
                    lang: "fr",
                    value:
                        "Orgueil et Préjugés\nLes cinq filles de Mrs Bennet Sample",
                },
            ],
            descriptions: [
                { lang: "en", value: "The english classic story" },
                { lang: "fr", value: "Le classic livre anglaise." },
            ],
            notes: [
                { lang: "en", value: "" },
                { lang: "fr", value: "" },
            ],
            covers: null,
            translators: [{ firstName: "Jane", lastName: "Doe" }],
            dates: { published: 1513249200000, updated: 1515672000000 },
            authors: [{ firstName: "Jane", lastName: "Austen" }],
            coverImages: [
                {
                    _id: "5ce45717b3bed200152d6860",
                    lang: "en",
                    value:
                        "https://res.cloudinary.com/mytoori/image/upload/v1558468375/mytoori/covers/25626339916_3aa16f2529_o.png",
                },
            ],
        },
        {
            _id: "5ceda4adace87fd1c4a17b85",
            author: "Chiara Valchera",
            description1:
                "Se il tuo paese, il tuo migliore amico ed il tuo futuro fossero in pericolo, avresti il coraggio di mettere a repentaglio la tua vita? Scopri le ardue scelte cui il giovane Frenk deve far fronte per liberare la sua patria fantastica da un'antica maledizione e salvare un migliore amico molto speciale da morte certa. Prova anche tu le intense emozioni e la suspense di questa storia fantasy e lasciati guidare all'interno di un mondo dove tutto è possibile...",
            description2:
                "If your country, your best friend and your future were at danger, would you put your life at risk and do anything to preserve them? Find out the hard choices that Frenk, a brave young boy, had to make in order to free his fantasic homeland from an ancient curse and to save his best friend from certain death. Feel the great emotions and suspense of this fantasy story and let it guide you into a magical world where anything is possible...",
            language1: "IT",
            language2: "EN",
            notes1:
                "Il testo in lingua italiana, in quanto redatto nella mia lingua madre, rappresenta la copia originale del romanzo e contiene un linguaggio ricco e sfaccettato, tipico della narrativa fiabesca. La sintassi è lineare e segue l'ordine cronologico degli eventi, non celando quindi trappole insidiose per il lettore straniero. \nLa traduzione verso l'inglese è la trasposizione ideale del romanzo verso una lingua che studio con passione da molti anni. Ideale, perché è stata realizzata con una fedeltà assoluta nei confronti dell'idea, (io stessa sono autrice e traduttrice); scelte più libere sono state invece applicate sul piano lessicale. La traduzione che troverete si sforza di essere il più possibile uno \"specchio della forma\". Tuttavia, laddove quest'ultima rischiava di subire limitazioni per ragioni culturali o stilistiche, si è fatto ricorso a un testo libero dal vincolo delle parole, che fa da specchio fedele soltanto nei confronti del contenuto. Detto questo... vi auguro buona lettura!",
            notes2:
                'The Italian text represents the original copy of the novel, as it has been composed in my mother language. It uses a rich and colorful lexicon, which is also proper to the fairy-tale narrative. The syntax is simple and clear and progresses chronologically, allowing a easier comprehension for the foreign reader.\nThe translation into English is a perfect transposition of the story to a language I\'ve studied with passion for many years. Therefore, it has been created with a 100% fidelity to the idea, (I am at the same time the author and the translator); while some freedom has been used poetically. This translation is meant to be a "perfect reflection of the form" in its highest capacity. But, whenever it risked to suffer from limitations of any kind, due to cultural or stylistic reasons, it has been released from the bondage deriving from single words, and only fidelity to content has been fully respected. That said... I wish you happy reading!',
            title1: "Lacrime di Drago",
            title2: "Dragon Tears",
            userId: "thnGvcgbdMmsfjtqS",
            authorFirstname: "Chiara",
            authorLastname: "Valchera",
            translatorFirstname: "Chiara",
            translatorLastname: "Valchera",
            cover: "25652530415",
            cover2: "28092701432",
            isbn: "com.mytoori.book.lacrimedidrago",
            isbn2: "",
            status: "published",
            titles: [
                { lang: "IT", value: "Lacrime di Drago" },
                { lang: "EN", value: "Dragon Tears" },
            ],
            descriptions: [
                {
                    lang: "IT",
                    value:
                        "Se il tuo paese, il tuo migliore amico ed il tuo futuro fossero in pericolo, avresti il coraggio di mettere a repentaglio la tua vita? Scopri le ardue scelte cui il giovane Frenk deve far fronte per liberare la sua patria fantastica da un'antica maledizione e salvare un migliore amico molto speciale da morte certa. Prova anche tu le intense emozioni e la suspense di questa storia fantasy e lasciati guidare all'interno di un mondo dove tutto è possibile...",
                },
                {
                    lang: "EN",
                    value:
                        "If your country, your best friend and your future were at danger, would you put your life at risk and do anything to preserve them? Find out the hard choices that Frenk, a brave young boy, had to make in order to free his fantasic homeland from an ancient curse and to save his best friend from certain death. Feel the great emotions and suspense of this fantasy story and let it guide you into a magical world where anything is possible...",
                },
            ],
            notes: [
                {
                    lang: "IT",
                    value:
                        "Il testo in lingua italiana, in quanto redatto nella mia lingua madre, rappresenta la copia originale del romanzo e contiene un linguaggio ricco e sfaccettato, tipico della narrativa fiabesca. La sintassi è lineare e segue l'ordine cronologico degli eventi, non celando quindi trappole insidiose per il lettore straniero. \nLa traduzione verso l'inglese è la trasposizione ideale del romanzo verso una lingua che studio con passione da molti anni. Ideale, perché è stata realizzata con una fedeltà assoluta nei confronti dell'idea, (io stessa sono autrice e traduttrice); scelte più libere sono state invece applicate sul piano lessicale. La traduzione che troverete si sforza di essere il più possibile uno \"specchio della forma\". Tuttavia, laddove quest'ultima rischiava di subire limitazioni per ragioni culturali o stilistiche, si è fatto ricorso a un testo libero dal vincolo delle parole, che fa da specchio fedele soltanto nei confronti del contenuto. Detto questo... vi auguro buona lettura!",
                },
                {
                    lang: "EN",
                    value:
                        'The Italian text represents the original copy of the novel, as it has been composed in my mother language. It uses a rich and colorful lexicon, which is also proper to the fairy-tale narrative. The syntax is simple and clear and progresses chronologically, allowing a easier comprehension for the foreign reader.\nThe translation into English is a perfect transposition of the story to a language I\'ve studied with passion for many years. Therefore, it has been created with a 100% fidelity to the idea, (I am at the same time the author and the translator); while some freedom has been used poetically. This translation is meant to be a "perfect reflection of the form" in its highest capacity. But, whenever it risked to suffer from limitations of any kind, due to cultural or stylistic reasons, it has been released from the bondage deriving from single words, and only fidelity to content has been fully respected. That said... I wish you happy reading!',
                },
            ],
            covers: [
                { lang: "IT", value: "25652530415" },
                { lang: "EN", value: "28092701432" },
            ],
            translators: [{ firstName: "Chiara", lastName: "Valchera" }],
            price: 22,
            dates: { published: 1513249200000, updated: 1515672000000 },
            authors: [{ firstName: "Chiara", lastName: "Valchera" }],
            coverImages: [
                {
                    _id: "5d0e45c513056484315f92f1",
                    value:
                        "https://res.cloudinary.com/mytoori/image/upload/v1560608643/mytoori/covers/25652530415_e79405192c_o.png.png",
                },
            ],
        },
        {
            _id: "5ceda4aeace87fd1c4a17b97",
            author: "Iris Spanbroek",
            title1: "De jongen met de gloeiende ogen",
            title2: "The boy with the glowing eyes",
            description1:
                "Doe een wens. Doe een wens en hoop dat hij uitkomt. De meeste wensen komen nooit uit en dat is voornamelijk omdat die ontzettend verveelde gast op kantoor, het kantoor waar jouw wensen binnenkomen, wel betere dingen te doen heeft.\nTot het moment dat hij een klein maar opmerkelijk wensje ontvangt van een jongen genaamd Jonathan Silver. Jonathan wenst gloeilampen als ogen. De gast op kantoor besluit om eens een risico te nemen, want, tja… wat kan er nou helemaal misgaan?\nBlijkbaar – heel erg veel.",
            description2:
                "Make a wish. Make a wish, and hope it comes true. Most wishes don’t come true, however, and that’s usually because the incredibly bored guy at the office that receives your wishes, has better things to do.\nUntil the moment he receives a small yet rather peculiar wish from a boy named Jonathan Silver. Jonathan wishes for light bulb eyes. The guy at the office decides to give things a shot, because, well… what could possible go wrong?\nApparently – a lot.",
            notes1:
                "Nederlands is mijn moedertaal – Engels is mijn tweede taal. Ik ben zo’n twaalf jaar geleden begonnen met het leren van Engels, via school. Ik leer nog steeds elke dag! Niveau C2.\n",
            notes2:
                "Dutch is my native language – English is my second language. I started learning English about twelve years ago, through school. I’m still learning things every day! Level C2.",
            language1: "nl",
            language2: "en",
            userId: "kzhHFMvrXo4P9p4Ff",
            authorFirstname: "Iris",
            authorLastname: "Spanbroek",
            translatorFirstname: "Iris",
            translatorLastname: "Spanbroek",
            cover: "28222554955",
            cover2: "25284933209",
            isbn: "com.mytoori.book.dejongenmetdegloeiendeogen",
            isbn2: "com.mytoori.book.theboywiththeglowingeyes",
            status: "published",
            titles: [
                { lang: "nl", value: "De jongen met de gloeiende ogen" },
                { lang: "en", value: "The boy with the glowing eyes" },
            ],
            descriptions: [
                {
                    lang: "nl",
                    value:
                        "Doe een wens. Doe een wens en hoop dat hij uitkomt. De meeste wensen komen nooit uit en dat is voornamelijk omdat die ontzettend verveelde gast op kantoor, het kantoor waar jouw wensen binnenkomen, wel betere dingen te doen heeft.\nTot het moment dat hij een klein maar opmerkelijk wensje ontvangt van een jongen genaamd Jonathan Silver. Jonathan wenst gloeilampen als ogen. De gast op kantoor besluit om eens een risico te nemen, want, tja… wat kan er nou helemaal misgaan?\nBlijkbaar – heel erg veel.",
                },
                {
                    lang: "en",
                    value:
                        "Make a wish. Make a wish, and hope it comes true. Most wishes don’t come true, however, and that’s usually because the incredibly bored guy at the office that receives your wishes, has better things to do.\nUntil the moment he receives a small yet rather peculiar wish from a boy named Jonathan Silver. Jonathan wishes for light bulb eyes. The guy at the office decides to give things a shot, because, well… what could possible go wrong?\nApparently – a lot.",
                },
            ],
            notes: [
                {
                    lang: "nl",
                    value:
                        "Nederlands is mijn moedertaal – Engels is mijn tweede taal. Ik ben zo’n twaalf jaar geleden begonnen met het leren van Engels, via school. Ik leer nog steeds elke dag! Niveau C2.\n",
                },
                {
                    lang: "en",
                    value:
                        "Dutch is my native language – English is my second language. I started learning English about twelve years ago, through school. I’m still learning things every day! Level C2.",
                },
            ],
            covers: [
                { lang: "nl", value: "28222554955" },
                { lang: "en", value: "25284933209" },
            ],
            translators: [{ firstName: "Iris", lastName: "Spanbroek" }],
            price: 10,
            dates: { published: 1513249200000, updated: 1515672000000 },
            authors: [{ firstName: "Iris", lastName: "Spanbroek" }],
            coverImages: [
                {
                    _id: "5d0e45c513056484315f92fd",
                    value:
                        "https://res.cloudinary.com/mytoori/image/upload/v1560608643/mytoori/covers/28222554955_342a0feeec_o.png.png",
                },
            ],
        },
    ],
};
