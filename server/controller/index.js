import express from "express";
import path from "path";
import serverRenderer from '../middleware/renderer';

const router = express.Router();

// root (/) should always serve our server rendered page
router.use('^/$', serverRenderer());

// other static resources should just be served as they are
router.use(express.static(
    path.resolve(__dirname, '..', '..', 'build'),
    { maxAge: '30d' },
));

/**
 * Ensures the front-end source and assets are still 
 * found if the user refreshes the page on a deep route: 
 * /book/com.mytoori.book.sample4
 * If this route is not here the front-end assets are not found
 */
router.use("^(?!api$)", serverRenderer());

export default router;